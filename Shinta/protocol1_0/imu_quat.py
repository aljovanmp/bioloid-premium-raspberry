from mpu6050 import mpu6050
import math
import time
from madgwickahrs import MadgwickAHRS

imu = mpu6050(0x68)
imu.set_accel_range(imu.ACCEL_RANGE_8G)
imu.set_gyro_range(imu.GYRO_RANGE_2000DEG)

madgwick_filter = MadgwickAHRS()

prev_time = time.time()
dt = 0.01

while(1):
	if time.time() - prev_time > dt:
		prev_time = time.time()
		data = imu.get_all_data()
		accel_data = data[0]
		gyro_data = data[1]
		gyro = [gyro_data['x'], gyro_data['y'], gyro_data['z']]
		accel = [accel_data['x'], accel_data['y'], accel_data['z']]
		madgwick_filter.update_imu(gyro, accel)
		ahrs = madgwick_filter.quaternion.to_euler_angles()
		deg = [ahrs[0]*180.0/math.pi, ahrs[1]*180.0/math.pi, ahrs[2]*180.0/math.pi]
		print("rpy: ", round(deg[0],2), round(deg[1],2), round(deg[2],2))
