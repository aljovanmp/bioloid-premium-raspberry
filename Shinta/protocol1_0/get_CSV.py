# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 13:14:56 2020

@author: Aljovan Magyarsa P
"""
import csv

def getCSV(filename):
    fields = [] 
    rows = [] 
    # reading csv file 
    with open(filename, 'r') as csvfile:
        # creating a csv reader object 
        csvreader = csv.reader(csvfile)
        
        # extracting field names through first row 
        fields = csvreader.__next__() 
        
        # extracting each data row one by one 
        for row in csvreader: 
            rows.append(row) 
  
        # get total number of rows 
        # print("Total no. of rows: %d"%(csvreader.line_num)) 
    
    # printing the field names 
    # print('Field names are:' + ', '.join(field for field in fields)) 
    
    
    #  printing first 5 rows 
    # print('\nFirst 5 rows are:\n') 
    
    # for row in rows: 
        # print(row)
        # for col in row:
            # print(col)
        
    return fields,rows
            