# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 15:52:26 2020

@author: Aljovan Magyarsa P
"""
from dynamixel_sdk import * 
import csv
class Servo:
    presentpos = 0
    ADDR_AX_PRES_POS               = 36
    ADDR_AX_GOAL_POSITION          = 30
    ADDR_AX_SPEED                  = 32
    
    def __init__(self, default, id, port_handler, packet_handler):
        self.default = default
        self.id = id
        self.PortHandler = port_handler
        self.PacketHandler = packet_handler
    
    def read(self,address,length):
        out = []
        if length == 2 :
            feedback, dxl_comm_result, dxl_error = self.PacketHandler.read2ByteTxRx(self.PortHandler, self.id, address)
        if length == 1 :
            feedback, dxl_comm_result, dxl_error = self.PacketHandler.read1ByteTxRx(self.PortHandler, self.id, address)
        out.append(feedback)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.PacketHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.PacketHandler.getRxPacketError(dxl_error))
        return out
        
    def move(self,regDegree,time,type='reg'):
                
        prespos= self.read(self.ADDR_AX_PRES_POS,2)
        
        reg2Degree = regDegree*0.29
        difference = abs(prespos[0] - regDegree) * 0.29
        speed = difference/(time*0.666)
        
        if type == 'degree':
            degree = int(reg2Degree)
        if type == 'reg':
            degree = regDegree
        self.prevGoal = degree
        speed = int(speed) + 3
        if speed < 7 :
            speed = 0
        dxl_comm_result, dxl_error = self.PacketHandler.write2ByteTxRx(self.PortHandler, self.id, self.ADDR_AX_GOAL_POSITION, degree)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.PacketHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.PacketHandler.getRxPacketError(dxl_error))
        
        dxl_comm_result, dxl_error = self.PacketHandler.write2ByteTxRx(self.PortHandler, self.id, self.ADDR_AX_SPEED, int(speed))
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.PacketHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.PacketHandler.getRxPacketError(dxl_error))
        else:
            print("Dynamixel#%d move" % self.id)
            
    def moveSync(self,regDegree,time,type='reg'):
        
        prespos = self.read(self.ADDR_AX_PRES_POS,2)
        reg2Degree = regDegree*0.29
        difference = (abs(prespos[0] - regDegree)) * 0.29
        speed = difference/(time*0.666)
         
        if type == 'degree':
            degree = int(reg2Degree)
        if type == 'reg':
            degree = regDegree
        self.prevGoal = degree
        speed = int(speed)
        # if speed < 12 :
            # speed = 0
        print("id: %s degreenow: %s goal: %s speed : %s diff(degree): %.2f diff(reg): %s"%(self.id,prespos[0],regDegree,int(speed),difference,abs(prespos[0] - regDegree)))
        
        self.param = [DXL_LOBYTE(DXL_LOWORD(degree)),\
                      DXL_HIBYTE(DXL_LOWORD(degree)), \
                      DXL_LOBYTE(DXL_LOWORD(int(speed))), \
                      DXL_HIBYTE(DXL_LOWORD(int(speed)))]
        # print("id     : ", self.id, '\t', "degree : ", hex(degree), '\t',"param  : ", self.param)
    