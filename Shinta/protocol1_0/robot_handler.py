
from dynamixel_sdk import *                    # Uses Dynamixel SDK library

class Robot:
    
    def __init__(self,object_dxl,port_handler,packet_handler,sync_write):
        self.dxl = object_dxl
        self.portHandler = port_handler
        self.packetHandler = packet_handler
        self.groupSyncWrite= sync_write
     
    def cekServo(self,addr_AX_torque, data):
        for obj in self.dxl:
            dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, obj.id, addr_AX_torque, data)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            else:
                print("Dynamixel#%d has been successfully connected" % obj.id)
    def cekServoHip(self,addr_AX_torque, data):
        for i in range(11,13):
            dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, i, addr_AX_torque, data)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            else:
                print("Dynamixel#%d has been successfully connected" % i)    
    def readAllPresentPos(self):
        addr_AX_Pres_pos = 36
        for obj in self.dxl:
            obj.presentpos, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, addr_AX_Pres_pos)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            print("present position id %s is %s in degrees %f" %(obj.id,obj.presentpos,(obj.presentpos*0.29)))
    def readAll(self,address,length):
        out = []
        for obj in self.dxl:
            if length == 2 :
                feedback, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, address)
            if length == 1 :
                feedback, dxl_comm_result, dxl_error = self.packetHandler.read1ByteTxRx(self.portHandler, obj.id, address)
            out.append(feedback)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
        return out
    def syncWrite(self):
        for obj in self.dxl:
            dxl_addparam_result = self.groupSyncWrite.addParam(obj.id, obj.param)
            # print(obj.id, ": ", obj.param)
            if dxl_addparam_result != True:
                print("[ID:%03d] groupSyncWrite addparam failed" % obj.id)
                print("return: ", dxl_addparam_result, )
                
        # Syncwrite goal position
        dxl_comm_result = self.groupSyncWrite.txPacket()
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
    
        # Clear syncwrite parameter storage
        self.groupSyncWrite.clearParam()