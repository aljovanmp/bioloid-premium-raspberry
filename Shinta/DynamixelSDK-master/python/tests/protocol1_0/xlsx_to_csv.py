# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 22:19:45 2020

@author: Aljovan Magyarsa P
"""

import xlrd
import csv

def csv_from_excel():
    wb = xlrd.open_workbook('pola.xlsx')
    sh = wb.sheet_by_name('Sheet1')
    your_csv_file = open('tes.csv', 'w',newline='')
    wr = csv.writer(your_csv_file, delimiter=',', \
                    quotechar='|', quoting=csv.QUOTE_MINIMAL)

    for rownum in range(sh.nrows):
        if (rownum == 0) :
            wr.writerow(sh.row_values(rownum))
        else:
            a = []
            print("row: ", sh.row_values(rownum))
            for i in sh.row_values(rownum):
                if (i == "" or i=="*--" or i=="*++"):
                    a.append(i)
                    #print("cek")
                else:
                    a.append(int(i))
                    #print(a)
            wr.writerow(a)

    your_csv_file.close()

# runs the csv_from_excel function:
csv_from_excel()
