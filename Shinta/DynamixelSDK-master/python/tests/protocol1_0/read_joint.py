#!/usr/bin/env python
# -*- coding: utf-8 -*-

################################################################################
# Copyright 2017 ROBOTIS CO., LTD.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

# Author: Ryu Woon Jung (Leon)

#
# *********     Sync Write Example      *********
#
#
# Available Dynamixel model on this example : All models using Protocol 1.0
# This example is tested with two Dynamixel MX-28, and an USB2DYNAMIXEL
# Be sure that Dynamixel MX properties are already set as %% ID : 1 / Baudnum : 34 (Baudrate : 57600)
#

import os
import get_CSV
import time

if os.name == 'nt':
    import msvcrt
    def getch():
        return msvcrt.getch().decode()
else:
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    def getch():
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

from dynamixel_sdk import *                    # Uses Dynamixel SDK library
import servo
import robot_handler as rh
dxl = []
DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller
                                                # ex) Windows: "COM1"   Linux: "/dev/ttyUSB0" Mac: "/dev/tty.usbserial-*"
# Protocol version
PROTOCOL_VERSION            = 1.0               # See which protocol version is used in the Dynamixel

# Initialize PortHandler instance
# Set the port path
# Get methods and members of PortHandlerLinux or PortHandlerWindows
portHandler = PortHandler(DEVICENAME)


# Initialize PacketHandler instance
# Set the protocol version
# Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
packetHandler = PacketHandler(PROTOCOL_VERSION)

dxl.append(servo.Servo(509,1,portHandler,packetHandler))
dxl.append(servo.Servo(839,2,portHandler,packetHandler))
dxl.append(servo.Servo(777,3,portHandler,packetHandler))
dxl.append(servo.Servo(540,4,portHandler,packetHandler))
dxl.append(servo.Servo(525,5,portHandler,packetHandler))
dxl.append(servo.Servo(509,6,portHandler,packetHandler))
dxl.append(servo.Servo(193,7,portHandler,packetHandler))
dxl.append(servo.Servo(853,8,portHandler,packetHandler))
dxl.append(servo.Servo(500,9,portHandler,packetHandler))
dxl.append(servo.Servo(508,10,portHandler,packetHandler))


# Control table address
ADDR_AX_TORQUE_ENABLE      = 24            # Control table address is different in Dynamixel model
ADDR_AX_GOAL_POSITION      = 30
ADDR_AX_PRESENT_POSITION   = 36
ADDR_AX_MOVING             = 46

# Data Byte Length
LEN_MX_GOAL_POSITION       = 4
LEN_MX_PRESENT_POSITION    = 2

# Default setting
DXL1_ID                     = 1                 # Dynamixel#1 ID : 1
DXL2_ID                     = 2                 # Dynamixel#1 ID : 2
BAUDRATE                    = 1000000             # Dynamixel default baudrate : 57600

TORQUE_ENABLE               = 1                 # Value for enabling the torque
TORQUE_DISABLE              = 0                 # Value for disabling the torque

# Initialize GroupSyncWrite instance
CoM = []
groupSyncWrite = GroupSyncWrite(portHandler, packetHandler, ADDR_AX_GOAL_POSITION, LEN_MX_GOAL_POSITION)
robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite, CoM)

csv_name='tes_telentang.csv'

# Open port
if robot.portHandler.openPort():
    print("Succeeded to open the port")
else:
    print("Failed to open the port")
    print("Press any key to terminate...")
    getch()
    quit()

# Set port baudrate
if robot.portHandler.setBaudRate(BAUDRATE):
    print("Succeeded to change the baudrate")
else:
    print("Failed to change the baudrate")
    print("Press any key to terminate...")
    getch()
    quit()
    
# Enable Dynamixel#1 Torque
# robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)

while 1:
    print("Press any key to continue! (or press ESC to quit!)")
    if getch() == chr(0x1b):
        break
    
    print("Press e to enable or d to disable torque")
    inp = getch()
    if inp == 'e':
        robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
    if inp == 'd':
        robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
        
    robot.readAllPresentPos()
    
robot.portHandler.closePort()
