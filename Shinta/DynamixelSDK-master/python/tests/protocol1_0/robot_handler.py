from mpu6050 import mpu6050
from Kalman import KalmanAngle

from servo import *
from imu import *

from dynamixel_sdk import *                    # Uses Dynamixel SDK library
from math import *
from array import *
from numpy import *
from time import *

global integ, kalmanX, kalmanY

integ = 0
kalmanX = KalmanAngle()
kalmanY = KalmanAngle()
radToDeg = 57.2957786

speed_dir = zeros(10)
ascend = zeros(10)
prev_adc = zeros(10)
current_adc = zeros(10)
comPos = {"x":0, "y":0, "z":0}
sudut = {"s1":0, "s2":0, "s3":0, "s4":0, "s5":0, "s6":0, "s7":0, "s8":0, "s9":0, "s10":0}
sudutSwing = {"roll":0, "pitch":0}

class Robot:
    # sudut servo (penomoran sesuai urutan DH, bukan id)
    theta = [0,0,0,0,0,0,0,0,0,0]
    tDefault = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    tDefaultKiri = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    servo_rot_dir = [-1,1,1,1,-1,1,1,1,1,1]
    servo_rot_dirKiri = [-1,-1,-1,-1,-1,1,-1,-1,-1,1]

    def __init__(self,object_dxl,port_handler,packet_handler,sync_write, object_CoM):
        self.dxl = object_dxl
        self.portHandler = port_handler
        self.packetHandler = packet_handler
        self.groupSyncWrite = sync_write
        self.CoM = object_CoM

    def cekServo(self,addr_AX_torque, data):
        for obj in self.dxl:
            dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, obj.id, addr_AX_torque, data)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            else:
                print("Dynamixel#%d has been successfully connected" % obj.id)

    def readAllPresentPos(self):
        addr_AX_Pres_pos = 36
        for obj in self.dxl:
            obj.presentpos, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, addr_AX_Pres_pos)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            # print("present position id %s is %s in degrees %f" %(obj.id,obj.presentpos,(obj.presentpos*0.29)))

    def readPredPresentPos(self):
        addr_AX_Pres_pos = 36
        for obj in self.dxl:
            if obj.id == 1:
                obj.presentpos, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, addr_AX_Pres_pos)
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
                elif dxl_error != 0:
                    print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            elif obj.id == 4:
                obj.presentpos, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, addr_AX_Pres_pos)
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
                elif dxl_error != 0:
                    print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            elif obj.id == 6:
                obj.presentpos, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, addr_AX_Pres_pos)
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
                elif dxl_error != 0:
                    print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            elif obj.id == 9:
                obj.presentpos, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, addr_AX_Pres_pos)
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
                elif dxl_error != 0:
                    print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            else:
                obj.presentpos = obj.prevGoal

    def readAll(self,address,length):
        out = []
        for obj in self.dxl:
            if length == 2 :
                feedback, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, address)
            if length == 1 :
                feedback, dxl_comm_result, dxl_error = self.packetHandler.read1ByteTxRx(self.portHandler, obj.id, address)
            out.append(feedback)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
        return out

    def syncWrite(self):
        for obj in self.dxl:
            dxl_addparam_result = self.groupSyncWrite.addParam(obj.id, obj.param)
            # print(obj.id, ": ", obj.param)
            if dxl_addparam_result != True:
                print("[ID:%03d] groupSyncWrite addparam failed" % obj.id)
                print("return: ", dxl_addparam_result)

        # Syncwrite goal position
        dxl_comm_result = self.groupSyncWrite.txPacket()
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))

        # Clear syncwrite parameter storage
        self.groupSyncWrite.clearParam()

    def updateThetaDHParam(self, LR_base):
        if (LR_base == '1'):
            self.theta[0] = ((self.dxl[4].presentpos-self.dxl[4].default)*0.29*self.servo_rot_dir[0])+self.tDefault[0]
            self.theta[1] = ((self.dxl[3].presentpos-self.dxl[3].default)*0.29*self.servo_rot_dir[1])+self.tDefault[1]
            self.theta[2] = ((self.dxl[2].presentpos-self.dxl[2].default)*0.29*self.servo_rot_dir[2])+self.tDefault[2]
            self.theta[3] = ((self.dxl[1].presentpos-self.dxl[1].default)*0.29*self.servo_rot_dir[3])+self.tDefault[3]
            self.theta[4] = ((self.dxl[0].presentpos-self.dxl[0].default)*0.29*self.servo_rot_dir[4])+self.tDefault[4]
            self.theta[5] = ((self.dxl[5].presentpos-self.dxl[5].default)*0.29*self.servo_rot_dir[5])+self.tDefault[5]
            self.theta[6] = ((self.dxl[6].presentpos-self.dxl[6].default)*0.29*self.servo_rot_dir[6])+self.tDefault[6]
            self.theta[7] = ((self.dxl[7].presentpos-self.dxl[7].default)*0.29*self.servo_rot_dir[7])+self.tDefault[7]
            self.theta[8] = ((self.dxl[8].presentpos-self.dxl[8].default)*0.29*self.servo_rot_dir[8])+self.tDefault[8]
            self.theta[9] = ((self.dxl[9].presentpos-self.dxl[9].default)*0.29*self.servo_rot_dir[9])+self.tDefault[9]
            
        if (LR_base == '0'):
            self.theta[9] = ((self.dxl[4].presentpos-self.dxl[4].default)*0.29*self.servo_rot_dirKiri[0])+self.tDefaultKiri[0]
            self.theta[8] = ((self.dxl[3].presentpos-self.dxl[3].default)*0.29*self.servo_rot_dirKiri[1])+self.tDefaultKiri[1]
            self.theta[7] = ((self.dxl[2].presentpos-self.dxl[2].default)*0.29*self.servo_rot_dirKiri[2])+self.tDefaultKiri[2]
            self.theta[6] = ((self.dxl[1].presentpos-self.dxl[1].default)*0.29*self.servo_rot_dirKiri[3])+self.tDefaultKiri[3]
            self.theta[5] = ((self.dxl[0].presentpos-self.dxl[0].default)*0.29*self.servo_rot_dirKiri[4])+self.tDefaultKiri[4]
            self.theta[4] = ((self.dxl[5].presentpos-self.dxl[5].default)*0.29*self.servo_rot_dirKiri[5])+self.tDefaultKiri[5]
            self.theta[3] = ((self.dxl[6].presentpos-self.dxl[6].default)*0.29*self.servo_rot_dirKiri[6])+self.tDefaultKiri[6]
            self.theta[2] = ((self.dxl[7].presentpos-self.dxl[7].default)*0.29*self.servo_rot_dirKiri[7])+self.tDefaultKiri[7]
            self.theta[1] = ((self.dxl[8].presentpos-self.dxl[8].default)*0.29*self.servo_rot_dirKiri[8])+self.tDefaultKiri[8]
            self.theta[0] = ((self.dxl[9].presentpos-self.dxl[9].default)*0.29*self.servo_rot_dirKiri[9])+self.tDefaultKiri[9]
        self.updateMatrix(self.theta)

    def calculateTransMatrix (self, LR_base, Last_axis):
        self.updateThetaDHParam(LR_base)
        Trans_matrix = []
        if LR_base == '1':
            indexx = 0
            for i in range(0, 21):
                if i <= Last_axis:
                    Trans_matrix.append(self.transformMatrixBaseKanan[i][:][:])
                indexx+=1
        else:
            for i in range(0, 21):
                if i <= Last_axis:
                    Trans_matrix.append(self.transformMatrixBaseKiri[i][:][:])
        Trans_mat_glob = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]
        index = 0

        for i in Trans_matrix:
            next_mat = array(i)
            recent_glob = matmul(Trans_mat_glob, next_mat)
            Trans_mat_glob = recent_glob.copy()
            index+=1
        return recent_glob

    def updateMatrix(self, theta):
        #theta = [0,0,0,0,0,0,0,0,0,0,0]
        # panjang link sejajar sumbu x global
        l0 = 57
        l1 = 57
        l2 = 15
        l4 = 50

        # panjang link sejajar sumbu z global
        h0 = 32
        h1 = 75
        h2 = 14
        h3 = 61
        h4 = 45

        self.transformMatrixBaseKanan = [[[1, 0, 0, 0],
                                        [0, 1, 0, 0],
                                        [0, 0, 1, 0],
                                        [0, 0, 0, 1]],
                                    [[1, 0, 0, -l0],
                                        [0, 1, 0, 0],
                                        [0, 0, 1, 0],
                                        [0, 0, 0, 1]],
                                    [[0, 0, -1, 0],
                                        [1, 0, 0, 0],
                                        [0, -1, 0, h0],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(-90+theta[0])), 0, -sin(radians(-90+theta[0])), 0], 
                                        [sin(radians(-90+theta[0])), 0, cos(radians(-90+theta[0])), 0], 
                                        [0, -1, 0, -l1],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(theta[1])), 0, -sin(radians(theta[1])), h1*cos(radians(theta[1]))], 
                                        [sin(radians(theta[1])), 0, cos(radians(theta[1])), h1*sin(radians(theta[1]))], 
                                        [0, -1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[1, 0, 0, 0],
                                        [0, 0, -1, 0],
                                        [0, 1, 0, -l2],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(theta[2])), 0, -sin(radians(theta[2])), h2*cos(radians(theta[2]))],
                                        [sin(radians(theta[2])), 0, cos(radians(theta[2])), h2*sin(radians(theta[2]))],
                                        [0, -1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[0, 0, 1, 0],
                                        [-1, 0, 0, 0],
                                        [0, -1, 0, l2],
                                        [0, 0, 0, 1]],
                                    [[0, 0, 1, 0],
                                        [1, 0, 0, 0],
                                        [0, 1, 0, h3],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(90+theta[3])), 0, -sin(radians(90+theta[3])), h4*cos(radians(90+theta[3]))],
                                        [sin(radians(90+theta[3])), 0, cos(radians(90+theta[3])), h4*sin(radians(90+theta[3]))],
                                        [0, -1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(90+theta[4])), 0, sin(radians(90+theta[4])), -l4*cos(radians(90+theta[4]))],
                                        [sin(radians(90+theta[4])), 0, -cos(radians(90+theta[4])), -l4*sin(radians(90+theta[4]))],
                                        [0, 1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[1, 0, 0, -l4],
                                        [0, 0, 1, 0],
                                        [0, -1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(-90+theta[5])), 0, sin(radians(-90+theta[5])), -h4*cos(radians(-90+theta[5]))],
                                        [sin(radians(-90+theta[5])), 0, -cos(radians(-90+theta[5])), -h4*sin(radians(-90+theta[5]))],
                                        [0, 1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(theta[6])), -sin(radians(theta[6])), 0, -h3*cos(radians(theta[6]))],
                                        [sin(radians(theta[6])), cos(radians(theta[6])), 0, -h3*sin(radians(theta[6]))],
                                        [0, 0, 1, 0],
                                        [0, 0, 0, 1]],
                                    [[0, 0, -1, 0],
                                        [-1, 0, 0, 0],
                                        [0, 1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[-1, 0, 0, l2],
                                        [0, 0, 1, 0],
                                        [0, 1, 0, h2],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(180+theta[7])), 0, sin(radians(180+theta[7])), -l2*cos(radians(180+theta[7]))],
                                        [sin(radians(180+theta[7])), 0, -cos(radians(180+theta[7])), -l2*sin(radians(180+theta[7]))],
                                        [0, 1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[1, 0, 0, 0],
                                        [0, 0, 1, 0],
                                        [0, -1, 0, h1],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(180+theta[8])), 0, sin(radians(180+theta[8])), -l1*cos(radians(180+theta[8]))],
                                        [sin(radians(180+theta[8])), 0, -cos(radians(180+theta[8])), -l1*sin(radians(180+theta[8]))],
                                        [0, 1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[1, 0, 0, 0],
                                        [0, 0, 1, 0],
                                        [0, -1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[0, 0, -1, 0],
                                        [-1, 0, 0, 0],
                                        [0, 1, 0, 0],
                                        [0, 0, 0, 1]],
                                    [[cos(radians(theta[9])), -sin(radians(theta[9])), 0, -h0*cos(radians(theta[9]))],
                                        [sin(radians(theta[9])), cos(radians(theta[9])), 0, -h0*sin(radians(theta[9]))],
                                        [0, 0, 1, 0],
                                        [0, 0, 0, 1]]]

        self.transformMatrixBaseKiri = [[[1, 0, 0, 0],
                                [0, 1, 0, 0],
                                [0, 0, 1, 0],
                                [0, 0, 0, 1]],
                            [[1, 0, 0, -l0],
                                [0, 1, 0, 0],
                                [0, 0, 1, 0],
                                [0, 0, 0, 1]],
                           [[0, 0, -1, 0],
                                [1, 0, 0, 0],
                                [0, -1, 0, h0],
                                [0, 0, 0, 1]],
                           [[cos(radians(-90+theta[0])), 0, -sin(radians(-90+theta[0])), 0],
                                [sin(radians(-90+theta[0])), 0, cos(radians(-90+theta[0])), 0],
                                [0, -1, 0, -l1],
                                [0, 0, 0, 1]],
                           [[cos(radians(theta[1])), 0, -sin(radians(theta[1])), h1*cos(radians(theta[1]))],
                                [sin(radians(theta[1])), 0, cos(radians(theta[1])), h1*sin(radians(theta[1]))],
                                [0, -1, 0, 0],
                                [0, 0, 0, 1]],
                           [[1, 0, 0, 0],
                                [0, 0, -1, 0],
                                [0, 1, 0, -l2],
                                [0, 0, 0, 1]],
                           [[cos(radians(theta[2])), 0, -sin(radians(theta[2])), h2*cos(radians(theta[2]))],
                                [sin(radians(theta[2])), 0, cos(radians(theta[2])), h2*sin(radians(theta[2]))],
                                [0, -1, 0, 0],
                                [0, 0, 0, 1]],
                           [[0, 0, 1, 0],
                                [-1, 0, 0, 0],
                                [0, -1, 0, l2],
                                [0, 0, 0, 1]],
                           [[0, 0, 1, 0],
                                [1, 0, 0, 0],
                                [0, 1, 0, h3],
                                [0, 0, 0, 1]],
                           [[cos(radians(90+theta[3])), 0, -sin(radians(90+theta[3])), h4*cos(radians(90+theta[3]))],
                                [sin(radians(90+theta[3])), 0, cos(radians(90+theta[3])), h4*sin(radians(90+theta[3]))],
                                [0, -1, 0, 0],
                                [0, 0, 0, 1]],
                           [[cos(radians(90+theta[4])), 0, sin(radians(90+theta[4])), l4*cos(radians(90+theta[4]))],
                                [sin(radians(90+theta[4])), 0, -cos(radians(90+theta[4])), l4*sin(radians(90+theta[4]))],
                                [0, 1, 0, 0],
                                [0, 0, 0, 1]],
                           [[1, 0, 0, l4],
                                [0, 0, 1, 0],
                                [0, -1, 0, 0],
                                [0, 0, 0, 1]],
                           [[cos(radians(-90+theta[5])), 0, sin(radians(-90+theta[5])), -h4*cos(radians(-90+theta[5]))],
                                [sin(radians(-90+theta[5])), 0, -cos(radians(-90+theta[5])), -h4*sin(radians(-90+theta[5]))],
                                [0, 1, 0, 0],
                                [0, 0, 0, 1]],
                           [[cos(radians(theta[6])), -sin(radians(theta[6])), 0, -h3*cos(radians(theta[6]))],
                                [sin(radians(theta[6])), cos(radians(theta[6])), 0, -h3*sin(radians(theta[6]))],
                                [0, 0, 1, 0],
                                [0, 0, 0, 1]],
                           [[0, 0, -1, 0],
                                [-1, 0, 0, 0],
                                [0, 1, 0, 0],
                                [0, 0, 0, 1]],
                           [[-1, 0, 0, l2],
                                [0, 0, 1, 0],
                                [0, 1, 0, h2],
                                [0, 0, 0, 1]],
                           [[cos(radians(180+theta[7])), 0, sin(radians(180+theta[7])), -l2*cos(radians(180+theta[7]))],
                                [sin(radians(180+theta[7])), 0, -cos(radians(180+theta[7])), -l2*sin(radians(180+theta[7]))],
                                [0, 1, 0, 0],
                                [0, 0, 0, 1]],
                           [[1, 0, 0, 0],
                                [0, 0, 1, 0],
                                [0, -1, 0, h1],
                                [0, 0, 0, 1]],
                           [[cos(radians(180+theta[8])), 0, sin(radians(180+theta[8])), -l1*cos(radians(180+theta[8]))],
                                [sin(radians(180+theta[8])), 0, -cos(radians(180+theta[8])), -l1*sin(radians(180+theta[8]))],
                                [0, 1, 0, 0],
                                [0, 0, 0, 1]],
                           [[1, 0, 0, 0],
                                [0, 0, 1, 0],
                                [0, -1, 0, 0],
                                [0, 0, 0, 1]],
                           [[0, 0, -1, 0],
                                [-1, 0, 0, 0],
                                [0, 1, 0, 0],
                                [0, 0, 0, 1]],
                           [[cos(radians(theta[9])), -sin(radians(theta[9])), 0, -h0*cos(radians(theta[9]))],
                                [sin(radians(theta[9])), cos(radians(theta[9])), 0, -h0*sin(radians(theta[9]))],
                                [0, 0, 1, 0],
                                [0, 0, 0, 1]]]

    def com(self, LR_base):
        x = 0
        y = 0
        z = 0
        m = 0
        self.readAllPresentPos()
        for obj in self.CoM:
            # print('===============================================')
            part = obj.cek(LR_base)
            # print()
            # print("Part: ", obj.Link, " CoM:")
            # print(part)
            x_next = obj.Coord_from_glob[0].copy()
            y_next = obj.Coord_from_glob[1].copy()
            z_next = obj.Coord_from_glob[2].copy()
            m_next = obj.mass
            # print("Coordinat Before: ")
            # print('x: ', x, 'y: ', y, 'z: ', z, 'm: ', m)
            # print()
            #print("Next : ")
            #print('x: ', x_next, 'y: ', y_next, 'z: ', z_next, 'm: ', m_next)
            x = around(((x*m) + (x_next*m_next)) / (m+m_next),3)
            y = around(((y*m) + (y_next*m_next)) / (m+m_next),3)
            z = around(((z*m) + (z_next*m_next)) / (m+m_next),3)
            m = round((m + m_next),3)
            comPos["x"], comPos["y"], comPos["z"] = x, y, z

    def comPrediction(self, LR_base):
        x = 0
        y = 0
        z = 0
        m = 0
        self.readPredPresentPos()
        for obj in self.CoM:
            # print('===============================================')
            part = obj.cek(LR_base)
            # print()
            # print("Part: ", obj.Link, " CoM:")
            # print(part)
            x_next = obj.Coord_from_glob[0].copy()
            y_next = obj.Coord_from_glob[1].copy()
            z_next = obj.Coord_from_glob[2].copy()
            m_next = obj.mass
            # print("Coordinat Before: ")
            # print('x: ', x, 'y: ', y, 'z: ', z, 'm: ', m)
            # print()
            #print("Next : ")
            #print('x: ', x_next, 'y: ', y_next, 'z: ', z_next, 'm: ', m_next)
            x = around(((x*m) + (x_next*m_next)) / (m+m_next),3)
            y = around(((y*m) + (y_next*m_next)) / (m+m_next),3)
            z = around(((z*m) + (z_next*m_next)) / (m+m_next),3)
            m = round((m + m_next),3)
            comPos["x"], comPos["y"], comPos["z"] = x, y, z

    def inverseKinematics(self,xGoal,yGoal,zGoal):
        HiptoKnee = 8
        KneetoAnkle = 7.5
        HiptoAnkle = sqrt(xGoal**2+yGoal**2+zGoal**2)
        theta1a = degrees(atan(xGoal/zGoal))
        theta1b = degrees(acos((HiptoKnee**2 + HiptoAnkle**2 - KneetoAnkle**2)/(2*HiptoKnee*HiptoAnkle)))
        thetak = degrees(acos((KneetoAnkle**2 + HiptoKnee**2 - HiptoAnkle**2)/(2*KneetoAnkle*HiptoKnee)))
        theta2 = 180 - thetak
        theta3 = theta1a + thetak + theta1b - 180
        theta4 = degrees(atan(yGoal/zGoal))
        theta5 = theta4

        # print(theta1a+theta1b,theta2,theta3)
        # print(254+theta1a+theta1b,222-theta2,156+theta3) #Base kiri, swing kanan
        # print(56-theta1a-theta1b,173+theta2,138-theta3) #Base kanan, swing kiri

        # print(theta4,theta5)
        # print(150-theta4,147-theta5) #Base kiri, swing kanan
        # print(150+theta4,151+theta5) #Base kanan, swing kiri

        degtoadc = 3.44827586207
        sudut1 = int(degtoadc*(150-theta4))
        sudut2 = int(degtoadc*(254+theta1a+theta1b))
        sudut3 = int(degtoadc*(222-theta2))
        sudut4 = int(degtoadc*(156-theta3))
        sudut5 = int(degtoadc*(147-theta5))
        # print(sudut1, sudut2, sudut3, sudut4, sudut5) #Base kiri, swing kanan

        sudut6 = int(degtoadc*(150+theta4))
        sudut7 = int(degtoadc*(56-theta1a-theta1b))
        sudut8 = int(degtoadc*(173+theta2))
        sudut9 = int(degtoadc*(138+theta3))
        sudut10 = int(degtoadc*(151+theta5))
        sudut["s1"],sudut["s2"],sudut["s3"],sudut["s4"],sudut["s5"],sudut["s6"],sudut["s7"],sudut["s8"],sudut["s9"],sudut["s10"], = sudut1,sudut2,sudut3,sudut4,sudut5,sudut6,sudut7,sudut8,sudut9,sudut10

    def walkInit(self, xGoal, yGoal, zGoal, tsup, LR_base):
        self.com(LR_base)
        print("COMnow: ", comPos)
        tc = sqrt(zGoal*0.101936*0.01) #konstanta waktu lintasan CoM
        C = cosh(tsup/tc)
        S = sinh(tsup/tc)
        vX = (-xGoal*(C+1)/(tc*S))
        vY = (yGoal*(1-C)/(tc*S))

        state1Roll = degrees(atan(yGoal/zGoal))
        state1Pitch = degrees(atan(xGoal/zGoal))
        return vX, vY, state1Roll, state1Pitch
    
    def calculatePattern(self, timeNow, vX, vY):
        tc = sqrt(comPos["z"]*0.101936*0.01) #konstanta waktu lintasan CoM
        
        vX_timeNow = (comPos["x"]*sinh(timeNow/tc)/tc) + (vX*cosh(timeNow/tc))
        vY_timeNow = (comPos["y"]*sinh(timeNow/tc)/tc) - (vY*cosh(timeNow/tc))
        
        # posisi tujuan pattern saat timeNow
        x_timeNow = (comPos["x"]*cosh(timeNow/tc)) + (tc*vX*sinh(timeNow/tc))
        y_timeNow = (comPos["y"]*cosh(timeNow/tc)) + (tc*(vY)*sinh(timeNow/tc))
        return(x_timeNow, y_timeNow)
    
    def walkUpdate(self, x_timeNow, y_timeNow):
        thetaRollTujuan = degrees(atan(x_timeNow/comPos["z"]))
        thetaPitchTujuan = degrees(atan(y_timeNow/comPos["z"]))
        # print("theta roll, pitch: ", thetaRollTujuan, thetaPitchTujuan)
        
        degtoadc = 3.44827586207
        # sudut1 = int(degtoadc*(150-(thetaRollTujuan+5)))
        sudut4 = int(degtoadc*(156+thetaPitchTujuan))
        sudut5 = int(degtoadc*(147-thetaRollTujuan))
        # sudut6 = int(degtoadc*(150+thetaPitchTujuan))
        sudut9 = int(degtoadc*(138-thetaPitchTujuan))
        sudut10 = int(degtoadc*(151+thetaRollTujuan))
        sudutSwingRoll = int(degtoadc*(thetaRollTujuan+1))
        sudutSwingPitch = int(degtoadc*(thetaPitchTujuan-5))
        sudut["s4"],sudut["s5"],sudut["s9"],sudut["s10"], = sudut4,sudut5,sudut9,sudut10
        sudutSwing["roll"],sudutSwing["pitch"] = sudutSwingRoll,sudutSwingPitch

    def mainProgram(self, LR_base, tsup, start_time, current_time, state_print, state_pola):
        if LR_base == '1':
            if ((time() - current_time >= 0) and (time() - current_time <= 2.2)):
                timeNow = time() - start_time
                # if(time() - timeEnd >= 0.5):
                #     posisiTujuan = self.calculatePattern(time() - start_time, vX, vY)
                #     print("posisi tujuan: ", posisiTujuan)
                #     x_timeNow = posisiTujuan[0]
                #     y_timeNow = posisiTujuan[1]
                #     timeEnd = time()
                if (timeNow >= 0) and (timeNow <= (tsup/4)) and state_pola == 1:
                    x_timeNow = -50.9184468
                    y_timeNow = 59.64767027
                    self.walkUpdate(x_timeNow, y_timeNow)
                    
                    self.comPrediction(LR_base)
                    print("COM Prediction: ", comPos)
                    
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[8].moveSync(472-sudutSwing["pitch"],tsup/4)
                    self.dxl[9].moveSync(516-sudutSwing["roll"],tsup/4)
                    self.syncWrite()
                    state_print = 1
                    state_pola = 2
                elif (timeNow >= (tsup/4)) and (timeNow <= (tsup/2) and state_pola == 2):
                    x_timeNow = -13.41625193 # + kiri
                    y_timeNow = 18.33908851 # + maju
                    sfX = -3
                    sfY = 3
                    sfZ = 12
                    
                    self.inverseKinematics(sfX,sfY,sfZ)
                    listsudut = list(sudut.values())
                    for i in range(0,10):
                        if listsudut[i] > 1023:
                            listsudut[i] = 1023

                    self.walkUpdate(x_timeNow, y_timeNow)
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[5].moveSync(listsudut[5],tsup/4)
                    self.dxl[6].moveSync(listsudut[6],tsup/4)
                    self.dxl[7].moveSync(listsudut[7],tsup/4)
                    self.dxl[8].moveSync(listsudut[8],tsup/4)
                    self.dxl[9].moveSync(listsudut[9],tsup/4)
                    self.syncWrite()
                    state_print = 1
                    state_pola = 3
                elif (timeNow >= (tsup/2)) and (timeNow <= (3*tsup/4) and state_pola == 3):
                    x_timeNow = 0.12178808
                    y_timeNow = 9.79818526
                    sfX = 0
                    sfY = 3
                    sfZ = 12

                    self.inverseKinematics(sfX,sfY,sfZ)
                    listsudut = list(sudut.values())
                    for i in range(0,10):
                        if listsudut[i] > 1023:
                            listsudut[i] = 1023

                    self.walkUpdate(x_timeNow, y_timeNow)
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[5].moveSync(listsudut[5],tsup/4)
                    self.dxl[6].moveSync(listsudut[6],tsup/4)
                    self.dxl[7].moveSync(listsudut[7],tsup/4)
                    self.dxl[8].moveSync(listsudut[8],tsup/4)
                    self.dxl[9].moveSync(listsudut[9],tsup/4)
                    self.syncWrite()
                    state_print = 1
                    state_pola = 4
                elif (timeNow >= (3*tsup/4) and (timeNow <= tsup) and state_pola == 4):
                    x_timeNow = 13.87219371
                    y_timeNow = 18.78656812
                    sfX = 6
                    sfY = 3
                    sfZ = 12

                    self.inverseKinematics(sfX,sfY,sfZ)
                    listsudut = list(sudut.values())
                    for i in range(0,10):
                        if listsudut[i] > 1023:
                            listsudut[i] = 1023

                    self.walkUpdate(x_timeNow, y_timeNow)
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[5].moveSync(listsudut[5],tsup/4)
                    self.dxl[6].moveSync(listsudut[6],tsup/4)
                    self.dxl[7].moveSync(listsudut[7],tsup/4)
                    self.dxl[8].moveSync(listsudut[8],tsup/4)
                    self.dxl[9].moveSync(listsudut[9],tsup/4)
                    self.syncWrite()
                    state_print = 1
                    state_pola = 5
                elif (timeNow >= tsup and state_pola == 5):
                    x_timeNow = 52.42944584
                    y_timeNow = 61.37062545
                    sfX = 8
                    sfY = 0
                    sfZ = 13.2

                    self.inverseKinematics(sfX,sfY,sfZ)
                    listsudut = list(sudut.values())
                    for i in range(0,10):
                        if listsudut[i] > 1023:
                            listsudut[i] = 1023

                    self.walkUpdate(x_timeNow, y_timeNow)
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[5].moveSync(listsudut[5],tsup/4)
                    self.dxl[6].moveSync(listsudut[6],tsup/4)
                    self.dxl[7].moveSync(listsudut[7],tsup/4)
                    self.dxl[8].moveSync(listsudut[8],tsup/4)
                    self.dxl[9].moveSync(listsudut[9],tsup/4)
                    self.syncWrite()
                    # self.dxl[8].moveSync(sudut[9]-sudutSwingPitch,tsup/4)
                    # self.dxl[9].moveSync(sudut[10]-sudutSwingRoll,tsup/4)
                    LR_base = '0'
                    current_time = time()
                    timeNow = 0
                    start_time = time()
                    self.syncWrite()
                    state_print = 1
                    state_pola = 1
                if state_print == 1:
                    print("timeNow: ", timeNow)
                    print("LR_base: ", LR_base, "posisi tujuan: ", x_timeNow, y_timeNow)
                    current_time = time()
                    state_print = 0
                    # self.com(LR_base)
                    # print (comPos)
        if LR_base == '0':
            if ((time() - current_time >= 0) and (time() - current_time <= 2.5)):
                # posisiTujuan = self.calculatePattern(time() - start_time, vX, vY, comInX, comInY, comInZ)
                # print("posisi tujuan: ", posisiTujuan)
                # x_timeNow = posisiTujuan[0]
                # y_timeNow = posisiTujuan[1]
                timeNow = time() - start_time
                if (timeNow >= 0) and (timeNow <= (tsup/4)) and state_pola == 1:
                    x_timeNow = -12.81045597
                    y_timeNow = 50.09391997
                    self.walkUpdate(x_timeNow, y_timeNow)
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[8].moveSync(472-sudutSwing["pitch"],tsup/4)
                    self.dxl[9].moveSync(516-sudutSwing["roll"],tsup/4)
                    self.syncWrite()
                    state_print = 1
                    state_pola = 2
                elif (timeNow >= (tsup/4)) and (timeNow <= (tsup/2) and state_pola == 2):
                    x_timeNow = -12.81045597 - 40 # + kiri
                    y_timeNow = 50.09391997 + 30 # + maju
                    sfX = -3
                    sfY = 3
                    sfZ = 12
                    
                    self.inverseKinematics(sfX,sfY,sfZ)
                    listsudut = list(sudut.values())
                    for i in range(0,10):
                        if listsudut[i] > 1023:
                            listsudut[i] = 1023
                    self.walkUpdate(x_timeNow, y_timeNow)
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[5].moveSync(listsudut[5],tsup/4)
                    self.dxl[6].moveSync(listsudut[6],tsup/4)
                    self.dxl[7].moveSync(listsudut[7],tsup/4)
                    self.dxl[8].moveSync(listsudut[8],tsup/4)
                    self.dxl[9].moveSync(listsudut[9],tsup/4)
                    # self.syncWrite()
                    state_print = 1
                    state_pola = 3
                elif (timeNow >= (tsup/2)) and (timeNow <= (3*tsup/4) and state_pola == 3):
                    x_timeNow = 0.05609366 - 50
                    y_timeNow = 7.47681743 + 60
                    sfX = 0
                    sfY = 3
                    sfZ = 12

                    self.inverseKinematics(sfX,sfY,sfZ)
                    listsudut = list(sudut.values())
                    for i in range(0,10):
                        if listsudut[i] > 1023:
                            listsudut[i] = 1023
                    self.walkUpdate(x_timeNow, y_timeNow)
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[5].moveSync(listsudut[5],tsup/4)
                    self.dxl[6].moveSync(listsudut[6],tsup/4)
                    self.dxl[7].moveSync(listsudut[7],tsup/4)
                    self.dxl[8].moveSync(listsudut[8],tsup/4)
                    self.dxl[9].moveSync(listsudut[9],tsup/4)
                    # self.syncWrite()
                    state_print = 1
                    state_pola = 4
                elif (timeNow >= (3*tsup/4) and (timeNow <= tsup) and state_pola == 4):
                    x_timeNow = 3.45058646 - 60
                    y_timeNow = 15.29275429 + 60
                    sfX = 6
                    sfY = 3
                    sfZ = 12

                    self.inverseKinematics(sfX,sfY,sfZ)
                    listsudut = list(sudut.values())
                    for i in range(0,10):
                        if listsudut[i] > 1023:
                            listsudut[i] = 1023
                    self.walkUpdate(x_timeNow, y_timeNow)
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[5].moveSync(listsudut[5],tsup/4)
                    self.dxl[6].moveSync(listsudut[6],tsup/4)
                    self.dxl[7].moveSync(listsudut[7],tsup/4)
                    self.dxl[8].moveSync(listsudut[8],tsup/4)
                    self.dxl[9].moveSync(listsudut[9],tsup/4)
                    # self.syncWrite()
                    state_print = 1
                    state_pola = 5
                elif (timeNow >= tsup and state_pola == 5):
                    x_timeNow = 13.68881275 - 40
                    y_timeNow = 53.45458987 + 15
                    sfX = 8
                    sfY = 0
                    sfZ = 13.2

                    self.inverseKinematics(sfX,sfY,sfZ)
                    listsudut = list(sudut.values())
                    for i in range(0,10):
                        if listsudut[i] > 1023:
                            listsudut[i] = 1023
                    self.walkUpdate(x_timeNow, y_timeNow)
                    self.dxl[3].moveSync(sudut["s4"],tsup/4)
                    self.dxl[4].moveSync(sudut["s5"],tsup/4)
                    self.dxl[5].moveSync(listsudut[5],tsup/4)
                    self.dxl[6].moveSync(listsudut[6],tsup/4)
                    self.dxl[7].moveSync(listsudut[7],tsup/4)
                    self.dxl[8].moveSync(sudut[8]-sudutSwing["pitch"],tsup/4)
                    self.dxl[9].moveSync(sudut[9]-sudutSwing["roll"],tsup/4)
                    LR_base = '1'
                    current_time = time()
                    timeNow = 0
                    start_time = time()
                    # self.syncWrite()
                    state_print = 1
                    state_pola = 1
                if state_print == 1:
                    print("timeNow: ", timeNow)
                    print("LR_base: ", LR_base, "posisi tujuan: ", x_timeNow, y_timeNow)
                    current_time = time()
                    state_print = 0
        return LR_base, state_pola, state_print

    def controlBerdiri(self, timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch, state1Pitch):
        global integ

        state1bPitch = state1Pitch
        timer = time()
        roll, pitch, timer = get_robot_ori(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch)
        state1Pitch = radians(pitch)
        wakt3 = 0.191972017288208 #didapat dari selisih time() sebelum memanggil fungsi dan setelah memanggil fungsi

        state2Pitch = (state1Pitch - state1bPitch)/wakt3
        
        Kberdiri = [[1.7489,0.3204,0,0], [0,0,1.7410,0.3112]]
        integ = self.integrator(state1Pitch, integ)
        uPitch = (Kberdiri[0][0] * (state1Pitch + integ)) + (Kberdiri[0][1] * state2Pitch)
        # alphaPitch = degrees((-uPitch + 1.292*9.81*22*sin(state1Pitch))/0.040761126) #shinta
        alphaPitch = degrees((-uPitch + 1.292*9.81*0.22*sin(state1Pitch))/0.03969209)

        deltaSudut = degrees(state2Pitch * wakt3) + (alphaPitch * wakt3 * wakt3 / 2)
        deltaKecSudut = degrees(state2Pitch) + alphaPitch * wakt3 # rad/s to deg/s
        
        posisi_kanan = self.dxl[3].dxlDeg() + deltaSudut
        posisi_kiri = self.dxl[8].dxlDeg() - deltaSudut
        rpm_servo = deltaKecSudut * 0.1667
        
        # print ("posisi_kanan: ", posisi_kanan)
        # print ("posisi_kiri: ", posisi_kiri)

        self.dxl[3].dxlMove2(posisi_kanan, rpm_servo)
        self.dxl[8].dxlMove2(posisi_kiri, rpm_servo)

        # print ("Position: ", posisi_kanan)
        # deg_kanan = posisi_kanan[0]*0.29296875
        # deg_kiri = posisi_kiri[0]*0.29296875
        # print ("deg_kanan : ", deg_kanan)

        # deg_kanan = posisi_kanan[0]*0.29296875 + deltaSudut
        # deg_kiri = posisi_kiri[0]*0.29296875 + deltaSudut
        # print ("deg_kanan+delta : ", deg_kanan)
        # print ("deltaKecSudut : %.3f rpm, %.3d bit" % (deltaKecSudut*0.1667, int(deltaKecSudut*0.1667*9.009)))

        # rpm_servo = deltaKecSudut * 0.1667
        # self.dxl[3].moveDeg_with_rpm(deg_kanan, rpm_servo)
        # self.dxl[8].moveDeg_with_rpm(deg_kiri, rpm_servo)
        # self.dxl[2].move(-90,2,'degree')
        # while 1:
        #     print("ok")
        
        # Kec
        # self.dxl[2].move(-deltaSudut,deltaKecSudut,'degree')
        
        # print("dt: ", dt)
        # print("state1bPitch: ", state1bPitch)
        # print("state1Pitch: ", state1Pitch)

        # print ("state1bPitch - deg : ", degrees(state1bPitch))
        # print ("state1Pitch - deg: ", degrees(state1Pitch))
        # print ("state2Pitch - deg/s: ", degrees(state2Pitch))
        # print("uPitch: ", uPitch)
        # print(1.292*9.81*22*sin(state1Pitch))
        # print("alphaPitch: ", alphaPitch)
        # print("posisi_kanan: ", posisi_kanan)
        # print("posisi_kiri: ", posisi_kiri)
        # print("deltaKecSudut: ", deltaKecSudut)
        # print("deltaSudut2: ", deltaSudut2)
        # print("=================================================================================")
        return state1Pitch

    def integrator(self, state1IMU, integ):
        ki = 0.04
        dt = 0.0415
        temp = ki * (state1IMU - 0) * dt
        integ = integ + temp
        return integ

    def control(self, x_timeNow, y_timeNow, LR_base, state1Roll, state1Pitch):
        self.com(LR_base)
        print("x_timeNow: ", x_timeNow, "y_timeNow: ", y_timeNow)
        print("COMnow: ", comPos)

        waktu = 0.06
        state1bRoll = state1Roll
        state1Roll = degrees(atan(comPos["y"]/comPos["z"]))
        state2Roll = (state1Roll - state1bRoll)/waktu

        state1bPitch = state1Pitch
        state1Pitch = degrees(atan(comPos["x"]/comPos["z"]))
        state2Pitch = (state1Pitch - state1bPitch)/waktu

        refRoll = degrees(atan(y_timeNow/comPos["z"]))
        refPitch = degrees(atan(x_timeNow/comPos["z"]))

        K = [[1.7489,0.3204,0,0], [0,0,1.7410,0.3112]]
        # K = [[1.2758,0.1872,0,0], [0,0,1.2658,0.1737]]
        # K = [[18.8267,2.0211,0,0], [0,0,23.7539,1.8181]]
        uRoll = (K[1][2]*(state1Roll - refRoll)) + (K[1][3]*(state2Roll - 0))
        uPitch = (K[0][0]*(state1Pitch - refPitch)) + (K[0][1]*(state2Pitch - 0))
        
        alphaRoll = degrees((-uRoll + (1.292*9.80665*(comPos["z"]/1000)*sin(radians(state1Roll))))/0.03969209)
        alphaPitch = degrees((-uPitch + (1.292*9.8066*(comPos["z"]/1000)*sin(radians(state1Pitch))))/0.040761126)

        deltaRoll = degrees(state2Roll*waktu) + alphaRoll*waktu*waktu/2
        deltaPitch = degrees(state2Pitch*waktu) + alphaPitch*waktu*waktu/2
        
        deltaKecSudutRoll = degrees(state2Roll) + alphaRoll * waktu # rad/s to deg/s
        deltaKecSudutPitch = degrees(state2Pitch) + alphaPitch * waktu
        
        rpm_servoRoll = deltaKecSudutRoll * 0.1667
        rpm_servoPitch = deltaKecSudutPitch * 0.1667

        posisi_kananRoll = self.dxl[4].dxlDeg() - deltaRoll
        posisi_kiriRoll = self.dxl[9].dxlDeg() + deltaRoll
        posisi_kananPitch = self.dxl[3].dxlDeg() - deltaPitch
        posisi_kiriPitch = self.dxl[8].dxlDeg() + deltaPitch

        # self.dxl[4].dxlMove2(posisi_kananRoll, rpm_servoRoll)
        # self.dxl[9].dxlMove2(posisi_kiriRoll, rpm_servoRoll)
        self.dxl[3].dxlMove2(posisi_kananPitch, rpm_servoPitch)
        self.dxl[8].dxlMove2(posisi_kiriPitch, rpm_servoPitch)

        # print ("posisi_kananRoll: ", posisi_kananRoll)
        # print ("posisi_kiriRoll: ", posisi_kiriRoll)
        # print ("posisi_kananPitch: ", posisi_kananPitch)
        # print ("posisi_kiriPitch: ", posisi_kiriPitch)

        # print("refRoll   : %.3f refPitch   : %.3f" %(refRoll             , refPitch))
        # print("state1Roll: %.3f state1Pitch: %.3f" %(state1Roll          , state1Pitch))
        # print("state2Roll: %.3f state2Pitch: %.3f" %(state2Roll          , state2Pitch))
        # print("uRoll     : %.3f uPitch     : %.3f" %(uRoll               , uPitch))
        # print("errorRoll : %.3f errorPitch : %.3f" %(state1Roll - refRoll, state1Pitch - refPitch))
        # print("alphaRoll : %.3f alphaPitch : %.3f" %(alphaRoll           , alphaPitch))
        print("deltaRoll : %.3f deltaPitch : %.3f" %(deltaRoll           , deltaPitch))
        # print("T2alphaRoll: %.3f"%(1.292*9.80665*(comInZ/1000)*sin(radians(state1Roll))))
        return state1Roll, state1Pitch

class CoM_Class:
    def __init__(self,Last_axis,CoM_Coordinate,Link_mass,Robot_obj):
        self.Last_a = Last_axis
        self.CoM_Coord = CoM_Coordinate
        self.mass = Link_mass
        self.Robot = Robot_obj
    
    def cek(self,LR_base):
        TM_from_glob = self.Robot.calculateTransMatrix(LR_base,self.Last_a)
        TM = array(TM_from_glob)
        Coord = array(self.CoM_Coord)
        #print()
        #print("Trans to: ", self.Last_a)
        #print(TM)
        #print("coo: ")
        self.Coord_from_glob = matmul(TM,Coord)
        # print(self.Coord_from_glob)
        return(self.Coord_from_glob)
