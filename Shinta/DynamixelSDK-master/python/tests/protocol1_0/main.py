import os
import time
import csv

if os.name == 'nt':
    import msvcrt
    def getch():
        return msvcrt.getch().decode()
else:
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    def getch():
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

from dynamixel_sdk import *                    # Uses Dynamixel SDK library
from numpy import *
from math import *
import servo
import robot_handler as rh
import imu as imu

dxl = []
DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller
                                                # ex) Windows: "COM1"   Linux: "/dev/ttyUSB0" Mac: "/dev/tty.usbserial-*"
# Protocol version
PROTOCOL_VERSION            = 1.0               # See which protocol version is used in the Dynamixel

# Initialize PortHandler instance
# Set the port path
# Get methods and members of PortHandlerLinux or PortHandlerWindows
portHandler = PortHandler(DEVICENAME)


# Initialize PacketHandler instance
# Set the protocol version
# Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
packetHandler = PacketHandler(PROTOCOL_VERSION)

# Control table address
ADDR_AX_TORQUE_ENABLE      = 24            # Control table address is different in Dynamixel model
ADDR_AX_GOAL_POSITION      = 30
ADDR_AX_PRESENT_POSITION   = 36
ADDR_AX_MOVING             = 46

# Data Byte Length
LEN_MX_GOAL_POSITION       = 4
LEN_MX_PRESENT_POSITION    = 2

# Default setting
DXL1_ID                     = 1                 # Dynamixel#1 ID : 1
DXL2_ID                     = 2                 # Dynamixel#1 ID : 2
BAUDRATE                    = 1000000             # Dynamixel default baudrate : 57600

TORQUE_ENABLE               = 1                 # Value for enabling the torque
TORQUE_DISABLE              = 0                 # Value for disabling the torque

#Posisi default kaki lurus
dxl.append(servo.Servo(514,1,portHandler,packetHandler))
dxl.append(servo.Servo(821,2,portHandler,packetHandler))
dxl.append(servo.Servo(792,3,portHandler,packetHandler))
dxl.append(servo.Servo(548,4,portHandler,packetHandler))
dxl.append(servo.Servo(524,5,portHandler,packetHandler))
dxl.append(servo.Servo(505,6,portHandler,packetHandler))
dxl.append(servo.Servo(211,7,portHandler,packetHandler))
dxl.append(servo.Servo(525,8,portHandler,packetHandler))
dxl.append(servo.Servo(484,9,portHandler,packetHandler))
dxl.append(servo.Servo(506,10,portHandler,packetHandler))

# Initialize GroupSyncWrite instance
groupSyncWrite = GroupSyncWrite(portHandler, packetHandler, ADDR_AX_GOAL_POSITION, LEN_MX_GOAL_POSITION)
CoM = []
robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite, CoM)

# CoM.append(CoM_Class(lastaxis, [[x], [y], [z], [1]], massa))
CoM.append(rh.CoM_Class(0, [[-4.71], [0], [7.05], [1]], 36, robot))
CoM.append(rh.CoM_Class(3, [[12.68], [-15.9], [0], [1]], 129, robot))
CoM.append(rh.CoM_Class(4, [[-12.57], [-0.65], [-0.89], [1]], 82, robot))
CoM.append(rh.CoM_Class(8, [[1.06], [-43.41], [0], [1]], 30, robot))
CoM.append(rh.CoM_Class(9, [[-39.69], [0.72], [1.4], [1]], 75, robot))
CoM.append(rh.CoM_Class(10, [[0], [-7.85], [36.197], [1]], 440+107, robot))
CoM.append(rh.CoM_Class(12, [[5.11], [-13.10], [0.81], [1]], 75, robot))
CoM.append(rh.CoM_Class(14, [[18.79], [-1.06], [0], [1]], 30, robot))
CoM.append(rh.CoM_Class(17, [[0.99], [62.17], [1], [1]], 82, robot))
CoM.append(rh.CoM_Class(18, [[41.1], [0], [12.68], [1]], 129, robot))
CoM.append(rh.CoM_Class(21, [[-4.71], [0], [64.05], [1]], 36, robot))
robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite, CoM)

# Open port
if robot.portHandler.openPort():
    print("Succeeded to open the port")
else:
    print("Failed to open the port")
    print("Press any key to terminate...")
    getch()
    quit()

# Set port baudrate
if robot.portHandler.setBaudRate(BAUDRATE):
    print("Succeeded to change the baudrate")
else:
    print("Failed to change the baudrate")
    print("Press any key to terminate...")
    getch()
    quit()
    
# Enable Dynamixel#1 Torque
robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)

# while 1:
#     print("Press any key to continue! (or press ESC to quit!)")
#     if getch() == chr(0x1b):
#         break
        
#     for obj in dxl :
#         obj.moveSync(obj.default, 2)
#     start = time.time()
    
#     robot.syncWrite()
#     indexMoving = 3
#     while indexMoving > 2 :
#         isMoving = robot.readAll(ADDR_AX_MOVING,1)
#         print('isMoving: ', isMoving)
#         indexMoving = 0
#         for i in isMoving:
#             indexMoving = indexMoving + i
#     end = time.time()
#     print("waktu: ", end - start)

# robot.com('1')

start = time.time()
dt = time.time() - start

# robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
# while 1:
#     pos = dxl[2].read(36,2) # 36 = ID read
#     print ("Position: ", pos)
#     deg = pos[0]*0.29296875
#     print ("deg : ", deg)
#     # while 1:
#     #     dxl[2].move

# init_roll, init_pitch = imu.init_IMU()
# zeroX, zeroY = imu.calibrate_IMU()
# roll, pitch = 0, 0
# timer1 = time.time()
# timer = timer1

# while 1:
#     roll, pitch, timer = imu.get_robot_ori(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch)
#     print("timer = ", round(timer - timer1, 3), "roll = ", round(roll, 3) , " pitch = ", round(pitch, 3))

# with open('KalibrasiIMUpositif.csv', mode='w') as csv_file:
#     writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
#     while 1:
#         roll, pitch, timer = imu.get_robot_ori(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch)
#         print(round(timer - timer1, 3), ",", round(pitch, 3) , ",", round(roll, 3))
#         writer.writerow([round(timer - timer1, 3), round(pitch, 3), round(roll, 3)])

state1Pitch = 0

# while 1:
#     state1Pitch = robot.controlBerdiri(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch, state1Pitch)
#     roll, pitch, timer = imu.get_robot_ori(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch)
#     print("timer = ", round(timer - timer1, 3), "pitch = ", round(pitch, 3) , " roll = ", round(roll, 3))

#     if (pitch > 12 or pitch < -12):
#         state1Pitch = robot.controlBerdiri(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch, state1Pitch)  

# with open('ControlPitch.csv', mode='w') as csv_file:
#     writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
#     while 1:
#         state1Pitch = robot.controlBerdiri(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch, state1Pitch)
#         roll, pitch, timer = imu.get_robot_ori(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch)
#         print("timer = ", round(timer - timer1, 3), "pitch = ", round(pitch, 3) , " roll = ", round(roll, 3))
#         writer.writerow([round(timer - timer1, 3), round(pitch, 3), round(roll, 3)])

#         if (pitch > 12 or pitch < -12):
#             state1Pitch = robot.controlBerdiri(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch, state1Pitch)  

# while 1:
#     state1Pitch = robot.controlBerdiri(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch, state1Pitch)
#     start_test = time.time()
#     roll, pitch, timer = imu.get_robot_ori(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch)
#     print("timer = ", round(timer - timer1, 3), "roll = ", round(roll, 3) , " pitch = ", round(pitch, 3))

#     if (pitch > 12 or pitch < -12):
#         state1Pitch = robot.controlBerdiri(timer, zeroX, zeroY, roll, pitch, init_roll, init_pitch, state1Pitch)  

#     dt_test = time.time()
#     print("time: ", dt_test - start_test)

# LR_base = input("Press 1 for right base or 0 for left base: ")
LR_base = '1'
if (LR_base == '1'):
    dxl[0].moveSync(507,2)
    dxl[1].moveSync(984,2)
    dxl[2].moveSync(581,2)
    dxl[3].moveSync(597,2)
    dxl[4].moveSync(514,2)
    dxl[5].moveSync(513,2)
    dxl[6].moveSync(261,2)
    dxl[7].moveSync(574,2)
    dxl[8].moveSync(386,2)
    dxl[9].moveSync(510,2)
if (LR_base == '0'):
    dxl[0].moveSync(553,2)
    dxl[1].moveSync(790,2)
    dxl[2].moveSync(786,2)
    dxl[3].moveSync(593,2)
    dxl[4].moveSync(544,2)
    dxl[5].moveSync(562,2)
    dxl[6].moveSync(11,2)
    dxl[7].moveSync(771,2)
    dxl[8].moveSync(435,2)
    dxl[9].moveSync(536,2)

while 1:
    print("Press any key to continue! (or press ESC to quit!)")
    if getch() == chr(0x1b):
        break
        
    start = time.time()
    robot.syncWrite()
    indexMoving = 3
    
    while indexMoving > 2 :
        isMoving = robot.readAll(ADDR_AX_MOVING,1)
        print('isMoving: ', isMoving)
        indexMoving = 0
        for i in isMoving:
            indexMoving = indexMoving + i
    end = time.time()
    print("waktu: ", end - start)

# sudut = list(robot.inverseKinematics(-3,0,15))
# # print(sudut)
# for i in range(0,10):
#     if sudut[i] > 1023:
#         sudut[i] = 1023
# # print(sudut)

testwaktu = time.time()
robot.comPrediction(LR_base)
print("COM Prediction: ", rh.comPos)
print("time: ", time.time()-testwaktu)

# while 1:
#     print("Press any key to continue to Inverse Kinematics! (or press ESC to quit!)")
#     inp = getch()
#     if inp == chr(0x1b):
#         break
#     else:
#         if LR_base == '1':
#             dxl[5].moveSync(sudut[6],2)
#             dxl[6].moveSync(sudut[7],2)
#             dxl[7].moveSync(sudut[8],2)
#             dxl[8].moveSync(sudut[9],2)
#             dxl[9].moveSync(sudut[10],2)
#         if LR_base == '0':
#             dxl[0].moveSync(sudut[1],2)
#             dxl[1].moveSync(sudut[2],2)
#             dxl[2].moveSync(sudut[3],2)
#             dxl[3].moveSync(sudut[4],2)
#             dxl[4].moveSync(sudut[5],2)
#         robot.syncWrite()

print("Press any key to continue to Pattern! (or press ESC to quit!)")
inp = getch()
tsup = 0

if inp != chr(0x1b):
    tsup = input("Enter moving interval: ")
    tsup = float(tsup)
    print("interval: ", tsup)

vX, vY, state1Roll, state1Pitch = robot.walkInit(5,2,16,tsup,LR_base)
    
timer = time.time()
start_time = time.time()
current_time = time.time()

# posisiTujuan = robot.calculatePattern(time.time() - start_time, vX, vY, comInX, comInY, comInZ)
# print("posisi tujuan: ", posisiTujuan)
# x_timeNow = posisiTujuan[0]
# y_timeNow = posisiTujuan[1]

timeNow = 0
current_time = time.time()
state_pola = 1
state_print = 0
timeEnd = 0

while 1:
    LR_base, state_pola, state_print = robot.mainProgram(LR_base, tsup, start_time, current_time, state_print, state_pola)
    # current_time = time.time()
    # if (current_time - previous_time >= 0.06):u
    #     read_dtime = 0.06
    #     MOVING_SPEED = 32 #moving speed masih belum sesuai
    #     prev_adc = robot.readEstimate(MOVING_SPEED, read_dtime, prev_adc, next_adc, current_adc)
    #     state1Roll, state1Pitch = robot.control(x_timeNow, y_timeNow, LR_base, state1Roll, state1Pitch, MOVING_SPEED, read_dtime, prev_adc, next_adc, current_adc)
    #     # print("est_adc: ", prev_adc)
    #     prev_time = current_time

    #     dt = time.time() - timer
    #     timer = time.time()
    #     data = robot.ambilSudut(data[0], data[1], dt)

    #     start = time.time()
    #     robot.com()
    #     end = time.time()
    #     print("waktu sampling: ", end - start)

    # robot.readAllPresentPos()

# print("Press E to enable or ESC to disable torque")
# inp = getch()
# if inp == 'e':
#     robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
# if inp == chr(0x1b):
#     robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)

# cek_arah = input("Press anything to continue or y to masuk ke Cek arah: ")
# if cek_arah == 'y':
#     while(1):
#         servo_id = input("Masukkan ID Servo: ")
#         sudut    = input("Masukkan Sudut   : ")
#         dxl[int(servo_id)-1].move(float(sudut),1,type='degree') 
#         break_arah = input("press y to break cek arah")
#         if break_arah == 'y':
#             break
# start = time.time()

# robot.com()

robot.portHandler.closePort()