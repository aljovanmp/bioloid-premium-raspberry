from array import *

from math import *

from numpy import *



# panjang link sejajar sumbu x global

l0 = 57

l1 = 57

l2 = 15

l4 = 50



# panjang link sejajar sumbu z global

h0 = 32

h1 = 75

h2 = 14

h3 = 61

h4 = 33



# sudut servo (penomoran sesuai urutan DH, bukan id)

theta = [0,0,0,0,0,0,0,0,0,0]





# matriks transformasi parameter DH



# T_n-1_ke_n = [[cos(theta_n), -sin(theta_n)*cos(alpha_n), sin(theta_n)*sin(alpha_n),  r_n*cos(theta_n)],

#               [sin(theta_n),  cos(theta_n)*cos(alpha_n), -cos(theta_n)*sin(alpha_n), r_n*sin(theta_n)],

#               [0,             sin(alpha_n),              cos(alpha_n),               d_n],

#               [0,             0,                         0,                          1]]



transformMatrixBaseKanan = [[[1, 0, 0, 0],

                                [0, 1, 0, 0],

                                [0, 0, 1, 0],

                                [0, 0, 0, 1]],

                            [[1, 0, 0, -l0],

                                [0, 1, 0, 0],

                                [0, 0, 1, 0],

                                [0, 0, 0, 1]],

                            [[0, 0, -1, 0],

                                [1, 0, 0, 0],

                                [0, -1, 0, h0],

                                [0, 0, 0, 1]],

                            [[cos(radians(-90.0)+theta[0]), 0, -sin((radians(-90.0)+theta[0])), 0], 

                                [sin(radians(-90.0)+theta[0]), 0, cos(radians(-90.0)+theta[0]), 0], 

                                [0, -1, 0, -l1], 

                                [0, 0, 0, 1]],

                            [[cos(theta[1]), 0, -sin(theta[1]), h1*cos(theta[1])], 

                                [sin(theta[1]), 0, cos(theta[1]), h1*sin(theta[1])], 

                                [0, -1, 0, 0], 

                                [0, 0, 0, 1]],

                            [[1, 0, 0, 0],

                                [0, 0, -1, 0],

                                [0, 1, 0, -l2],

                                [0, 0, 0, 1]],

                            [[cos(theta[2]), 0, -sin(theta[2]), h2*cos(theta[2])],

                                [sin(theta[2]), 0, cos(theta[2]), h2*sin(theta[2])],

                                [0, -1, 0, 0],

                                [0, 0, 0, 1]],

                            [[0, 0, 1, 0],

                                [-1, 0, 0, 0],

                                [0, -1, 0, l2],

                                [0, 0, 0, 1]],

                            [[0, 0, 1, 0],

                                [1, 0, 0, 0],

                                [0, 1, 0, h3],

                                [0, 0, 0, 1]],

                            [[cos(radians(90.0)+theta[3]), 0, -sin(radians(90.0)+theta[3]), h4*cos(radians(90.0)+theta[3])],

                                [sin(radians(90.0)+theta[3]), 0, cos(radians(90.0)+theta[3]), h4*sin(radians(90.0)+theta[3])],

                                [0, -1, 0, 0],

                                [0, 0, 0, 1]],

                            [[cos(radians(90.0)+theta[4]), 0, sin(radians(90.0)+theta[4]), -l4*cos(radians(90.0)+theta[4])],

                                [sin(radians(90.0)+theta[4]), 0, -cos(radians(90.0)+theta[4]), -l4*sin(radians(90.0)+theta[4])],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                            [[1, 0, 0, -l4],

                                [0, 0, 1, 0],

                                [0, -1, 0, 0],

                                [0, 0, 0, 1]],

                            [[cos(radians(-90.0)+theta[5]), 0, sin(radians(-90.0)+theta[5]), -h4*cos(radians(-90.0)+theta[5])],

                                [sin(radians(-90.0)+theta[5]), 0, -cos(radians(-90.0)+theta[5]), -h4*sin(radians(-90.0)+theta[5])],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                            [[cos(theta[6]), -sin(theta[6]), 0, -h3*cos(theta[6])],

                                [sin(theta[6]), cos(theta[6]), 0, -h3*sin(theta[6])],

                                [0, 0, 1, 0],

                                [0, 0, 0, 1]],

                            [[0, 0, -1, 0],

                                [-1, 0, 0, 0],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                            [[-1, 0, 0, l2],

                                [0, 0, 1, 0],

                                [0, 1, 0, h2],

                                [0, 0, 0, 1]],

                            [[cos(radians(180.0)+theta[7]), 0, sin(radians(180.0)+theta[7]), -l2*cos(radians(180.0)+theta[7])],

                                [sin(radians(180.0)+theta[7]), 0, -cos(radians(180.0)+theta[7]), -l2*sin(radians(180.0)+theta[7])],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                            [[1, 0, 0, 0],

                                [0, 0, 1, 0],

                                [0, -1, 0, h1],

                                [0, 0, 0, 1]],

                            [[cos(radians(180.0)+theta[8]), 0, sin(radians(180.0)+theta[8]), -l1*cos(radians(180.0)+theta[8])],

                                [sin(radians(180.0)+theta[8]), 0, -cos(radians(180.0)+theta[8]), -l1*sin(radians(180.0)+theta[8])],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                            [[1, 0, 0, 0],

                                [0, 0, 1, 0],

                                [0, -1, 0, 0],

                                [0, 0, 0, 1]],

                            [[0, 0, -1, 0],

                                [-1, 0, 0, 0],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                            [[cos(theta[9]), -sin(theta[9]), 0, -h0*cos(theta[9])],

                                [sin(theta[9]), cos(theta[9]), 0, -h0*sin(theta[9])],

                                [0, 0, 1, 0],

                                [0, 0, 0, 1]]]



transformMatrixBaseKiri = [[[1, 0, 0, 0],

                                [0, 1, 0, 0],

                                [0, 0, 1, 0],

                                [0, 0, 0, 1]],

                            [[1, 0, 0, -l0],

                                [0, 1, 0, 0],

                                [0, 0, 1, 0],

                                [0, 0, 0, 1]],

                           [[0, 0, -1, 0],

                                [1, 0, 0, 0],

                                [0, -1, 0, h0],

                                [0, 0, 0, 1]],

                           [[cos(radians(-90.0)+theta[0]), 0, -sin((radians(-90.0)+theta[0])), 0],

                                [sin(radians(-90.0)+theta[0]), 0, cos(radians(-90.0)+theta[0]), 0],

                                [0, -1, 0, -l1],

                                [0, 0, 0, 1]],

                           [[cos(theta[1]), 0, -sin(theta[1]), h1*cos(theta[1])],

                                [sin(theta[1]), 0, cos(theta[1]), h1*sin(theta[1])],

                                [0, -1, 0, 0],

                                [0, 0, 0, 1]],

                           [[1, 0, 0, 0],

                                [0, 0, -1, 0],

                                [0, 1, 0, -l2],

                                [0, 0, 0, 1]],

                           [[cos(theta[2]), 0, -sin(theta[2]), h2*cos(theta[2])],

                                [sin(theta[2]), 0, cos(theta[2]), h2*sin(theta[2])],

                                [0, -1, 0, 0],

                                [0, 0, 0, 1]],

                           [[0, 0, 1, 0],

                                [-1, 0, 0, 0],

                                [0, -1, 0, l2],

                                [0, 0, 0, 1]],

                           [[0, 0, 1, 0],

                                [1, 0, 0, 0],

                                [0, 1, 0, h3],

                                [0, 0, 0, 1]],

                           [[cos(radians(90.0)+theta[3]), 0, -sin(radians(90.0)+theta[3]), h4*cos(radians(90.0)+theta[3])],

                                [sin(radians(90.0)+theta[3]), 0, cos(radians(90.0)+theta[3]), h4*sin(radians(90.0)+theta[3])],

                                [0, -1, 0, 0],

                                [0, 0, 0, 1]],

                           [[cos(radians(90.0)+theta[4]), 0, sin(radians(90.0)+theta[4]), l4*cos(radians(90.0)+theta[4])],

                                [sin(radians(90.0)+theta[4]), 0, -cos(radians(90.0)+theta[4]), l4*sin(radians(90.0)+theta[4])],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                           [[1, 0, 0, l4],

                                [0, 0, 1, 0],

                                [0, -1, 0, 0],

                                [0, 0, 0, 1]],

                           [[cos(radians(-90.0)+theta[5]), 0, sin(radians(-90.0)+theta[5]), -h4*cos(radians(-90.0)+theta[5])],

                                [sin(radians(-90.0)+theta[5]), 0, -cos(radians(-90.0)+theta[5]), -h4*sin(radians(-90.0)+theta[5])],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                           [[cos(theta[6]), -sin(theta[6]), 0, -h3*cos(theta[6])],

                                [sin(theta[6]), cos(theta[6]), 0, -h3*sin(theta[6])],

                                [0, 0, 1, 0],

                                [0, 0, 0, 1]],

                           [[0, 0, -1, 0],

                                [-1, 0, 0, 0],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                           [[-1, 0, 0, l2],

                                [0, 0, 1, 0],

                                [0, 1, 0, h2],

                                [0, 0, 0, 1]],

                           [[cos(radians(180.0)+theta[7]), 0, sin(radians(180.0)+theta[7]), -l2*cos(radians(180.0)+theta[7])],

                                [sin(radians(180.0)+theta[7]), 0, -cos(radians(180.0)+theta[7]), -l2*sin(radians(180.0)+theta[7])],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                           [[1, 0, 0, 0],

                                [0, 0, 1, 0],

                                [0, -1, 0, h1],

                                [0, 0, 0, 1]],

                           [[cos(radians(180.0)+theta[8]), 0, sin(radians(180.0)+theta[8]), -l1*cos(radians(180.0)+theta[8])],

                                [sin(radians(180.0)+theta[8]), 0, -cos(radians(180.0)+theta[8]), -l1*sin(radians(180.0)+theta[8])],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                           [[1, 0, 0, 0],

                                [0, 0, 1, 0],

                                [0, -1, 0, 0],

                                [0, 0, 0, 1]],

                           [[0, 0, -1, 0],

                                [-1, 0, 0, 0],

                                [0, 1, 0, 0],

                                [0, 0, 0, 1]],

                           [[cos(theta[9]), -sin(theta[9]), 0, -h0*cos(theta[9])],

                                [sin(theta[9]), cos(theta[9]), 0, -h0*sin(theta[9])],

                                [0, 0, 1, 0],

                                [0, 0, 0, 1]]]





def calculateTransMatrix (base, Last_axis):

    # print(base)

    Trans_matrix = []

    print('Last: ', Last_axis)

    if base == 1: #base = 1: base Kanan

        indexx = 0

        for i in range(0, 21):



            if i <= Last_axis:

                # print("index : ", indexx)

                Trans_matrix.append(transformMatrixBaseKanan[i][:][:])

                # print(transformMatrixBaseKanan[i][:][:])

            indexx+=1

        # if Last_axis == 4:

            # print("Trans matrix: ")

            # print(array(Trans_matrix))

    else:

        for i in range(0, 21):

            if i <= Last_axis:

                Trans_matrix.append(transformMatrixBaseKiri[i][:][:])

    # for i in Trans_matrix:

    #     if Last_axis == 4:

    #         print('mat_L: ')

    #         print(array(i))

    Trans_mat_glob = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

    print("================")

    index = 0

    

    for i in Trans_matrix:

        # if Last_axis == 4:

        #     print('mat: ')

        #     print(array(i))

        next_mat = array(i)

        recent_glob = matmul(Trans_mat_glob, next_mat)

        Trans_mat_glob = recent_glob.copy()

        # if Last_axis == 4:

            

            # print('transto indx: ', index,'next: ')

            # print(next_mat)

            # print("Trans_glob: ")

            # print(Trans_mat_glob)

        index+=1

            

    return recent_glob

def updateMatrixTheta(theta):

    updateThetaDHParam(Read_Flag)

    theta = []

    return {'thetas': theta}





class CoM_Class:

    def __init__(self,Last_axis,CoM_Coordinate,Link_mass):

        self.Last_a = Last_axis

        self.CoM_Coord = CoM_Coordinate

        self.mass = Link_mass

    def cek(self,base):

        TM_from_glob = calculateTransMatrix(base,self.Last_a)

        TM = array(TM_from_glob)

        Coord = array(self.CoM_Coord)

        print()

        print("Trans to: ", self.Last_a)

        print(TM)

        print("coo: ")

        self.Coord_from_glob = matmul(TM,Coord)

        # print(self.Coord_from_glob)

        return(self.Coord_from_glob)



CoM = []



# CoM.append(CoM_Class(lastaxis, [[x], [y], [z], [1]], massa))

CoM.append(CoM_Class(0, [[-4.71], [0], [7.05], [1]], 36))

CoM.append(CoM_Class(3, [[12.68], [-15.9], [0], [1]], 129))

CoM.append(CoM_Class(4, [[-12.57], [-0.65], [-0.89], [1]], 82))

CoM.append(CoM_Class(8, [[1.06], [-43.41], [0], [1]], 30))

CoM.append(CoM_Class(9, [[-39.69], [0.72], [1.4], [1]], 75))

CoM.append(CoM_Class(10, [[0], [-10], [21.575], [1]], 440))

CoM.append(CoM_Class(12, [[5.11], [-13.10], [0.81], [1]], 75))

CoM.append(CoM_Class(14, [[18.79], [-1.06], [0], [1]], 30))

CoM.append(CoM_Class(17, [[0.99], [62.17], [1], [1]], 82))

CoM.append(CoM_Class(18, [[41.1], [0], [12.68], [1]], 129))

CoM.append(CoM_Class(21, [[-4.71], [0], [64.05], [1]], 36))



def com(COM):

    x = 0

    y = 0

    z = 0

    m = 0

    for obj in COM:

        # print('===============================================')

        part = obj.cek(1)

        # print()

        # print("Part: ", obj.Link, " CoM:")

        # print(part)

        x_next = obj.Coord_from_glob[0].copy()

        y_next = obj.Coord_from_glob[1].copy()

        z_next = obj.Coord_from_glob[2].copy()

        m_next = obj.mass

        # print("Coordinat Before: ")

        # print('x: ', x, 'y: ', y, 'z: ', z, 'm: ', m)

        # print()

        print("Next : ")

        print('x: ', x_next, 'y: ', y_next, 'z: ', z_next, 'm: ', m_next)

        x = around(((x*m) + (x_next*m_next)) / (m+m_next),3)

        y = around(((y*m) + (y_next*m_next)) / (m+m_next),3)

        z = around(((z*m) + (z_next*m_next)) / (m+m_next),3)

        m = round((m + m_next),3)

        # print()

        # print('CoM: ')

        print("Last_axis: ", obj.Last_a)

        print('x: ', x, 'y: ', y, 'z: ', z, 'm: ', m)

        # print()

        

    return{'x':x, 'y':y,'z':z}



A = com(CoM)

print(A)
