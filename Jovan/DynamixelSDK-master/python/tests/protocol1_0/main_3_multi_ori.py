import threading
import os
import get_CSV
import time
import numpy as np
import imu_inMain as imu
import torch.nn as nn
from torch.utils.data import TensorDataset, DataLoader
import torch.nn.functional as F
import torch
from Kalman import KalmanAngle
from dynamixel_sdk import *                    # Uses Dynamixel SDK library
import servo
import robot_handler as rh
from scipy.interpolate import CubicSpline
import pandas as pd

from mpu6050 import mpu6050
import math

angle_Imu = 0
againn = ''
t2_start = 0
kalAngleYfix = 0
kalAngleXfix = 0
gyroY = 0

def ambilData():
    mpu = mpu6050(0x68)
    # print("Temp : ", str(mpu.get_temp()))
    # print()
    imu = [0. for k in range (6)] #inisialisasi imu
    accel_data = mpu.get_accel_data()
    imu[0] = accel_data['x']
    imu[1] = accel_data['y']
    imu[2] = accel_data['z']

    gyro_data = mpu.get_gyro_data()
    imu[3] = gyro_data['x']
    imu[4] = gyro_data['y']
    imu[5] = gyro_data['z']
    #print("Imu : " + str(imu))
    #print()
    return imu

def thread_task(flag):
    global angle_Imu
    global againn
    global t2_start
    global kalAngleYfix
    global kalAngleXfix
    global gyroY
    
    againn = 'y'
    t2_start = 0
    
    if flag ==1:
        
        dxl = []
        fwd = []
        DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller
                                                        # ex) Windows: "COM1"   Linux: "/dev/ttyUSB0" Mac: "/dev/tty.usbserial-*"
        # Protocol version
        PROTOCOL_VERSION            = 1.0               # See which protocol version is used in the Dynamixel

        # Initialize PortHandler instance
        # Set the port path
        # Get methods and members of PortHandlerLinux or PortHandlerWindows
          
        portHandler = PortHandler(DEVICENAME)


        # Initialize PacketHandler instance
        # Set the protocol version
        # Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
        packetHandler = PacketHandler(PROTOCOL_VERSION)

        dxl.append(servo.Servo(235,1,portHandler,packetHandler))
        dxl.append(servo.Servo(788,2,portHandler,packetHandler))
        dxl.append(servo.Servo(279,3,portHandler,packetHandler))
        dxl.append(servo.Servo(744,4,portHandler,packetHandler))
        dxl.append(servo.Servo(462,5,portHandler,packetHandler))
        dxl.append(servo.Servo(561,6,portHandler,packetHandler))
        dxl.append(servo.Servo(358,7,portHandler,packetHandler))
        dxl.append(servo.Servo(666,8,portHandler,packetHandler))
        dxl.append(servo.Servo(507,9,portHandler,packetHandler))
        dxl.append(servo.Servo(516,10,portHandler,packetHandler))
        dxl.append(servo.Servo(341,11,portHandler,packetHandler))
        dxl.append(servo.Servo(682,12,portHandler,packetHandler))
        dxl.append(servo.Servo(240,13,portHandler,packetHandler))
        dxl.append(servo.Servo(783,14,portHandler,packetHandler))
        dxl.append(servo.Servo(647,15,portHandler,packetHandler))
        dxl.append(servo.Servo(376,16,portHandler,packetHandler))
        dxl.append(servo.Servo(507,17,portHandler,packetHandler))
        dxl.append(servo.Servo(516,18,portHandler,packetHandler))

        for a in range(1,41):
            fwd.append(rh.Forward(a))


        # Control table address
        ADDR_AX_TORQUE_ENABLE      = 24            # Control table address is different in Dynamixel model
        ADDR_AX_GOAL_POSITION      = 30
        ADDR_AX_PRESENT_POSITION   = 36
        ADDR_AX_MOVING             = 46

        # Data Byte Length
        LEN_MX_GOAL_POSITION       = 4
        LEN_MX_PRESENT_POSITION    = 2

        # Default setting
        DXL1_ID                     = 1                 # Dynamixel#1 ID : 1
        DXL2_ID                     = 2                 # Dynamixel#1 ID : 2
        BAUDRATE                    = 1000000          # Dynamixel default baudrate : 57600

        TORQUE_ENABLE               = 1                 # Value for enabling the torque
        TORQUE_DISABLE              = 0                 # Value for disabling the torque

        # Initialize GroupSyncWrite instance
        groupSyncWrite = GroupSyncWrite(portHandler, packetHandler, ADDR_AX_GOAL_POSITION, LEN_MX_GOAL_POSITION)
        CoM = []
        robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite,fwd, CoM)
        CoM.append(rh.CoM_Class('L0_Kanan' ,2  ,[[0.005] ,[0]      ,[0.007] ,[1]],robot,0.036))
        CoM.append(rh.CoM_Class('L1_Kanan' ,5  ,[[0.013] ,[-0.016] ,[0]     ,[1]],robot,0.129))
        CoM.append(rh.CoM_Class('L2_Kanan' ,6  ,[[-0.013],[0]      ,[-0.001],[1]],robot,0.082))
        CoM.append(rh.CoM_Class('L3_Kanan' ,10  ,[[0]     ,[-0.043] ,[0]     ,[1]],robot,0.030))
        CoM.append(rh.CoM_Class('L4_Kanan' ,11 ,[[-0.013],[0]      ,[0.016] ,[1]],robot,0.124))
        CoM.append(rh.CoM_Class('L5_Kanan' ,14 ,[[0]     ,[-0.0165],[-0.016],[1]],robot,0.015))
        CoM.append(rh.CoM_Class('L0_Kiri'  ,16 ,[[0.005] ,[0]      ,[0.007] ,[1]],robot,0.036))
        CoM.append(rh.CoM_Class('L1_Kiri'  ,19 ,[[0.013] ,[-0.016] ,[0]     ,[1]],robot,0.129))
        CoM.append(rh.CoM_Class('L2_Kiri'  ,20 ,[[-0.013],[0]      ,[-0.001],[1]],robot,0.082))
        CoM.append(rh.CoM_Class('L3_Kiri'  ,24 ,[[0]     ,[-0.043] ,[0]     ,[1]],robot,0.030))
        CoM.append(rh.CoM_Class('L4_Kiri'  ,25 ,[[-0.013],[0]      ,[0.016] ,[1]],robot,0.124))
        CoM.append(rh.CoM_Class('L5_Kiri'  ,28 ,[[0]     ,[-0.017] ,[-0.016],[1]],robot,0.015))
        CoM.append(rh.CoM_Class('L6_KIri'  ,33 ,[[-0.004],[-0.012] ,[0]     ,[1]],robot,0.011))
        CoM.append(rh.CoM_Class('L7_Kiri'  ,34 ,[[0.044] ,[0]      ,[0.001] ,[1]],robot,0.075))
        CoM.append(rh.CoM_Class('L8_Kiri'  ,35 ,[[0.080] ,[0.001]  ,[0]     ,[1]],robot,0.079))
        CoM.append(rh.CoM_Class('Badan'    ,37 ,[[-0.015],[-0.036] ,[-0.046],[1]],robot,0.472))
        CoM.append(rh.CoM_Class('L6_Kanan' ,38 ,[[-0.004],[-0.012] ,[0]     ,[1]],robot,0.011))
        CoM.append(rh.CoM_Class('L7_Kanan' ,39 ,[[0.044] ,[0]      ,[0.001] ,[1]],robot,0.075))
        CoM.append(rh.CoM_Class('L8_Kanan' ,40 ,[[0.080] ,[0.001]  ,[0]     ,[1]],robot,0.079))

        robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite,fwd, CoM)
        import csv
        data_list = []
        data_list_2 = []



        # Open port
        if robot.portHandler.openPort():
            print("Succeeded to open the port")
        else:
            print("Failed to open the port")
            print("Press any key to terminate...")
            getch()
            quit()

        # Set port baudrate
        if robot.portHandler.setBaudRate(BAUDRATE):
            print("Succeeded to change the baudrate")
        else:
            print("Failed to change the baudrate")
            print("Press any key to terminate...")
            getch()
            quit()
            
        # Enable Dynamixel#1 Torque
        robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
        quitt = ''
        while 1:
            print("Press any key to continue! (or press ESC to quit!) or q to terminate or f to forward")
            quitt = getch()
            if quitt == chr(0x1b) or quitt=='q' or quitt=='f':
                break
            for obj in dxl :
                obj.moveSync(obj.default, 2,0, move_flag = 0)
                # print('prev: ', obj.prevGoal)
            start = time.time()
            
            # dxl[0].move(100,200)
            # Add Dynamixel goal position value to the Syncwrite parameter storage
            robot.syncWrite()
            indexMoving = 3
            while indexMoving > 2 :
                # print('cek2')
                isMoving = robot.readAll(ADDR_AX_MOVING,1)
                print('isMoving: ', isMoving)
                indexMoving = 0
                for i in isMoving:
                    indexMoving = indexMoving + i
            end = time.time()
            print("waktu: ", end - start)
        
        t2_start = 1
        
        robot.readAllPresentPos()
        cek_arah = input("Press anything to continue or y to masuk ke Cek arah: ")
        if cek_arah == 'y':
            while(1):
                servo_id = input("Masukkan ID Servo: ")
                sudut    = input("Masukkan Sudut   : ")
                dxl[int(servo_id)-1].move(int(sudut),1,type='degree') 
                break_arah = input("press y to break cek arah")
                if break_arah == 'y':
                    break

        cek_TM = input("press anything to continue or y to masuk ke Cek TM: ")
        if cek_TM == 'y': 
            while(1):
                robot.readAllPresentPos()
                Last_a = input("Masukkan Last axis: ")    
                TM = robot.TransMatGlob(int(Last_a))
                print('====================')
                print('Trans mat to: ', TM['to'])
                Trans_m = np.array(TM['TM'])
                print(Trans_m)
                x = input("Masukkan x : ")
                y = input("Masukkan y : ")
                z = input("Masukkan z : ")
                local = np.array([[int(x)],[int(y)],[int(z)],[1]])
                glob = np.matmul(Trans_m,local)
                print("global: ")
                print(glob)
        COM = robot.com()
        print("CoM : ",COM)
        X,Y,Z = 0,0,0
        for ex in COM['x']:
            X = ex
        for ye in COM['y']:
            Y = ye
        for zet in COM['z']:
            Z = zet
        # for i in range(0,2):
            # com = CoM[i].cek()
            # print("i :",i," ",com)

        cek_arah_2 = input("Press anything to continue or y to masuk ke cek IMU: ")
        if cek_arah_2 == 'y':
            servo_id = []
            sudut= []
            timer_last = 0
            data_imu = {'kalibrator': [],
            'time':[],
            'Pitch Angle':[]}
            data_imu = {}
            first = 1
            time_before = 0
            time_save = []
            while(1):
                againnn = input("Press y to input another joint: ")
                if againnn == 'y':
                    # for i in [15,16]
                    sudut = float(input("Masukkan sudut: "))
                    if sudut == -10:
                        sudut_joint = -8.5
                    elif sudut == 10:
                        sudut_joint = 9
                    elif sudut == -20:
                        sudut_joint = -18.5
                    elif sudut == 20:
                        sudut_joint = 18.5
                    elif sudut == 15:
                        sudut_joint = 14.5   
                    elif sudut ==0 :
                        sudut_joint = 0
                    elif sudut<0:
                        sudut_joint = sudut + 1.5
                    else:
                        sudut_joint = sudut

                    # print()
                    dxl[10].moveSync(-sudut_joint, 1, dxl[10].prevGoal, type = 'degree', read = 1, time_type = 'rpm')
                    dxl[11].moveSync(sudut_joint, 1, dxl[11].prevGoal, type = 'degree', read = 1, time_type = 'rpm')
                #     servo_id.append(input("Masukkan ID Servo: "))
                #     sudut.append(input("Masukkan Sudut   : "))
                else:
                    
                    robot.syncWrite()
                    ind = 0
                    time_imu = []
                    theta = []
                    print(time_save)
                    # 

                    excel_name = 'Data' +'imu_2' + '.xlsx'
                    time.sleep(1.5)
                    # for i in servo_id:
                    #     dxl[int(i)-1].move(int(sudut[ind]),2,type='degree')
                    #     ind+=1
                    timer = time.time()
                    indec = 0

                    while(indec <= 150):
                        print('Pitch: ', round(kalAngleYfix,3))
                        time_now = round(timer_last+time.time() - timer,2)
                        # data = 
                        # if sudut != -20:
                        theta.append(round(kalAngleYfix,3))
                        time_imu.append(time_now+time_before)
                        indec+=1
                        time.sleep(0.05)
                    timer_last = round(timer_last+time.time() - timer,2)
                    # if sudut != -20:
                    time_before += timer_last
                    name = 'sudut_' + str(sudut)
                    data_imu[name] = theta
                    first = 0
                    for i in time_imu:
                        time_save.append(i)

                    break_arah = input("press y to break cek IMU or n to cek again")
                    if break_arah == 'y':
                        break
        data_imu['time'] = time_imu
        df = pd.DataFrame(data_imu)
        df.to_excel(excel_name)  
        # robot.readAllPresentPos()
        # dxl[0].move(dxl[0].presentpos - 50, 10)
        step = 0
        inp  = ''
        tittle =''
        times = 0
        periode = ''

        
        data_input = []
        data_target = []
        state_fall = 0
        inp = ''
        times_max,times_min = 0,0
        step_max, step_min = 0,0
        Pitch_max, Pitch_min = 0,0
        CMx_max, CMx_min = 0, 0
        CMy_max, CMy_min = 0, 0
        CMz_max, CMz_min = 0, 0
        Ankle_max, Ankle_min = 0,0
        Speed_max, Speed_min = 0,0
        Goal_max, Goal_min = 0,0
        omega_max, omega_min = 0,0
        omega = 0
        times = 0
        add_data = ''
        speed_last = 0
        model = 0
        model_name = 'none'
        name = 'none'
        pengujian = int(input("Masukkan  1 untuk pengujian: "))
        while(1):
            print('recent model: ', model_name)
            change_model = input("Use another model?: ")
            if change_model == 'y':
                number = input("Masukkan file: ")
                PATH = 'mode' + number +'.pth'
                model_name = PATH
                hid = int(input("Masukkan Jumlah Hidden Node: "))
                layer = int(input("Masukkan jumlah layer: "))
                old_model = input("Model lama?: ")
                
                if old_model == 'y':
                    if layer == 2:
                        model = torch.nn.Sequential(
                                torch.nn.Linear(8, hid),
                                torch.nn.LeakyReLU(),
                                # torch.nn.Linear(hid, int(4)),
                                
                                torch.nn.Linear(hid, int(100)),
                                torch.nn.LeakyReLU(),
                                # torch.nn.Linear(100, int(100)),
                                # torch.nn.LeakyReLU(),
                                torch.nn.Linear(int(100), 2),
                                
                            )
                    if layer == 3:
                        model = torch.nn.Sequential(
                                torch.nn.Linear(8, hid),
                                torch.nn.LeakyReLU(),
                                # torch.nn.Linear(hid, int(4)),
                                
                                torch.nn.Linear(hid, int(100)),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(100, int(100)),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(int(100), 2),
                                
                            )
                    elif layer == 6:
                        model = torch.nn.Sequential(
                                torch.nn.Linear(6, hid),
                                # torch.nn.LeakyReLU(),
                                # torch.nn.Linear(hid, int(4)),
                                torch.nn.Linear(hid, hid),
                                # torch.nn.LeakyReLU(),
                                torch.nn.Linear(hid, 100),
                                # torch.nn.LeakyReLU(),
                                torch.nn.Linear(100, 100),
                                # torch.nn.LeakyReLU(),
                                torch.nn.Linear(100, 50),
                                # torch.nn.LeakyReLU(),
                                torch.nn.Linear(50, 50),
                                # torch.nn.LeakyReLU(),
                                torch.nn.Linear(50, 2),
                            )
                    elif layer == 5:
                        model = torch.nn.Sequential(
                            torch.nn.Linear(6, hid),
                            torch.nn.LeakyReLU(),
                            # torch.nn.Linear(hid, int(4)),
                            torch.nn.Linear(hid, hid),
                            torch.nn.LeakyReLU(),
                            torch.nn.Linear(hid, hid),
                            torch.nn.LeakyReLU(),
                            torch.nn.Linear(hid, hid),
                            torch.nn.LeakyReLU(),
                            torch.nn.Linear(hid, hid),
                            torch.nn.LeakyReLU(),
                            torch.nn.Linear(hid, 2),
                        )

                if old_model == 'n':
                    if layer == 3:
                        model = torch.nn.Sequential(
                            torch.nn.Linear(8, hid),
                            torch.nn.LeakyReLU(),
                            # torch.nn.Linear(hid, int(4)),
                            torch.nn.Linear(hid, hid),
                            torch.nn.LeakyReLU(),
                            torch.nn.Linear(hid, 100),
                            torch.nn.LeakyReLU(),
                            torch.nn.Linear(100, 2),
                            )
                    elif layer == 2:
                        model = torch.nn.Sequential(
                                torch.nn.Linear(8, hid),
                                torch.nn.LeakyReLU(),
                                # torch.nn.Linear(hid, int(4)),
                                torch.nn.Linear(hid, hid),
                                torch.nn.LeakyReLU(),
                                # torch.nn.Linear(100, 100),
                                # torch.nn.LeakyReLU(),
                                torch.nn.Linear(hid, 2),
                            )
                    elif layer == 4:
                        model = torch.nn.Sequential(
                                torch.nn.Linear(8, hid),
                                torch.nn.LeakyReLU(),
                                # torch.nn.Linear(hid, int(4)),
                                torch.nn.Linear(hid, hid),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(hid, 100),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(100, 50),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(50, 2),
                            )
                    elif layer == 5:
                        model = torch.nn.Sequential(
                                torch.nn.Linear(8, hid),
                                torch.nn.LeakyReLU(),
                                # torch.nn.Linear(hid, int(4)),
                                torch.nn.Linear(hid, hid),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(hid, 100),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(100, 100),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(100, 50),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(50, 2),
                            )
                    elif layer == 6:
                        model = torch.nn.Sequential(
                                torch.nn.Linear(6, hid),
                                torch.nn.LeakyReLU(),
                                # torch.nn.Linear(hid, int(4)),
                                torch.nn.Linear(hid, hid),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(hid, 100),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(100, 100),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(100, 50),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(50, 50),
                                torch.nn.LeakyReLU(),
                                torch.nn.Linear(50, 2),
                            )
                
                model.load_state_dict(torch.load(PATH))

            
            title = ''
            print("Press 1 to get telentang pattern or 2 to get tengkurap")
            inp_pattern = getch()
            if inp_pattern == '1':
                csv_name='tes_telentang.csv'
                title = 'telentang'
                #Telentang pitch
                xp = np.array([0,0.3,0.61,1,1.61,2.2,3,3.5,5.61,5.7,6,7,9])
                yp = np.array([-82.663,-82.663,-82.659,-83.739,-110.186,-116.282,-76.439,-64,-0.5,0,0,0,0])
                #Telentang CoM
                xc = np.array([0, 0.2, 0.51, 0.72, 1, 1.67, 1.77, 2, 2.71, 2.81, 3, 3.8, 4, 4.5, 5, 5.82, 6, 7, 8, 9])
                yc = np.array([87.54, 88.32, 89.67, 90.65, 92.33, 122.01, 127.04, 133.4, 105.26, 100.51, 95.95, 78.95\
                    ,78.78, 82.87, 88.87, 87.9, 87.9, 87.9, 87.9, 87.9])
            if inp_pattern == '2':
                csv_name='tes_tengkurap.csv'
                title = 'tengkurap'
                #Tengkurap pitch
                xp = np.array([0,0.5,1,2,2.5,2.75,2.87,3,3.22,3.35,4,5,5.3, 5.7, 6, 6.05, 6.1, 6.2, 6.3, 6.4, 6.5, 6.6, 6.7\
                    ,6.8,6.9,7,9])
                yp = np.array([94.58,88, 82.02, 96.1, 97.5, 98.2, 98.6, 99, 106, 101.45, 80, 36.36, 21.61, 5, 1.3, 1, 0.7\
                    ,0,0,0,0,0,0,0,0,0,0])

                #Tengkurap CoM
                xc = np.array([0, 0.1, 0.22, 0.32, 0.45, 0.56, 0.67, 0.77, 0.89, 1, 1.1, 1.2, 1.32, 1.47, 1.6, 1.72, 1.85\
                    , 1.95, 2,2.1, 2.25, 2.5, 2.75, 3, 4, 4.5, 5, 6, 7, 8, 9])
                yc = np.array([87.22, 89.31, 91.01, 92.65, 95.28, 97.21, 98.82, 100.86, 102.92, 103.39, 101.5, 99.52, 97.49\
                    ,94.84, 92.59, 90.65, 88.68, 87.99, 88,87.75,87.5, 87, 87, 85.67, 67.1, 72, 80.61, 87.93, 87.93, 87.93, 87.93])
            
            setpoint_pitch = CubicSpline(xp, yp)
            setpoint_com = CubicSpline(xc, yc)
            print("Press any key to continue to Pattern! (or press ESC to quit!)")
            inp = getch()
            times = input("Enter moving interval: ")
            times = float(times)
            print("interval: ", times)
            periode = str(times)

            if (pengujian):
                num_step = 6
                using_model = 'y'
                empirical = []
                
            else:
                num_step = input("Masukkan Jumlah step: ")
                using_model = input("Using model?: ")
                empirical = []
                add_data = input("Adding Data?: ")
                if add_data == 'y':
                    speed_last = float(input("Masukkan kecepatan: "))
        
                while(1):
                    emp_again = input("Masukkan y untuk menambah sudut: ")
                    if emp_again == 'y':
                        step_e  = int(input("Masukkan step yg ingin diubah: "))
                        angle_e = int(input("Masukkan sudut yg ingin diubah: "))
                        speed_e = int(input("Masukkan speed yg ingin diubah: "))
                        empirical.append({'step':step_e, 'angle':angle_e, 'speed':speed_e})
                    else:
                        break

            data2 = []
            data2_save= []
            for obj in dxl :
                obj.moveSync(obj.default, 2,0, move_flag = 0)
                # print('prev: ', obj.prevGoal)
            robot.syncWrite()
            time.sleep(3)
            times_M,detail_step_M,pitch,CMx,CMy,CMz,Ankle_angle = 0,0,0,0,0,0,0
            while step <= int(num_step):
                if inp == chr(0x1b) or quitt=='q' or quitt=='f':
                    break
                timer_all = time.time()
                print("waktu1: %.2f" %(time.time() - start))
                if step !=6:
                    param,rows = get_CSV.getCSV(csv_name)
                    for row in rows:
                        # print('id: ',row[0], '\t',"row: ", row[2], '\t', row[4])
                        if row[1] == str(step):
                            id_joint = int(row[2])
                            goal = int(row[4])
                            # if id_joint!=16 and id_joint!=15:
                            dxl[id_joint-1].moveSync(goal,times,dxl[id_joint-1].prevGoal, read=1)
                print("waktu2: %.2f" %(time.time() - start))
                print()
                print('reading time: ', time.time() - timer_all)
                print('moving step ', step)
                print('Pitch : ', round(kalAngleYfix,3))
                if using_model != 'y':
                    robot.syncWrite()
        
                if step != 0 :
                    end = time.time()
                    print("waktu: %.2f" %(end - start))
                start = time.time()
                timer = time.time()
                elapsed = 0
                timerCoM = 0
                # print("Processing time 0.5: ", time.time()-timer_all)
                # if step != 6:
                COM = robot.com(read_flag=0, Time_read=elapsed)
                print("Reading CoM without read joint..")
                # else:   
                    # COM = robot.com(read_flag=1, Time_read=elapsed)
                
                for ex in COM['x']:
                    X = ex
                for ye in COM['y']:
                    Y = ye
                for zet in COM['z']:
                    Z = zet
                # print("Processing time 0.7: ", time.time()-timer_all)
                # angle_Imu = imu.IMU(dt, 11, Komp[0], Komp[1], KalmanX, KalmanY, zero_gyro = Komp[2])
                detail_step = step + round(elapsed/times,2)
                # print('before model presentpos: ', dxl[15].presentpos)
                # print('posNow: ', dxl[15].posNow)
                # print('CoMx: %f CoMy: %f CoMz: %f'%(X,Y,Z))
                # print("Processing time 0.9: ", time.time()-timer_all)
                com_feedback = math.degrees(math.atan2(Z,X))
                error_c = round(com_feedback - setpoint_com(detail_step),3)
                error_p = round(kalAngleYfix - setpoint_pitch(detail_step),3)
                gy = 0
                if inp_pattern=='2': 
                    times_max,times_min = 5,0
                    step_max, step_min = 5,0
                    Pitch_max, Pitch_min = 109.436,-26.261
                    CMx_max, CMx_min = 0.048, -0.041
                    CMy_max, CMy_min = -0.008, -0.02
                    CMz_max, CMz_min = 0.176, 0.114
                    Ankle_max, Ankle_min = 550, 288
                    Speed_max, Speed_min = 21.534, 0.555
                    Goal_max, Goal_min = 550,288
                    omega_max, omega_min = 61,-176
                    e_pitch_max, e_pitch_min = 104.64, -65.13
                    e_com_max, e_com_min = 26, -25
                    
                    times_M = ((times - times_min) / (times_max-times_min)) * 100
                    detail_step_M = ((detail_step - step_min) / (step_max-step_min))* 100
                    pitch = ((round(kalAngleYfix,3)- Pitch_min)/(Pitch_max-Pitch_min)) * 100
                    CMx = ((X-CMx_min)/(CMx_max - CMx_min)) * 100
                    CMy = ((Y-CMy_min)/(CMy_max - CMy_min)) * 100
                    CMz = ((Z-CMz_min)/(CMz_max - CMz_min)) * 100
                    Ankle_angle = ((dxl[15].presentpos-Ankle_min)/(Ankle_max - Ankle_min)) * 100
                    omega = ((gyroY - omega_min) / (omega_max-omega_min)) * 100
                    e_pitch = ((error_p - e_pitch_min) / (e_pitch_max-e_pitch_min)) * 100
                    e_com = ((error_c - e_com_min) / (e_com_max - e_com_min)) * 100
                
                if inp_pattern=='1':
                    print("Masuk telentang")
                    times_max,times_min = 5,0
                    step_max, step_min = 6,0
                    Pitch_max, Pitch_min = 5.95,-118.26
                    CMx_max, CMx_min = 0.031, -0.084
                    CMy_max, CMy_min = -0.001, -0.013
                    CMz_max, CMz_min = 0.177, 0.08
                    Ankle_max, Ankle_min = 509, 230
                    Speed_max, Speed_min = 30.08, 0.555
                    Goal_max, Goal_min = 509,237
                    omega_max, omega_min = 178.87,-126.545
                    gy = gyroY
                    e_pitch_max, e_pitch_min = 32.45, -121
                    e_com_max, e_com_min = 22, -16
                    
                    times_M = ((times - times_min) / (times_max-times_min)) * 100
                    detail_step_M = ((detail_step - step_min) / (step_max-step_min))* 100
                    pitch = ((round(kalAngleYfix,3)- Pitch_min)/(Pitch_max-Pitch_min)) * 100
                    CMx = ((X-CMx_min)/(CMx_max - CMx_min)) * 100
                    CMy = ((Y-CMy_min)/(CMy_max - CMy_min)) * 100
                    CMz = ((Z-CMz_min)/(CMz_max - CMz_min)) * 100
                    Ankle_angle = ((dxl[15].presentpos-Ankle_min)/(Ankle_max - Ankle_min)) * 100
                    omega = ((gy - omega_min) / (omega_max-omega_min)) * 100
                    e_pitch = ((error_p - e_pitch_min) / (e_pitch_max-e_pitch_min)) * 100
                    e_com = ((error_c - e_com_min) / (e_com_max - e_com_min)) * 100
                
                if using_model == 'y':
                    data_input = np.array([times_M,detail_step_M,\
                        e_pitch,e_com, \
                        # pitch, CMx, CMy, CMz, \
                        Ankle_angle,omega], dtype='float32')
                    #ankle speed xmin 0.555 xmax 17.316
                    #goal xmin 288 xmax 550
                    # data = [times,str(detail_step),str(angle_Imu['Pitch']),str(X), str(Y),str(Z), \
                    # str(dxl[15].presentpos)]
                    data_input_show = np.array([times,detail_step,error_p,error_c, dxl[15].presentpos, gy])
                    print('data input: ')
                    print(data_input_show)
                    inputs = torch.from_numpy(data_input)
                    # print('before: ', data)
                    # print('input: ',inputs)
                    before = time.time()
                    pred = model(inputs)
                    pred_n = pred.detach().numpy()
                    # print('Model time: ', time.time() - before)
                    predict = []
                    for i in pred_n: 
                        predict.append(i)
                        # print('pred: ', i)
                    
                    predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                    predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                    print("Processing time 1: ", time.time()-timer_all)
                    # if inp_pattern == '1':
                    #     dif = 0
                    #     if step == 2:
                    #         if predict_goal>239:
                    #             dif = predict_goal - 239
                    #             predict_goal = 239

                    # konfigurasi 0.5:
                    # if inp_pattern == '1':
                    #     if predict_speed > 100:
                    #         predict_speed = 5
                    #     if step == 5:
                    #         if predict_goal > 400:
                    #             predict_goal = 376
                    #     if step ==6:
                    #         predict_goal = 374
                    #         predict_speed = 2
                    #  0.4
                    # if inp_pattern == '1':
                    #     if step ==6:
                    #         predict_speed = 2
                    #         if predict_goal > 380:
                    #             predict_goal = 370
                    #             predict_speed = 1
                    #     elif step == 3:
                    #         if predict_goal >242:
                    #             predict_goal = 238
                    #             predict_speed = 4
                    # if times==0.4:
                        # if inp_pattern == '2':
                        #     if step ==6:
                        #         if predict_speed < 5:
                        #             predict_speed = 12
                    #  0.3, 0.25
                    if times<= 0.3:
                        if inp_pattern == '1':
                            if step ==3:
                                if predict_goal>246:
                                    predict_goal =251
                                    predict_speed = 1
                    #     elif step == 4:
                    #         predict_goal = 327
                    #         predict_speed = dxl[15].speed*0.111
                    #     elif step == 6:
                    #         if predict_goal<370:
                    #             predict_goal = 380
                    #         if predict_speed>3:
                    #             predict_speed = 2

                    # if inp_pattern == '2':
                    #     dif = 0
                    #     if step == 4:
                    #         predict_speed = 10
                        # if empirical_step != 0:
                    for i in empirical:
                        if (step == i['step']):
                            print('speed_ori: ', predict_speed)
                            print('goal_ori : ', predict_goal)
                            predict_goal += i['angle']
                            predict_speed += i['speed']
                            
                            
                    predict_goal_data = ((predict_goal-Goal_min)/(Goal_max-Goal_min))*100
                    predict_speed_data = ((predict_speed-Speed_min)/(Speed_max-Speed_min))*100
                    
                    # com_feedback = math.degrees(math.atan2(Z,X))
                    # error_c = round(com_feedback - setpoint_com(detail_step),3)
                    # error_p = round(kalAngleYfix - setpoint_pitch(detail_step),3)
                    # print("waktu cb: ", time.time() - sta)
                    data2 = ([times_M,str(detail_step_M),e_pitch,e_com,str(pitch),str(CMx), str(CMy),str(CMz), \
                    str(Ankle_angle),round(omega,3), round(predict_speed_data,3), round(predict_goal_data,3)])
                    # print('posNow: ', dxl[15].posNow)
                    data = [times,detail_step, error_p, error_c, str(round(kalAngleYfix,3)),str(X),\
                        str(Y),str(Z),str(dxl[15].presentpos),round(gy,3),str(predict_speed), \
                        predict_goal, title]
                    data_list.append(data)
                    data2_save.append(data2)
                    data2 = []
                    
                    print('pattern speed: ', dxl[15].speed*0.111)
                    print('pattern goal : ', dxl[15].prevGoal)
                    print('speed: ', predict_speed)
                    print('goal : ', predict_goal)
                    
                    dif_predict = predict_goal - dxl[15].default
                    dxl[15].moveSync(int(predict_goal),predict_speed,dxl[15].presentpos, time_type='rpm')
                    dxl[14].moveSync(int(dxl[14].default-dif_predict),predict_speed,dxl[14].presentpos, time_type='rpm')
                    print("Processing time: ", time.time()-timer_all)
                    robot.syncWrite()
                    
                else:
                    # print("saving data to not using model....")
                    goal,speedd = 0,0
                    # print('speed: ', dxl[15].speed)

                    goal = round((((dxl[15].prevGoal-Goal_min)/(Goal_max-Goal_min)) * 100),3)
                    speedd = round(((((dxl[15].speed*0.111)-Speed_min)/(Speed_max-Speed_min)) * 100),3)
                    
                    # print('speed in data: ', speedd)
                    # goal = round((((dxl[15].prevGoal-288)/(550-228)) * 100),3)
                    # speedd = round(((((dxl[15].speed*0.111)-0.555)/(17.316-0.555)) * 100),3)
                    data2 = ([times_M,str(detail_step_M),e_pitch,e_com,str(pitch),str(CMx), str(CMy),str(CMz), \
                    str(Ankle_angle),round(omega,3), round(speedd,3), round(goal,3)])
                    # print('posNow: ', dxl[15].posNow)
                    data = [times,detail_step,str(round(kalAngleYfix,3)),str(X), str(Y),str(Z),str(dxl[15].presentpos), round(gy,3), \
                                    str(dxl[15].speed*0.111), str(dxl[15].prevGoal), title]
                    data_list.append(data)
                    data2_save.append(data2)
                    data2 = []
                start = time.time()
                timer = time.time()
                elapsed = 0
                if inp_pattern == '2':
                    timer_stab = time.time() - 0.4
                else:
                    timer_stab = time.time() - 0.05
                timerIMU = time.time()
                
                flag_stab = 0
                st = 0
                # if inp_pattern == '1':
                #     if times == 0.3:
                #         if step == 5:
                #             times += times
                while (elapsed < times):
                    # print('cek')
                    elapsed = time.time() - start
                    dt = time.time() - timer
                    dt_stab = time.time() - timer_stab
                    if using_model == 'y':
                        if (inp_pattern=='2' and step>=3):
                            if dt_stab >= (times/2):
                                print('dt: ', dt_stab)
                                print("Masuk Setengah")
                                detail_step = step + round(elapsed/times,2)
                                com_feedback = math.degrees(math.atan2(Z,X))
                                error_c = round(com_feedback - setpoint_com(detail_step),3)
                                error_p = round(kalAngleYfix - setpoint_pitch(detail_step),3)
                                times_M = ((times - times_min) / (times_max-times_min)) * 100
                                detail_step_M = ((detail_step - step_min) / (step_max-step_min))* 100
                                pitch = ((round(kalAngleYfix,3)- Pitch_min)/(Pitch_max-Pitch_min)) * 100
                                CMx = ((X-CMx_min)/(CMx_max - CMx_min)) * 100
                                CMy = ((Y-CMy_min)/(CMy_max - CMy_min)) * 100
                                CMz = ((Z-CMz_min)/(CMz_max - CMz_min)) * 100
                                Ankle_angle = ((dxl[15].presentpos-Ankle_min)/(Ankle_max - Ankle_min)) * 100
                                omega = ((gyroY - omega_min) / (omega_max-omega_min)) * 100
                                e_pitch = ((error_p - e_pitch_min) / (e_pitch_max-e_pitch_min)) * 100
                                e_com = ((error_c - e_com_min) / (e_com_max - e_com_min)) * 100

                                data_input = np.array([times_M,detail_step_M,\
                                    e_pitch,e_com, \
                                    # pitch, CMx, CMy, CMz, \
                                    Ankle_angle,omega], dtype='float32')
                                #ankle speed xmin 0.555 xmax 17.316
                                #goal xmin 288 xmax 550
                                #data = [times,str(detail_step),str(angle_Imu['Pitch']),str(X), str(Y),str(Z), \
                                #str(dxl[15].presentpos)]
                                # print('input:')
                                # print(data_input)
                                
                                inputs = torch.from_numpy(data_input)
                                #print('before: ', data)
                                #print('input: ',inputs)
                                before = time.time()
                                pred = model(inputs)
                                pred_n = pred.detach().numpy()
                                #print('Model time: ', time.time() - before)
                                predict = []
                                for i in pred_n: 
                                    predict.append(i)
                                    #print('pred: ', i)
                                
                                predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                if inp_pattern == '2':
                                    if step == 4:
                                        st += 1
                                        if st == 1:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                        else:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                        # predict_speed = 10
                                    elif step ==5:
                                        st += 1
                                        if st == 1:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) + 3
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            if times == 0.4:
                                                if predict_goal > 360:
                                                    predict_goal = 356
                                                    predict_speed = 4
                                        else:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) + 4
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            if times == 0.4:
                                                if predict_goal < 374:
                                                    predict_goal = 374
                                                    predict_speed = 6
                                    elif step ==3:
                                        st += 1
                                        if st == 1:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                        else:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            # predict_goal = 285
                                    
                                    elif step ==6:
                                        st += 1
                                        print('st: ', st)
                                        if st == 1:
                                            # if times == 0.4:
                                            #     predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) + 2
                                            # else:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            if predict_speed < 0:
                                                predict_speed = 1
                                        else:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) 
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            if predict_speed < 0:
                                                predict_speed = 1
                                            # predict_goal = 383
                                    # flag_stab = 1
                                
                                print('speed_1: ', predict_speed)
                                print('goal_1 : ', predict_goal)
                                
                                predict_goal_data = ((predict_goal-Goal_min)/(Goal_max-Goal_min))*100
                                predict_speed_data = ((predict_speed-Speed_min)/(Speed_max-Speed_min))*100
                                
                                data2 = ([times_M,str(detail_step_M), e_pitch, e_com, str(pitch),str(CMx), str(CMy),str(CMz), \
                                str(Ankle_angle),round(omega,3), str(predict_speed_data),str(predict_goal_data)])
                                data2_save.append(data2)
                                data2 = []
                                dif_predict = predict_goal - dxl[15].default
                                dxl[15].moveSync(int(predict_goal),predict_speed,dxl[15].presentpos, time_type='rpm')
                                dxl[14].moveSync(int(dxl[14].default-dif_predict),predict_speed,dxl[14].presentpos, time_type='rpm')
                                robot.syncWrite()
                                timer_stab = time.time()
                        if (inp_pattern == '1' and step>=3):
                            if times < 0.25:
                                stab_1 = times/2.3
                            else:
                                stab_1 = times/2.7
                            if dt_stab >= (stab_1):
                                print("Masuk Setengah")
                                detail_step = step + round(elapsed/times,2)
                                com_feedback = math.degrees(math.atan2(Z,X))
                                error_c = round(com_feedback - setpoint_com(detail_step),3)
                                error_p = round(kalAngleYfix - setpoint_pitch(detail_step),3)

                                times_M = ((times - times_min) / (times_max-times_min)) * 100
                                detail_step_M = ((detail_step - step_min) / (step_max-step_min))* 100
                                pitch = ((round(kalAngleYfix,3)- Pitch_min)/(Pitch_max-Pitch_min)) * 100
                                CMx = ((X-CMx_min)/(CMx_max - CMx_min)) * 100
                                CMy = ((Y-CMy_min)/(CMy_max - CMy_min)) * 100
                                CMz = ((Z-CMz_min)/(CMz_max - CMz_min)) * 100
                                Ankle_angle = ((dxl[15].posNow-Ankle_min)/(Ankle_max - Ankle_min)) * 100
                                omega = ((gyroY - omega_min) / (omega_max-omega_min)) * 100
                                e_pitch = ((error_p - e_pitch_min) / (e_pitch_max-e_pitch_min)) * 100
                                e_com = ((error_c - e_com_min) / (e_com_max - e_com_min)) * 100
                                
                                data_input = np.array([times_M,detail_step_M,\
                                    e_pitch,e_com, \
                                    # pitch, CMx, CMy, CMz, \
                                    Ankle_angle,omega], dtype='float32')
                                # data_input = np.array([times_M,detail_step_M,pitch, CMx, CMy, CMz, \
                                # Ankle_angle,omega], dtype='float32')
                                #ankle speed xmin 0.555 xmax 17.316
                                #goal xmin 288 xmax 550
                                #data = [times,str(detail_step),str(angle_Imu['Pitch']),str(X), str(Y),str(Z), \
                                #str(dxl[15].presentpos)]
                                # print('input:')
                                # print(data_input)
                                
                                inputs = torch.from_numpy(data_input)
                                #print('before: ', data)
                                #print('input: ',inputs)
                                before = time.time()
                                pred = model(inputs)
                                pred_n = pred.detach().numpy()
                                #print('Model time: ', time.time() - before)
                                predict = []
                                for i in pred_n: 
                                    predict.append(i)
                                    #print('pred: ', i)
                                
                                predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                if inp_pattern == '1':
                                    if step == 3:
                                        print('st: ', st)
                                        st += 1
                                        if st == 1:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) 
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            # 0.3
                                            # if predict_goal> 250:
                                            #     predict_goal = 248
                                            #     predict_speed = 4 
                                            # 0.25
                                            # if predict_speed< 11:
                                            #     predict_speed = 13
                                            #     predict_goal = 242
                            
                                        elif st == 2:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)

                                            # 0.25
                                            # if predict_speed < 10:
                                            #     predict_speed = 12
                                            # if predict_goal > 251:
                                            #     predict_goal = 244
                                                # predict_speed = 14                                          
                                            # 0.3
                                            # if predict_speed < 14:
                                            #     predict_speed = 16
                                            #     predict_goal = 260
                                            # 0.5
                                            # predict_goal = 244
                                            # predict_speed = 1
                                            # 0.4
                                            # if predict_goal > 250:
                                            #     predict_goal = 244
                                            #     predict_speed = 4
                                        else:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)

                                    if step == 4:
                                        st += 1
                                        print('st: ', st)
                                        if st == 1:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) 
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            # konfigurasi 0.3,0.5
                                            # predict_speed = 10
                                            # predict_goal = 357
                                            # 0.4
                                            # if predict_goal < 320:
                                            #     predict_goal = 357
                                            #     predict_speed = 10
                                            # 0.25
                                            # predict_speed = 10
                                            # predict_goal = 350
                                        elif st == 2:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            # 0.25
                                            # if predict_goal > 335:
                                            #     predict_goal = 334
                                            #     predict_speed = 7
                                            # elif predict_goal < 315:
                                            #     predict_goal = 327
                                            # if predict_speed < 0:
                                            #     predict_speed = 1

                                            # 0.4
                                            # elif predict_speed < 3:
                                            #     predict_speed = 8
                                            # if predict_goal > 327:
                                            #     predict_goal = 327
                                            #     predict_speed = 5

                                            # konfigurasi 0.5
                                            # predict_goal = 327
                                            # konfigurasi 0.3
                                            # predict_goal = 327     
                                            # predict_speed = 1
                                        else:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                        
                                    if step ==5:
                                        predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                        predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                        st += 1
                                        print('st: ', st)
                                        if st == 1:
                                            #konfigurasi 0.3
                                            # predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) - 3

                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3) 
                                            # if predict_goal < 370:
                                            #     predict_goal = 350
                                        elif st == 2:
                                            # 0.3
                                            # predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) - 5
                                            # predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3) + 10
                                            # 0.25
                                            # predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) + 3

                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            # konfigurasi 0.5
                                            # if predict_goal < 365:
                                            #     predict_goal = 374
                                            # predict_goal = 376
                                        else:
                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            # flag_stab = 1
                                        # if predict_goal > 390:
                                        #     if predict_speed < 2:
                                        #         predict_goal =374
                                        #         predict_speed = 2
                                        # if predict_speed > 50:
                                        #     predict_speed = 5
                                        # elif predict_speed < 1:
                                        #     predict_speed = 2

                                    if step >=6:
                                        predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                        predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                        st += 1
                                        print('st: ', st)
                                        if st == 1:
                                            # 0.3
                                            # predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) - 3
                                            # 0.25
                                            # predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3) - 10

                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            # 0.5
                                            # predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3) - 30
                                            # if predict_goal <374:
                                            #     predict_goal = 374
                                            # if predict_speed > 5:
                                            #     predict_speed = 1

                                            # 0.4
                                            # if predict_speed > 5:
                                            #     predict_speed = 1
                                                
                                            # 0.3
                                            # if predict_speed > 5:
                                            #     predict_speed = 3

                                        elif st == 2:
                                            # 0.3
                                            # predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3) - 9

                                            predict_speed = round((((predict[0]/100)*(Speed_max-Speed_min)) + Speed_min),3)
                                            predict_goal = round((((predict[1]/100)*(Goal_max-Goal_min)) + Goal_min),3)
                                            # predict_goal = 374
                                            if predict_speed<0:
                                                predict_speed = 1
                                            # 0.5,0.4
                                            # elif predict_speed > 5:
                                            #     predict_speed = 2
                                            
                                print('speed_1: ', predict_speed)
                                print('goal_1 : ', predict_goal)
                                
                                predict_goal_data = ((predict_goal-Goal_min)/(Goal_max-Goal_min))*100
                                predict_speed_data = ((predict_speed-Speed_min)/(Speed_max-Speed_min))*100
                                
                                data2 = ([times_M,str(detail_step_M),e_pitch,e_com,str(pitch),str(CMx), str(CMy),str(CMz), \
                                str(Ankle_angle),round(omega,3), round(predict_speed_data,3), round(predict_goal_data,3)])
                                data2_save.append(data2)
                                data2 = []
                                dif_predict = predict_goal - dxl[15].default
                                dxl[15].moveSync(int(predict_goal),predict_speed,dxl[15].presentpos, time_type='rpm')
                                dxl[14].moveSync(int(dxl[14].default-dif_predict),predict_speed,dxl[14].presentpos, time_type='rpm')
                                robot.syncWrite()
                                timer_stab = time.time()
                                
                    if ((dt>=0.1) and ((time.time() - start+0.1) < times)):
                        if using_model == 'n':
                            goal = round((((dxl[15].prevGoal-Goal_min)/(Goal_max-Goal_min)) * 100),3)
                            speedd = round(((((dxl[15].speed*0.111)-Speed_min)/(Speed_max-Speed_min)) * 100),3)
                            predict_goal = goal
                            predict_speed = speedd
                        # print("dt: ", dt)
                        # print('elapsed: ', elapsed)
                        timer = time.time()
                        # st = time.time()
                        COM = robot.com(read_flag=0, Time_read=elapsed)
                        for ex in COM['x']:
                            X = ex
                        for ye in COM['y']:
                            Y = ye
                        for zet in COM['z']:
                            Z = zet
                        
                        # angle_Imu = imu.IMU(0.005, 11, Komp[0], Komp[1], KalmanX, KalmanY, zero_gyro = Komp[2])
                        
                        detail_step = str(step + round(elapsed/times,2))
                        com_feedback = math.degrees(math.atan2(Z,X))
                        error_c = round(com_feedback - setpoint_com(detail_step),3)
                        error_p = round(kalAngleYfix - setpoint_pitch(detail_step),3)
                        # print("waktu cb: ", time.time() - sta)
                        data = [times,detail_step, error_p, error_c,\
                            # setpoint_pitch(detail_step), setpoint_com(detail_step), \
                            str(round(kalAngleYfix,3)),str(X),\
                            str(Y),str(Z),str(dxl[15].presentpos),round(gy,3),str(predict_speed), \
                            predict_goal, title]
                        data_list.append(data)
                        # print('time CoM: ', time.time()-st)
                        
                print('elapsed times: %.2f' %elapsed)

                step = step + 1
                # print('index: ', indexMoving)
            
            timer = time.time()
            flag_def = 0
            detail_step = step
            while time.time() - timer <= 3:
                detail_step = step + time.time() - timer
                if add_data == 'y':
                    
                    if flag_def == 0:
                        for obj in dxl[14],dxl[15]:
                            prev = obj.prevGoal
                            obj.moveSync(obj.default, speed_last,prev)
                        # print('prev: ', obj.prenvGoal)
                        robot.readAllPresentPos(real_data_flag=0, time_read=(time.time()-timer))
                        print("Def......")
                        print('ankle: ', dxl[15].posNow)
                        robot.syncWrite()
                        detail_step = 6
                        com_feedback = math.degrees(math.atan2(Z,X))
                        error_c = round(com_feedback - setpoint_com(detail_step),3)
                        error_p = round(kalAngleYfix - setpoint_pitch(detail_step),3)
                        times_M = ((times - times_min) / (times_max-times_min)) * 100
                        detail_step_M = ((detail_step - step_min) / (step_max-step_min))* 100
                        pitch = ((round(kalAngleYfix,3)- Pitch_min)/(Pitch_max-Pitch_min)) * 100
                        CMx = ((X-CMx_min)/(CMx_max - CMx_min)) * 100
                        CMy = ((Y-CMy_min)/(CMy_max - CMy_min)) * 100
                        CMz = ((Z-CMz_min)/(CMz_max - CMz_min)) * 100
                        Ankle_angle = ((dxl[15].posNow-Ankle_min)/(Ankle_max - Ankle_min)) * 100
                        omega = ((gyroY - omega_min) / (omega_max-omega_min)) * 100
                        predict_goal_data = ((dxl[15].prevGoal-Goal_min)/(Goal_max-Goal_min))*100
                        predict_speed_data = (((dxl[15].speed*0.111)-Speed_min)/(Speed_max-Speed_min))*100
                        e_pitch = ((error_p - e_pitch_min) / (e_pitch_max-e_pitch_min)) * 100
                        e_com = ((error_c - e_com_min) / (e_com_max - e_com_min)) * 100
                        data2 = ([times_M,str(detail_step_M),e_pitch,e_com,str(pitch),str(CMx), str(CMy),str(CMz), \
                    str(Ankle_angle),round(omega,3), round(predict_speed_data,3), round(predict_goal_data,3)])
                        data2_save.append(data2)
                        flag_def = 1
                robot.readAllPresentPos(real_data_flag=0, time_read=(time.time()-timer))
                times_M = ((times - times_min) / (times_max-times_min)) * 100
                detail_step_M = ((detail_step - step_min) / (step_max-step_min))* 100
                pitch = ((round(kalAngleYfix,3)- Pitch_min)/(Pitch_max-Pitch_min)) * 100
                CMx = ((X-CMx_min)/(CMx_max - CMx_min)) * 100
                CMy = ((Y-CMy_min)/(CMy_max - CMy_min)) * 100
                CMz = ((Z-CMz_min)/(CMz_max - CMz_min)) * 100
                Ankle_angle = ((dxl[15].posNow-Ankle_min)/(Ankle_max - Ankle_min)) * 100
                gy = gyroY
                omega = ((gyroY - omega_min) / (omega_max-omega_min)) * 100
                predict_goal_data = ((dxl[15].prevGoal-Goal_min)/(Goal_max-Goal_min))*100
                predict_speed_data = (((dxl[15].speed*0.111)-Speed_min)/(Speed_max-Speed_min))*100
                
                # data2 = ([times_M,str(detail_step_M),str(pitch),str(CMx), str(CMy),str(CMz), \
                #     str(Ankle_angle),round(omega,3), round(predict_speed_data,3), round(predict_goal_data,3)])
                com_feedback = math.degrees(math.atan2(Z,X))
                error_c = round(com_feedback - setpoint_com(detail_step),3)
                error_p = round(kalAngleYfix - setpoint_pitch(detail_step),3)
                        # print("waktu cb: ", time.time() - sta)
                data = [times,detail_step, error_p, error_c,\
                    # setpoint_pitch(detail_step),setpoint_com(detail_step), \
                    str(round(kalAngleYfix,3)),str(X),\
                    str(Y),str(Z),str(dxl[15].presentpos),round(gy,3),str(predict_speed), \
                    predict_goal, title]
                # data2_save.append(data2)
                data_list.append(data)
                data = []
                time.sleep(0.1)
                
            
            input_confirm = input("Press y to input data: ")
            # input_confirm = input("Press y to input data: ")
            
            if input_confirm =='y':
                print('recent file: ', name)
                new = input("File Baru (y/n): ")
                fall = input("Robot Fall?: ")
                if new == 'y':
                    if fall == 'y':
                        number = input("Enter data name: ")
                        name = 'Data' + number + '_Fall_'+ title+  '.csv'
                        name2 = 'Data' + number + '_Fall_'+ title+ '_2'+ '.csv'
                        print("saving to file: ", name)
                        Title_wrapper = ['Pola: ' +  title] 
                        state_fall = 1
                        with open(name, mode='a') as file:
                            Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            # Data.writerow(["    "])
                            # Data.writerow(Title_wrapper)
                            Data.writerow(['Periode(s)', 'Step','error pitch', 'error com',\
                                # 'setpoint pitch', 'setpoint_com',\
                                'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                                'Left Ankle Angle Now','Omega (theta/s)','Left Ankle Speed(rpm)', 'Goal', 'Pola'])
                            Data.writerows(data_list)
                            Data.writerow(["    "])
                            Data.writerows(data2_save)
                            Data.writerow(["    "])
                        with open(name2, mode='a') as file:
                            Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            Data.writerow(['Periode(s)', 'Step','error pitch', 'error com',\
                                # 'setpoint pitch', 'setpoint_com',\
                                'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                                'Left Ankle Angle Now','Omega (theta/s)','Left Ankle Speed(rpm)', 'Goal'])
                            Data.writerows(data2_save)
                            # Data.writerow(["    "])
                    else:
                        number = input("Enter data name: ")
                        print('tittle: ', title)
                        name = 'Data' + number + '_'+ title +'.csv'                        
                        name2 = 'Data' + number + '_' + title + '_2'+ '.csv'
                        print("saving to file: ", name)
                        Title_wrapper = ['Pola: ' +  title] 
                        with open(name, mode='a') as file:
                            Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            # Data.writerow(["    "])
                            # Data.writerow(Title_wrapper)
                            Data.writerow(['Periode(s)', 'Step','error pitch', 'error com',\
                                # 'setpoint pitch', 'setpoint_com',\
                                'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                                'Left Ankle Angle Now','Omega (theta/s)','Left Ankle Speed(rpm)', 'Goal', 'Pola'])
                            Data.writerows(data_list)
                            Data.writerow(["    "])
                            Data.writerows(data2_save)
                            Data.writerow(["    "])
                        with open(name2, mode='a') as file:
                            Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            Data.writerow(['Periode(s)', 'Step','error pitch', 'error com',\
                                # 'setpoint pitch', 'setpoint_com',\
                                'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                                'Left Ankle Angle Now','Omega (theta/s)','Left Ankle Speed(rpm)', 'Goal'])
                            Data.writerows(data2_save)
                            # Data.writerow(["    "])
                        
                    
                else:
                    if fall == 'y':
                        name = 'Data' + number + '_Fall_' + title + '.csv'
                        name2 = 'Data' + number + '_Fall_' + title + '_2' + '.csv'
                        print("saving to file: ", name)
                        Title_wrapper = ['Pola: ' +  title] 
                        if state_fall == 1:
                            with open(name, mode='a') as file:
                                Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                                # Data.writerow(["    "])
                                # Data.writerow(Title_wrapper)
                                Data.writerows(data_list)
                                Data.writerow(["    "])
                                Data.writerows(data2_save)
                                Data.writerow(["    "])

                            with open(name2, mode='a') as file:
                                Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                                Data.writerows(data2_save)
                                # Data.writerow(["    "])
                        else:
                            with open(name, mode='a') as file:
                                Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                                # Data.writerow(["    "])
                                # Data.writerow(Title_wrapper)
                                Data.writerow(['Periode(s)', 'Step','error pitch', 'error com',\
                                    # 'setpoint pitch', 'setpoint_com',\
                                    'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                                    'Left Ankle Angle Now','Omega (theta/s)','Left Ankle Speed(rpm)', 'Goal', 'Pola'])
                                Data.writerows(data_list)
                                Data.writerow(["    "])
                                Data.writerows(data2_save)
                                Data.writerow(["    "])

                            with open(name2, mode='a') as file:
                                Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                                Data.writerows(data2_save)
                                # Data.writerow(["    "])

                            state_fall = 1
                    else:
                        print('tittle: ', title)
                        name = 'Data' + number + '_' + title + '.csv'
                        name2 = 'Data' + number + '_' + title + '_2'+ '.csv'
                        print("saving to file: ", name) 
                        Title_wrapper = ['Pola: ' +  title] 
                        with open(name, mode='a') as file:
                            Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            # Data.writerow(["    "])
                            # Data.writerow(Title_wrapper)
                            # Data.writerow(['Periode(s)','Elapsed', 'Step', 'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                            # 'Left Ankle Speed(rpm)','Right Ankle Speed(rpm)', 'Left Ankle Angle', 'Right Ankle Angle', 'Pola'])
                            Data.writerows(data_list)
                            Data.writerow(["    "])
                            Data.writerows(data2_save)
                            Data.writerow(["    "])

                        with open(name2, mode='a') as file:
                                Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                                Data.writerows(data2_save)
                                # Data.writerow(["    "])
              
            againn = input("Masukkan y untuk merecord ulang: ")
            if againn != 'y':
                break
                
            if againn == 'y':
                step = 0
                data_list= []
                X = 0
                Y = 0
                Z = 0
        robot.readAllPresentPos()
        print("Press E to enable or ESC to disable torque or 2 to disable hip")
        inp = getch()
        if inp == 'e':
            robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
        if inp == chr(0x1b):
            robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
        if inp == '2':
            robot.cekServoHip(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
            while(1):
                print("Press E to enable or ESC to out")
                inpu = getch()
                if inpu == 'e':
                    robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
                    robot.readAllPresentPos()
                else:
                    break
        robot.portHandler.closePort()
   
    else:
        while(1):
            if t2_start == 1:
                break
            time.sleep(0.1)
                
        dt_sampling = float(input("Masukkan dt Imu: "))
        dt = dt_sampling
        kalmanX = KalmanAngle()
        kalmanY = KalmanAngle()
        kompX = 0
        kompY = 0

        radToDeg = 57.2957786
        kalAngleX = 0
        kalAngleY = 0
        AcGy = ambilData()

        accX = AcGy[0]
        accY = AcGy[1]
        accZ = AcGy[2]
        
        roll = math.atan(accY/math.sqrt((accX**2)+(accZ**2))) * radToDeg
        pitch = math.atan2(-accX,accZ) * radToDeg

        # print("Roll awal = ",roll)
        # print("Pitch awal = ",pitch)

        kalmanX.setAngle(roll)
        kalmanY.setAngle(pitch)
        gyroXAngle = roll;
        gyroYAngle = pitch;
        compAngleX = roll;
        compAngleY = pitch;

        timer = time.time()
        flag = 0
        loopke = 0
        zero = 0
        calibrate = 0 
        kalAngleYfix = 0
        while True:
            if againn != 'y':
                break
            if calibrate == 0:
                print('calibrating...')
                for i in range(100):
                    AcGy = ambilData()
                    zero += 0 - AcGy[4]
                    time.sleep(0.05)
                calibrate = 1
                zero /= 100
                # print('zero: ', zero)

            loopke += 1
            if(flag >100): #Problem with the connection
                print("There is a problem with the connection")
                flag=0
                continue
            # try:
            AcGy = ambilData()
            accX = AcGy[0]
            accY = AcGy[1]
            accZ = AcGy[2]


            gyroX = AcGy[3]
            gyroY = AcGy[4] + zero
            gyroZ = AcGy[5]

            dt = time.time() - timer
            timer = time.time()
            # print('dt: ', dt)

            roll = math.atan2(accY,accZ) * radToDeg
            pitch = math.atan2(-accX,accZ) * radToDeg


            # print('gyro bef:', gyroX)
            gyroXRate = gyroX
            gyroYRate = gyroY


            if((pitch < -90 and kalAngleY >90) or (pitch > 90 and kalAngleY < -90)):
                kalmanY.setAngle(pitch)
                complAngleY = pitch
                kalAngleY   = pitch
                gyroYAngle  = pitch
            else:
                kalAngleY = kalmanY.getAngle(pitch,gyroYRate,dt)

            if(abs(kalAngleY)>90):
                gyroXRate  = -gyroXRate
                kalAngleX = kalmanX.getAngle(roll,gyroXRate,dt)
            #angle = (rate of change of angle) * change in time
            gyroXAngle = gyroXRate * dt
            gyroYAngle = gyroYAngle * dt

            #compAngle = constant * (old_compAngle + angle_obtained_from_gyro) + constant * angle_obtained from accelerometer
            compAngleX = 0.93 * (compAngleX + kalmanX.rate * dt) + 0.07 * roll
            compAngleY = 0.93 * (compAngleY + kalmanY.rate * dt) + 0.07 * pitch

            if ((gyroXAngle < -180) or (gyroXAngle > 180)):
                gyroXAngle = kalAngleX
            if ((gyroYAngle < -180) or (gyroYAngle > 180)):
                gyroYAngle = kalAngleY


            if (kalAngleY > -180 and kalAngleY<0):
                kalAngleYfix = round(kalAngleY + 360,3) + kompY
            else:
                kalAngleYfix = round(kalAngleY,3) + kompY

            # print('Pitch  : %1f' %(kalAngleY),'rate    : %1f'%kalmanY.rate,'bias: %1f' %(kalmanY.bias),sep='\t')
            # print('pitch_a: ', round(kalAngleYfix,3), 'raw_gyro: %1f raw_acc: %1f'%(gyroYRate,pitch))
            # print()

            time.sleep(dt_sampling) #kalau terlalu lambat, maka semakin lambat pula pasnya

            if (loopke == 15): #pasang kompensator
                # kompX = 0 - kalAngleXfix
                kompY = round(0 - kalAngleYfix,3)
                print("Done calibrating IMU...")
        
    # except Exception as exc:
        # flag += 1
        # KalmanX = KalmanAngle()
        # KalmanY = KalmanAngle()
        # dt = 0.1
        # Komp = imu.IMU(dt,0,0,0, KalmanX, KalmanY)
        # print('komp: ', Komp)
        # dt_sampling = float(input("Masukkan dt Imu: "))
        # timer_IMU = time.time() - dt_sampling
        # while(1):
            # print("Looping imu")
            # if againn != 'y':
                # break
            # dt = time.time() - timer_IMU
            # print('dt: ', dt)
            # angle_Imu = imu.IMU(dt, 11, Komp[0], Komp[1], KalmanX, KalmanY, zero_gyro = Komp[2])
            # timer_IMU = time.time()
            # time.sleep(dt_sampling)
        
    
def main_task():
    
    t1 = threading.Thread(target=thread_task, args=(1,)) 
    t2 = threading.Thread(target=thread_task, args=(0,)) 
    
    try:
		# Start the threa
        t1.start()
        t2.start()
        while t1.is_alive() and t2.is_alive():
            t1.join(0.5)
            t2.join(0.5)

	# When ctrl+c is received
    except KeyboardInterrupt as e:
        t1.alive = False
        t2.alive = False
        
        t1.join()
        t2.join()
        sys.exit(e)

if os.name == 'nt':
    import msvcrt
    def getch():
        return msvcrt.getch().decode()
else:
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    def getch():
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch



main_task()
