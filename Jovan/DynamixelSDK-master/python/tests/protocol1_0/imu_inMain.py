from mpu6050 import mpu6050
import time
import math



def ambilData():
    mpu = mpu6050(0x68)
    # print("Temp : ", str(mpu.get_temp()))
    # print()
    imu = [0. for k in range (6)] #inisialisasi imu
    accel_data = mpu.get_accel_data()
    imu[0] = accel_data['x']
    imu[1] = accel_data['y']
    imu[2] = accel_data['z']

    gyro_data = mpu.get_gyro_data()
    imu[3] = gyro_data['x']
    imu[4] = gyro_data['y']
    imu[5] = gyro_data['z']
    #print("Imu : " + str(imu))
    #print()
    return imu
    
# time.sleep(1)



def IMU(dt, loopke, KompX, KompY, KalX_obj, KalY_obj, zero_gyro=0):
    
    kalmanX = KalX_obj
    kalmanY = KalY_obj
    kompX = 0
    kompY = 0

    radToDeg = 57.2957786
    kalAngleX = 0
    kalAngleY = 0
    kalAngleYfix = 0
    kalAngleXfix = 0

    AcGy = [0. for k in range (6)] #inisialisasi nilai akselerometer dan gyroscope
    AcGy = ambilData()
    accX = AcGy[0]
    accY = AcGy[1]
    accZ = AcGy[2]

    roll = math.atan2(accY,accZ) * radToDeg
    pitch = math.atan2(-accX,accZ) * radToDeg

    # print("Roll awal = ",roll)
    # print("Pitch awal = ",pitch)
    flag = 0
    if loopke == 0:
        zero = zero_gyro
        kalmanX.setAngle(roll)
        kalmanY.setAngle(pitch)
        loopkeNew = loopke
        print("Calibrating...")
        for i in range(100):
            AcGy = ambilData()
            zero += 0 - AcGy[4]
            time.sleep(0.05)
        calibrate = 1
        zero /= 100
        state = 0
        while loopkeNew <13:
            # print("MASUKK")
            loopkeNew += 1
            if(flag >100): #Problem with the connection
                print("There is a problem with the connection")
                flag=0
            AcGy = ambilData()
            accX = AcGy[0]
            accY = AcGy[1]
            accZ = AcGy[2]
            
            gyroX = AcGy[3]
            gyroY = AcGy[4] + zero
            gyroZ = AcGy[5]

            # dt = time.time() - timer
            # timer = time.time()

            roll = math.atan2(accY,accZ) * radToDeg
            pitch = math.atan2(-accX,accZ) * radToDeg

            gyroXRate = gyroX
            gyroYRate = gyroY

            if((pitch < -90 and kalAngleY >90) or (pitch > 90 and kalAngleY < -90)):
                kalmanY.setAngle(pitch)
                complAngleY = pitch
                kalAngleY   = pitch
                gyroYAngle  = pitch
            else:
                kalAngleY = kalmanY.getAngle(pitch,gyroYRate,dt)

            if(abs(kalAngleY)>90):
                gyroXRate  = -gyroXRate
                kalAngleX = kalmanX.getAngle(roll,gyroXRate,dt)

            #angle = (rate of change of angle) * change in time
            gyroXAngle = gyroXRate * dt
            gyroYAngle = gyroYRate * dt

            #compAngle = constant * (old_compAngle + angle_obtained_from_gyro) + constant * angle_obtained from accelerometer
            # compAngleX = 0.93 * (compAngleX + gyroXRate * dt) + 0.07 * roll
            # compAngleY = 0.93 * (compAngleY + gyroYRate * dt) + 0.07 * pitch

            if ((gyroXAngle < -180) or (gyroXAngle > 180)):
                gyroXAngle = kalAngleX
            if ((gyroYAngle < -180) or (gyroYAngle > 180)):
                gyroYAngle = kalAngleY
            
            if (kalAngleY > -180 and kalAngleY<0):
                kalAngleYfix = round(kalAngleY + 360,3) + kompY
            else:
                kalAngleYfix = round(kalAngleY,3) + kompY

            
            # print("Angle Pitch: " + str(kalAngleYfix), '    ', 'state: ', state)
            # print(str(roll)+"  "+str(gyroXAngle)+"  "+str(compAngleX)+"  "+str(kalAngleX)+"  "+str(pitch)+"  "+str(gyroYAngle)+"  "+str(co$
            # print("---------------------------------------------------------------------------------")
            # time.sleep(0.1) #kalau terlalu lambat, maka semakin lambat pula pasnya
            time.sleep(dt)
            
            if (loopkeNew >= 10 and state == 0): #pasang kompensator
            
                kompX = 0 - kalAngleXfix
                kompY = 0 - kalAngleYfix
                state = 1
        return (kompX,kompY,zero)   
        
    else:
        kompX = KompX
        kompY = KompY
        # print("MASUKK Else")
        AcGy = ambilData()
        accX = AcGy[0]
        accY = AcGy[1]
        accZ = AcGy[2]
        
        gyroX = AcGy[3]
        gyroY = AcGy[4] + zero_gyro
        gyroZ = AcGy[5]

        # dt = time.time() - timer
        # timer = time.time()

        roll = math.atan2(accY,accZ) * radToDeg
        pitch = math.atan2(-accX,accZ) * radToDeg

        gyroXRate = gyroX
        gyroYRate = gyroY


        if((pitch < -90 and kalAngleY >90) or (pitch > 90 and kalAngleY < -90)):
            kalmanY.setAngle(pitch)
            complAngleY = pitch
            kalAngleY   = pitch
            gyroYAngle  = pitch
        else:
            kalAngleY = kalmanY.getAngle(pitch,gyroYRate,dt)

        if(abs(kalAngleY)>90):
            gyroXRate  = -gyroXRate
            kalAngleX = kalmanX.getAngle(roll,gyroXRate,dt)

        #angle = (rate of change of angle) * change in time
        gyroXAngle = gyroXRate * dt
        gyroYAngle = gyroYRate * dt

        #compAngle = constant * (old_compAngle + angle_obtained_from_gyro) + constant * angle_obtained from accelerometer
        # compAngleX = 0.93 * (compAngleX + gyroXRate * dt) + 0.07 * roll
        # compAngleY = 0.93 * (compAngleY + gyroYRate * dt) + 0.07 * pitch

        if ((gyroXAngle < -180) or (gyroXAngle > 180)):
            gyroXAngle = kalAngleX
        if ((gyroYAngle < -180) or (gyroYAngle > 180)):
            gyroYAngle = kalAngleY
        
        if (kalAngleY > -180 and kalAngleY<0):
                kalAngleYfix = round(kalAngleY + 360,3) + kompY
        else:
            kalAngleYfix = round(kalAngleY,3) + kompY
        # print('Pitch  : %1f' %(kalAngleY),'rate    : %1f'%kalmanY.rate,'bias: %1f' %(kalmanY.bias),sep='\t')
        ## print()
        # print('pitch_a: ', round(kalAngleYfix,3), 'raw_gyro: %1f raw_acc: %1f'%(gyroYRate,pitch))
        # print()

        # print("Angle Roll: " + str(kalAngleXfix)+"   " +"Angle Pitch: " + str(kalAngleYfix))
        #print(str(roll)+"  "+str(gyroXAngle)+"  "+str(compAngleX)+"  "+str(kalAngleX)+"  "+str(pitch)+"  "+str(gyroYAngle)+"  "+str(co$
        # print("---------------------------------------------------------------------------------")
        # time.sleep(0.1) #kalau terlalu lambat, maka semakin lambat pula pasnya
        return{'Pitch':round(kalAngleYfix,3), 'Roll': round(kalAngleXfix,3), 'pitch_acc':round(pitch,3), 'pitch_gyro':round(gyroYRate,3), 'kalAngleY':round(kalAngleY,3)}
