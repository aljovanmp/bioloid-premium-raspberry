import os
import get_CSV
import time
import numpy as np
from dynamixel_sdk import *                    # Uses Dynamixel SDK library
import servo
import robot_handler as rh
import pandas as pd

if os.name == 'nt':
    import msvcrt
    def getch():
        return msvcrt.getch().decode()
else:
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    def getch():
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

dxl = []
fwd = []
DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller
                                                # ex) Windows: "COM1"   Linux: "/dev/ttyUSB0" Mac: "/dev/tty.usbserial-*"
# Protocol version
PROTOCOL_VERSION            = 1.0               # See which protocol version is used in the Dynamixel

# Initialize PortHandler instance
# Set the port path
# Get methods and members of PortHandlerLinux or PortHandlerWindows
    
portHandler = PortHandler(DEVICENAME)


# Initialize PacketHandler instance
# Set the protocol version
# Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
packetHandler = PacketHandler(PROTOCOL_VERSION)

dxl.append(servo.Servo(235,1,portHandler,packetHandler))
dxl.append(servo.Servo(788,2,portHandler,packetHandler))
dxl.append(servo.Servo(279,3,portHandler,packetHandler))
dxl.append(servo.Servo(744,4,portHandler,packetHandler))
dxl.append(servo.Servo(462,5,portHandler,packetHandler))
dxl.append(servo.Servo(561,6,portHandler,packetHandler))
dxl.append(servo.Servo(358,7,portHandler,packetHandler))
dxl.append(servo.Servo(666,8,portHandler,packetHandler))
dxl.append(servo.Servo(507,9,portHandler,packetHandler))
dxl.append(servo.Servo(516,10,portHandler,packetHandler))
dxl.append(servo.Servo(341,11,portHandler,packetHandler))
dxl.append(servo.Servo(682,12,portHandler,packetHandler))
dxl.append(servo.Servo(240,13,portHandler,packetHandler))
dxl.append(servo.Servo(783,14,portHandler,packetHandler))
dxl.append(servo.Servo(647,15,portHandler,packetHandler))
dxl.append(servo.Servo(376,16,portHandler,packetHandler))
dxl.append(servo.Servo(507,17,portHandler,packetHandler))
dxl.append(servo.Servo(516,18,portHandler,packetHandler))

# Control table address
ADDR_AX_TORQUE_ENABLE      = 24            # Control table address is different in Dynamixel model
ADDR_AX_GOAL_POSITION      = 30
ADDR_AX_PRESENT_POSITION   = 36
ADDR_AX_MOVING             = 46

# Data Byte Length
LEN_MX_GOAL_POSITION       = 4
LEN_MX_PRESENT_POSITION    = 2

BAUDRATE                    = 1000000          # Dynamixel default baudrate : 57600

TORQUE_ENABLE               = 1                 # Value for enabling the torque
TORQUE_DISABLE              = 0                 # Value for disabling the torque

# Initialize GroupSyncWrite instance
groupSyncWrite = GroupSyncWrite(portHandler, packetHandler, ADDR_AX_GOAL_POSITION, LEN_MX_GOAL_POSITION)
CoM = []
robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite,fwd, CoM)
import csv
data_list = []
data_list_2 = []



# Open port
if robot.portHandler.openPort():
    print("Succeeded to open the port")
else:
    print("Failed to open the port")
    print("Press any key to terminate...")
    getch()
    quit()

# Set port baudrate
if robot.portHandler.setBaudRate(BAUDRATE):
    print("Succeeded to change the baudrate")
else:
    print("Failed to change the baudrate")
    print("Press any key to terminate...")
    getch()
    quit()
    
# Enable Dynamixel#1 Torque
robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
quitt = ''
while 1:
    print("Press any key to continue! (or press ESC to quit!) or q to terminate or f to forward")
    quitt = getch()
    if quitt == chr(0x1b) or quitt=='q' or quitt=='f':
        break
    for obj in dxl :
        obj.moveSync(obj.default, 2,0, move_flag = 0)
        # print('prev: ', obj.prevGoal)
    start = time.time()
    
    # dxl[0].move(100,200)
    # Add Dynamixel goal position value to the Syncwrite parameter storage
    robot.syncWrite()
    indexMoving = 3
    while indexMoving > 2 :
        # print('cek2')
        isMoving = robot.readAll(ADDR_AX_MOVING,1)
        print('isMoving: ', isMoving)
        indexMoving = 0
        for i in isMoving:
            indexMoving = indexMoving + i
    end = time.time()
    print("waktu: ", end - start)

excel_file = "Demo" + '.xlsx'
file = pd.read_excel(excel_file)


while(1):
    print("Pilihan gerakan :")
    print("1. Bow")
    print('2. Push up')
    print('3. Hand stand')
    print('4. Rap chest')
    print('5. Clap')
    print('6. FSL')
    print('7. Telentang')
    print('8. Tengkurap')

    pola = input("Pilih gerakan: ")

    if pola == '1':
        index = 'Bow'
        last = 1
        times = 1.5
        read_flag = 1
    elif pola == '2':
        index = 'Push up'
        last = 3
        times = 0.7
        flag_ind = 3
        read_flag = 1
    elif pola == '3':
        index = 'Hand Stand'
        last = 3
        read_flag = 1
        times = 1
    elif pola == '4':
        index = 'Rap chest'
        last = 3
        flag_ind = 1
        read_flag = 1
        times = 1
    elif pola == '5':
        index = 'Clap'
        last = 1
        read_flag = 1
    elif pola == '6':
        index = 'FSL'
        last = 4
        flag_ind = 0
        times = 0.1
        read_flag = 0
    elif pola == '7':
        index = 'Telentang'
        last = 1
        flag_ind = 0
        times = 1
        read_flag = 1
    elif pola == '8':
        index = 'Tengkurap'
        last = 1
        flag_ind = 0
        times = 1
        read_flag = 1
    loop = 3
    # print('index: ', index)
    print('times: ', times)
    ind_1 = 0
    i = 1
    while i<=last:
        name = index + " " +str(i)
        if i == 2:
            print("repeating...")
            ind_1+=1
            if ind_1 <= flag_ind:
                i-=1
            else:
                ind_1 = 0
            if pola == '4':
                times = 0.3
        if pola == '4' and i == 3:
            times = 1
        # print('i: ', i)
        i+=1
        ind = 0
        # print('name:', name)
        for j in file[name]:
            if pd.notna(j):
                # print('cek')
                goal = int(j)
                dxl[ind].moveSync(goal,times,dxl[ind].prevGoal, read=read_flag)
                ind+=1
                if ind == 18:
                    robot.syncWrite()
                    # print("Moving...")
                    start = time.time()
                    ind = 0
                    while(time.time() - start <= times):
                        elapsed = time.time() - start
                        # print('elapse: ', elapsed)