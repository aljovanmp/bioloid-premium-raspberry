"""
Created on Fri Mar 13 16:32:45 2020

@author: Aljovan Magyarsa P
"""

#!/usr/bin/env python
# -*- coding: utf-8 -*-

################################################################################
# Copyright 2017 ROBOTIS CO., LTD.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

# Author: Ryu Woon Jung (Leon)

#
# *********     Sync Write Example      *********
#
#
# Available Dynamixel model on this example : All models using Protocol 1.0
# This example is tested with two Dynamixel MX-28, and an USB2DYNAMIXEL
# Be sure that Dynamixel MX properties are already set as %% ID : 1 / Baudnum : 34 (Baudrate : 57600)
#

import os
import get_CSV
import time
import numpy as np


if os.name == 'nt':
    import msvcrt
    def getch():
        return msvcrt.getch().decode()
else:
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    def getch():
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

from dynamixel_sdk import *                    # Uses Dynamixel SDK library
import servo
import robot_handler as rh
dxl = []
fwd = []
DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller
                                                # ex) Windows: "COM1"   Linux: "/dev/ttyUSB0" Mac: "/dev/tty.usbserial-*"
# Protocol version
PROTOCOL_VERSION            = 1.0               # See which protocol version is used in the Dynamixel

# Initialize PortHandler instance
# Set the port path
# Get methods and members of PortHandlerLinux or PortHandlerWindows
portHandler = PortHandler(DEVICENAME)


# Initialize PacketHandler instance
# Set the protocol version
# Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
packetHandler = PacketHandler(PROTOCOL_VERSION)

dxl.append(servo.Servo(235,1,portHandler,packetHandler))
dxl.append(servo.Servo(788,2,portHandler,packetHandler))
dxl.append(servo.Servo(279,3,portHandler,packetHandler))
dxl.append(servo.Servo(744,4,portHandler,packetHandler))
dxl.append(servo.Servo(462,5,portHandler,packetHandler))
dxl.append(servo.Servo(561,6,portHandler,packetHandler))
dxl.append(servo.Servo(358,7,portHandler,packetHandler))
dxl.append(servo.Servo(666,8,portHandler,packetHandler))
dxl.append(servo.Servo(507,9,portHandler,packetHandler))
dxl.append(servo.Servo(516,10,portHandler,packetHandler))
dxl.append(servo.Servo(341,11,portHandler,packetHandler))
dxl.append(servo.Servo(682,12,portHandler,packetHandler))
dxl.append(servo.Servo(240,13,portHandler,packetHandler))
dxl.append(servo.Servo(783,14,portHandler,packetHandler))
dxl.append(servo.Servo(647,15,portHandler,packetHandler))
dxl.append(servo.Servo(376,16,portHandler,packetHandler))
dxl.append(servo.Servo(507,17,portHandler,packetHandler))
dxl.append(servo.Servo(516,18,portHandler,packetHandler))

for a in range(1,41):
    fwd.append(rh.Forward(a))


# Control table address
ADDR_AX_TORQUE_ENABLE      = 24            # Control table address is different in Dynamixel model
ADDR_AX_GOAL_POSITION      = 30
ADDR_AX_PRESENT_POSITION   = 36
ADDR_AX_MOVING             = 46

# Data Byte Length
LEN_MX_GOAL_POSITION       = 4
LEN_MX_PRESENT_POSITION    = 2

# Default setting
DXL1_ID                     = 1                 # Dynamixel#1 ID : 1
DXL2_ID                     = 2                 # Dynamixel#1 ID : 2
BAUDRATE                    = 1000000             # Dynamixel default baudrate : 57600

TORQUE_ENABLE               = 1                 # Value for enabling the torque
TORQUE_DISABLE              = 0                 # Value for disabling the torque

# Initialize GroupSyncWrite instance
groupSyncWrite = GroupSyncWrite(portHandler, packetHandler, ADDR_AX_GOAL_POSITION, LEN_MX_GOAL_POSITION)
CoM = []
robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite,fwd, CoM)
CoM.append(rh.CoM_Class('L0_Kanan' ,2  ,[[0.005] ,[0]      ,[0.007] ,[1]],robot,0.036))
CoM.append(rh.CoM_Class('L1_Kanan' ,5  ,[[0.013] ,[-0.016] ,[0]     ,[1]],robot,0.129))
CoM.append(rh.CoM_Class('L2_Kanan' ,6  ,[[-0.013],[0]      ,[-0.001],[1]],robot,0.082))
CoM.append(rh.CoM_Class('L3_Kanan' ,10  ,[[0]     ,[-0.043] ,[0]     ,[1]],robot,0.030))
CoM.append(rh.CoM_Class('L4_Kanan' ,11 ,[[-0.013],[0]      ,[0.016] ,[1]],robot,0.124))
CoM.append(rh.CoM_Class('L5_Kanan' ,14 ,[[0]     ,[-0.0165],[-0.016],[1]],robot,0.015))
CoM.append(rh.CoM_Class('L0_Kiri'  ,16 ,[[0.005] ,[0]      ,[0.007] ,[1]],robot,0.036))
CoM.append(rh.CoM_Class('L1_Kiri'  ,19 ,[[0.013] ,[-0.016] ,[0]     ,[1]],robot,0.129))
CoM.append(rh.CoM_Class('L2_Kiri'  ,20 ,[[-0.013],[0]      ,[-0.001],[1]],robot,0.082))
CoM.append(rh.CoM_Class('L3_Kiri'  ,24 ,[[0]     ,[-0.043] ,[0]     ,[1]],robot,0.030))
CoM.append(rh.CoM_Class('L4_Kiri'  ,25 ,[[-0.013],[0]      ,[0.016] ,[1]],robot,0.124))
CoM.append(rh.CoM_Class('L5_Kiri'  ,28 ,[[0]     ,[-0.017] ,[-0.016],[1]],robot,0.015))
CoM.append(rh.CoM_Class('L6_KIri'  ,33 ,[[-0.004],[-0.012] ,[0]     ,[1]],robot,0.011))
CoM.append(rh.CoM_Class('L7_Kiri'  ,34 ,[[0.044] ,[0]      ,[0.001] ,[1]],robot,0.075))
CoM.append(rh.CoM_Class('L8_Kiri'  ,35 ,[[0.080] ,[0.001]  ,[0]     ,[1]],robot,0.079))
CoM.append(rh.CoM_Class('Badan'    ,37 ,[[-0.015],[-0.036] ,[-0.046],[1]],robot,0.472))
CoM.append(rh.CoM_Class('L6_Kanan' ,38 ,[[-0.004],[-0.012] ,[0]     ,[1]],robot,0.011))
CoM.append(rh.CoM_Class('L7_Kanan' ,39 ,[[0.044] ,[0]      ,[0.001] ,[1]],robot,0.075))
CoM.append(rh.CoM_Class('L8_Kanan' ,40 ,[[0.080] ,[0.001]  ,[0]     ,[1]],robot,0.079))

robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite,fwd, CoM)

import rospy
from std_msgs.msg import String

def talker(msg):
	pub = rospy.Publisher('chatter', String, queue_size=10)
	rospy.init_node('talker', anonymous=True)
	rate = rospy.Rate(10) # 10hz
	while not rospy.is_shutdown():
		
	hello_str = str(msg) + " %s" % rospy.get_time()
	rospy.loginfo(hello_str)
	pub.publish(hello_str)
	rate.sleep()


# Open port
if robot.portHandler.openPort():
    print("Succeeded to open the port")
else:
    print("Failed to open the port")
    print("Press any key to terminate...")
    getch()
    quit()

# Set port baudrate
if robot.portHandler.setBaudRate(BAUDRATE):
    print("Succeeded to change the baudrate")
else:
    print("Failed to change the baudrate")
    print("Press any key to terminate...")
    getch()
    quit()
    
# Enable Dynamixel#1 Torque
robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
quitt = ''
while 1:
    print("Press any key to continue! (or press ESC to quit!) or q to terminate or f to forward")
    quitt = getch()
    if quitt == chr(0x1b) or quitt=='q' or quitt=='f':
        break
    for obj in dxl :
        obj.moveSync(obj.default, 2)
    start = time.time()
    
    # dxl[0].move(100,200)
    # Add Dynamixel goal position value to the Syncwrite parameter storage
    robot.syncWrite()
    indexMoving = 3
    while indexMoving > 2 :
        # print('cek2')
        isMoving = robot.readAll(ADDR_AX_MOVING,1)
        print('isMoving: ', isMoving)
        indexMoving = 0
        for i in isMoving:
            indexMoving = indexMoving + i
    end = time.time()
    print("waktu: ", end - start)
robot.readAllPresentPos()
cek_arah = input("Press anything to continue or y to masuk ke Cek arah: ")
if cek_arah == 'y':
    while(1):
        servo_id = input("Masukkan ID Servo: ")
        sudut    = input("Masukkan Sudut   : ")
        dxl[int(servo_id)-1].move(int(sudut),1,type='degree') 
        break_arah = input("press y to break cek arah")
        if break_arah == 'y':
            break

cek_TM = input("press anything to continue or y to masuk ke Cek TM: ")
if cek_TM == 'y': 
    while(1):
        robot.readAllPresentPos()
        Last_a = input("Masukkan Last axis: ")    
        TM = robot.TransMatGlob(int(Last_a))
        print('====================')
        print('Trans mat to: ', TM['to'])
        Trans_m = np.array(TM['TM'])
        print(Trans_m)
        x = input("Masukkan x : ")
        y = input("Masukkan y : ")
        z = input("Masukkan z : ")
        local = np.array([[int(x)],[int(y)],[int(z)],[1]])
        glob = np.matmul(Trans_m,local)
        print("global: ")
        print(glob)

print("CoM : ",robot.com())
# for i in range(0,2):
    # com = CoM[i].cek()
    # print("i :",i," ",com)
# calibrate = input("Press y to calibrate IMU: ")
# if calibrate== 'y':
    # while(1):
        # if again == 'n'
            # break
        
        # calibrate = input("Press y to calibrate IMU: ")
        
    
# robot.readAllPresentPos()
# dxl[0].move(dxl[0].presentpos - 50, 10)
step = 0
inp  = ''
if quitt!='q':
    print("Press 1 to get telentang pattern or 2 to get tengkurap")
    inp = getch()
    if inp == '1':
        csv_name='tes_telentang.csv'
    if inp == '2':
        csv_name='tes_tengkurap.csv'
    print("Press any key to continue to Pattern! (or press ESC to quit!)")
    inp = getch()
    times = input("Enter moving interval: ")
    times = float(times)
    print("interval: ", times)

# if quitt=='q':
    # Trans_All = robot.updateTransMat()
    # for i in range(0,2):
        # print(Trans_All[i])
    # Trans_1 = robot.TransMatGlob(Trans_All,2)
    # print(Trans_1)
num_step = input("Masukkan Jumlah step: ")
while step < int(num_step):
    if inp == chr(0x1b) or quitt=='q' or quitt=='f':
        break
    try:
		talker(step)
        param,rows = get_CSV.getCSV(csv_name)
        for row in rows:
            # print('id: ',row[0], '\t',"row: ", row[2], '\t', row[4])
            if row[1] == str(step):
                id_joint = int(row[2])
                goal = int(row[4])
                dxl[id_joint-1].moveSync(goal,times)
        print('moving step ', step)
        robot.syncWrite()
        if step != 0 :
            end = time.time()
            print("waktu: %.2f" %(end - start))
        start = time.time()
        
        elapsed = 0
        while (elapsed < times):
            # print('cek')
            elapsed = time.time() - start
            print('elapsed times: %.2f' %elapsed, end = "\r")
        print('elapsed times: %.2f' %elapsed)
        COM = robot.com()
        print()
        print('com: ')
        print(COM)
        print()
        # start_coba = time.time()
        # isMoving = robot.readAll(ADDR_AX_MOVING,1)
        # print('isMoving: ', isMoving)
        # print('waktu read: ', time.time() - start_coba)
        # robot.readAllPresentPos()
        # indexMoving = 0
        # angle_error_flag = 0
        # for obj in dxl:
            # if isMoving[obj.id-1] == 1:
                # angle_error = abs(obj.presentpos - obj.prevGoal)
                # print(obj.id, ' angle error: ', angle_error)
                # if angle_error<6 and angle_error_flag == 0 :
                    # indexMoving = 0
                # else:
                    # indexMoving = 3
                    # angle_error_flag = 1
        # while indexMoving > 2 :
            # print('cek2')
            # isMoving = robot.readAll(ADDR_AX_MOVING,1)
            # print('isMoving: ', isMoving)
            # indexMoving = 0
            # for i in isMoving:
                # indexMoving = indexMoving + i
        step = step + 1
	except rospy.ROSInterruptException:
		pass
    
    
    # print('index: ', indexMoving)
if quitt!='q':
    robot.readAllPresentPos()
    print("Press E to enable or ESC to disable torque or 2 to disable hip")
    inp = getch()
    if inp == 'e':
        robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
    if inp == chr(0x1b):
        robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
    if inp == '2':
        robot.cekServoHip(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
        while(1):
            print("Press E to enable or ESC to out")
            inpu = getch()
            if inpu == 'e':
                robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
                robot.readAllPresentPos()
            else:
                break
robot.portHandler.closePort()
