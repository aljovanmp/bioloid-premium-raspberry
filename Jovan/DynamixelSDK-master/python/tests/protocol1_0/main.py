"""
Created on Fri Mar 13 16:32:45 2020

@author: Aljovan Magyarsa P
"""

#!/usr/bin/env python
# -*- coding: utf-8 -*-

################################################################################
# Copyright 2017 ROBOTIS CO., LTD.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

# Author: Ryu Woon Jung (Leon)

#
# *********     Sync Write Example      *********
#
#
# Available Dynamixel model on this example : All models using Protocol 1.0
# This example is tested with two Dynamixel MX-28, and an USB2DYNAMIXEL
# Be sure that Dynamixel MX properties are already set as %% ID : 1 / Baudnum : 34 (Baudrate : 57600)
#

import os
import get_CSV
import time
import numpy as np
import imu_inMain as imu


if os.name == 'nt':
    import msvcrt
    def getch():
        return msvcrt.getch().decode()
else:
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    def getch():
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

from dynamixel_sdk import *                    # Uses Dynamixel SDK library
import servo
import robot_handler as rh
dxl = []
fwd = []
DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller
                                                # ex) Windows: "COM1"   Linux: "/dev/ttyUSB0" Mac: "/dev/tty.usbserial-*"
# Protocol version
PROTOCOL_VERSION            = 1.0               # See which protocol version is used in the Dynamixel

# Initialize PortHandler instance
# Set the port path
# Get methods and members of PortHandlerLinux or PortHandlerWindows
  
portHandler = PortHandler(DEVICENAME)


# Initialize PacketHandler instance
# Set the protocol version
# Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
packetHandler = PacketHandler(PROTOCOL_VERSION)

dxl.append(servo.Servo(235,1,portHandler,packetHandler))
dxl.append(servo.Servo(788,2,portHandler,packetHandler))
dxl.append(servo.Servo(279,3,portHandler,packetHandler))
dxl.append(servo.Servo(744,4,portHandler,packetHandler))
dxl.append(servo.Servo(462,5,portHandler,packetHandler))
dxl.append(servo.Servo(561,6,portHandler,packetHandler))
dxl.append(servo.Servo(358,7,portHandler,packetHandler))
dxl.append(servo.Servo(666,8,portHandler,packetHandler))
dxl.append(servo.Servo(507,9,portHandler,packetHandler))
dxl.append(servo.Servo(516,10,portHandler,packetHandler))
dxl.append(servo.Servo(341,11,portHandler,packetHandler))
dxl.append(servo.Servo(682,12,portHandler,packetHandler))
dxl.append(servo.Servo(240,13,portHandler,packetHandler))
dxl.append(servo.Servo(783,14,portHandler,packetHandler))
dxl.append(servo.Servo(647,15,portHandler,packetHandler))
dxl.append(servo.Servo(376,16,portHandler,packetHandler))
dxl.append(servo.Servo(507,17,portHandler,packetHandler))
dxl.append(servo.Servo(516,18,portHandler,packetHandler))

for a in range(1,41):
    fwd.append(rh.Forward(a))


# Control table address
ADDR_AX_TORQUE_ENABLE      = 24            # Control table address is different in Dynamixel model
ADDR_AX_GOAL_POSITION      = 30
ADDR_AX_PRESENT_POSITION   = 36
ADDR_AX_MOVING             = 46

# Data Byte Length
LEN_MX_GOAL_POSITION       = 4
LEN_MX_PRESENT_POSITION    = 2

# Default setting
DXL1_ID                     = 1                 # Dynamixel#1 ID : 1
DXL2_ID                     = 2                 # Dynamixel#1 ID : 2
BAUDRATE                    = 1000000          # Dynamixel default baudrate : 57600

TORQUE_ENABLE               = 1                 # Value for enabling the torque
TORQUE_DISABLE              = 0                 # Value for disabling the torque

# Initialize GroupSyncWrite instance
groupSyncWrite = GroupSyncWrite(portHandler, packetHandler, ADDR_AX_GOAL_POSITION, LEN_MX_GOAL_POSITION)
CoM = []
robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite,fwd, CoM)
CoM.append(rh.CoM_Class('L0_Kanan' ,2  ,[[0.005] ,[0]      ,[0.007] ,[1]],robot,0.036))
CoM.append(rh.CoM_Class('L1_Kanan' ,5  ,[[0.013] ,[-0.016] ,[0]     ,[1]],robot,0.129))
CoM.append(rh.CoM_Class('L2_Kanan' ,6  ,[[-0.013],[0]      ,[-0.001],[1]],robot,0.082))
CoM.append(rh.CoM_Class('L3_Kanan' ,10  ,[[0]     ,[-0.043] ,[0]     ,[1]],robot,0.030))
CoM.append(rh.CoM_Class('L4_Kanan' ,11 ,[[-0.013],[0]      ,[0.016] ,[1]],robot,0.124))
CoM.append(rh.CoM_Class('L5_Kanan' ,14 ,[[0]     ,[-0.0165],[-0.016],[1]],robot,0.015))
CoM.append(rh.CoM_Class('L0_Kiri'  ,16 ,[[0.005] ,[0]      ,[0.007] ,[1]],robot,0.036))
CoM.append(rh.CoM_Class('L1_Kiri'  ,19 ,[[0.013] ,[-0.016] ,[0]     ,[1]],robot,0.129))
CoM.append(rh.CoM_Class('L2_Kiri'  ,20 ,[[-0.013],[0]      ,[-0.001],[1]],robot,0.082))
CoM.append(rh.CoM_Class('L3_Kiri'  ,24 ,[[0]     ,[-0.043] ,[0]     ,[1]],robot,0.030))
CoM.append(rh.CoM_Class('L4_Kiri'  ,25 ,[[-0.013],[0]      ,[0.016] ,[1]],robot,0.124))
CoM.append(rh.CoM_Class('L5_Kiri'  ,28 ,[[0]     ,[-0.017] ,[-0.016],[1]],robot,0.015))
CoM.append(rh.CoM_Class('L6_KIri'  ,33 ,[[-0.004],[-0.012] ,[0]     ,[1]],robot,0.011))
CoM.append(rh.CoM_Class('L7_Kiri'  ,34 ,[[0.044] ,[0]      ,[0.001] ,[1]],robot,0.075))
CoM.append(rh.CoM_Class('L8_Kiri'  ,35 ,[[0.080] ,[0.001]  ,[0]     ,[1]],robot,0.079))
CoM.append(rh.CoM_Class('Badan'    ,37 ,[[-0.015],[-0.036] ,[-0.046],[1]],robot,0.472))
CoM.append(rh.CoM_Class('L6_Kanan' ,38 ,[[-0.004],[-0.012] ,[0]     ,[1]],robot,0.011))
CoM.append(rh.CoM_Class('L7_Kanan' ,39 ,[[0.044] ,[0]      ,[0.001] ,[1]],robot,0.075))
CoM.append(rh.CoM_Class('L8_Kanan' ,40 ,[[0.080] ,[0.001]  ,[0]     ,[1]],robot,0.079))

robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite,fwd, CoM)
import csv
data_list = []
data_list_2 = []


# Open port
if robot.portHandler.openPort():
    print("Succeeded to open the port")
else:
    print("Failed to open the port")
    print("Press any key to terminate...")
    getch()
    quit()

# Set port baudrate
if robot.portHandler.setBaudRate(BAUDRATE):
    print("Succeeded to change the baudrate")
else:
    print("Failed to change the baudrate")
    print("Press any key to terminate...")
    getch()
    quit()
    
# Enable Dynamixel#1 Torque
robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
quitt = ''
while 1:
    print("Press any key to continue! (or press ESC to quit!) or q to terminate or f to forward")
    quitt = getch()
    if quitt == chr(0x1b) or quitt=='q' or quitt=='f':
        break
    for obj in dxl :
        obj.moveSync(obj.default, 2,0, move_flag = 0)
        print('prev: ', obj.prevGoal)
    start = time.time()
    
    # dxl[0].move(100,200)
    # Add Dynamixel goal position value to the Syncwrite parameter storage
    robot.syncWrite()
    indexMoving = 3
    while indexMoving > 2 :
        # print('cek2')
        isMoving = robot.readAll(ADDR_AX_MOVING,1)
        print('isMoving: ', isMoving)
        indexMoving = 0
        for i in isMoving:
            indexMoving = indexMoving + i
    end = time.time()
    print("waktu: ", end - start)
robot.readAllPresentPos()
cek_arah = input("Press anything to continue or y to masuk ke Cek arah: ")
if cek_arah == 'y':
    while(1):
        servo_id = input("Masukkan ID Servo: ")
        sudut    = input("Masukkan Sudut   : ")
        dxl[int(servo_id)-1].move(int(sudut),1,type='degree') 
        break_arah = input("press y to break cek arah")
        if break_arah == 'y':
            break

cek_TM = input("press anything to continue or y to masuk ke Cek TM: ")
if cek_TM == 'y': 
    while(1):
        robot.readAllPresentPos()
        Last_a = input("Masukkan Last axis: ")    
        TM = robot.TransMatGlob(int(Last_a))
        print('====================')
        print('Trans mat to: ', TM['to'])
        Trans_m = np.array(TM['TM'])
        print(Trans_m)
        x = input("Masukkan x : ")
        y = input("Masukkan y : ")
        z = input("Masukkan z : ")
        local = np.array([[int(x)],[int(y)],[int(z)],[1]])
        glob = np.matmul(Trans_m,local)
        print("global: ")
        print(glob)
COM = robot.com()
print("CoM : ",COM)
X,Y,Z = 0,0,0
for ex in COM['x']:
    X = ex
for ye in COM['y']:
    Y = ye
for zet in COM['z']:
    Z = zet
# for i in range(0,2):
    # com = CoM[i].cek()
    # print("i :",i," ",com)
calibrate = input("Press y to calibrate IMU: ")
Komp = 0
if calibrate== 'y':
    timer = time.time()
    while(1):
        dt = time.time() - timer
        timer = time.time()
        Komp = imu.IMU(dt,0,0,0)
        print("Komp: ", Komp)
        again = input("Press y to calibrate IMU: ")
        if again == 'n':
            break
cek_arah_2 = input("Press anything to continue or y to masuk ke cek IMU: ")
if cek_arah_2 == 'y':
    servo_id = []
    sudut= []
    while(1):
        againnn = input("Press y to input another joint: ")
        if againnn == 'y':
            servo_id.append(input("Masukkan ID Servo: "))
            sudut.append(input("Masukkan Sudut   : "))
        else:
            ind = 0
            for i in servo_id:
                dxl[int(i)-1].move(int(sudut[ind]),1,type='degree')
                ind+=1
            timer = time.time()
            indec = 0
            while(indec <= 200):
                dt = time.time() - timer
                angle_Imu = imu.IMU(dt, 11, Komp[0], Komp[1])
                timer = time.time()
                print("Pitch: ", angle_Imu['Pitch'])
                indec+=1
                time.sleep(0.1)
            
            break_arah = input("press y to break cek IMU or n to cek again")
            if break_arah == 'y':
                break
        
    
# robot.readAllPresentPos()
# dxl[0].move(dxl[0].presentpos - 50, 10)
step = 0
inp  = ''
title =''
times = 0
periode = ''
number = ''
while(1):
    if quitt!='q':
        print("Press 1 to get telentang pattern or 2 to get tengkurap")
        inp = getch()
        if inp == '1':
            csv_name='tes_telentang.csv'
            title = 'telentang'
        if inp == '2':
            csv_name='tes_tengkurap.csv'
            title = 'tengkurap'
        print("Press any key to continue to Pattern! (or press ESC to quit!)")
        inp = getch()
        times = input("Enter moving interval: ")
        times = float(times)
        print("interval: ", times)
        periode = str(times)

# if quitt=='q':
    # Trans_All = robot.updateTransMat()
    # for i in range(0,2):
        # print(Trans_All[i])
    # Trans_1 = robot.TransMatGlob(Trans_All,2)
    # print(Trans_1)

    
    num_step = input("Masukkan Jumlah step: ")
    
    while step < int(num_step):
        if inp == chr(0x1b) or quitt=='q' or quitt=='f':
            break
        param,rows = get_CSV.getCSV(csv_name)
        for row in rows:
            # print('id: ',row[0], '\t',"row: ", row[2], '\t', row[4])
            if row[1] == str(step):
                id_joint = int(row[2])
                goal = int(row[4])
                dxl[id_joint-1].moveSync(goal,times,dxl[id_joint-1].prevGoal, read=1)
        print('moving step ', step)
        robot.syncWrite()
        if step != 0 :
            end = time.time()
            print("waktu: %.2f" %(end - start))
        start = time.time()
        timer = time.time()
        elapsed = 0
        timerCoM = 0
        
        COM = robot.com(read_flag=0, Time_read=elapsed)
        for ex in COM['x']:
            X = ex
        for ye in COM['y']:
            Y = ye
        for zet in COM['z']:
            Z = zet
        
        angle_Imu = imu.IMU(dt, 11, Komp[0], Komp[1])
        detail_step = str(step + round(elapsed/times,2))
        data = [times,round(elapsed,2),detail_step,str(angle_Imu['Pitch']),str(X), str(Y),str(Z),str(dxl[15].speed*0.111), \
        str(dxl[14].speed*0.111), str(dxl[15].presentpos), str(dxl[14].presentpos), title]
        data_list.append(data)
        while (elapsed < times):
            # print('cek')
            elapsed = time.time() - start
            dt = time.time() - timer
            dtCoM = time.time() - timerCoM
            print('elapsed times: %.2f' %elapsed, ' elapsed dt   : %.2f' %dt, end = '\r')
            # if dtCoM>= 0.3 and times-elapsed >= 0.3 and times >= 1:
                # timerCoM = time.time()
                # COM = robot.com()
                # print()
                # print('com: ')
                # print(COM)
                # print()
                # for ex in COM['x']:
                    # X = ex
                # for ye in COM['y']:
                    # Y = ye
                # for zet in COM['z']:
                    # Z = zet
            # if dt >= 0.1 and times-elapsed >= 0.1 and times <= 0.3:
                # COM = robot.com()
                # print()
                ## print('com: ')
                ## print(COM)
                ## print()
                # for ex in COM['x']:
                    # X = ex
                # for ye in COM['y']:
                    # Y = ye
                # for zet in COM['z']:
                    # Z = zet
             # and times-elapsed >= 0.1
            if dt >= 0.1:
                COM = robot.com(read_flag=0, Time_read=elapsed)
                for ex in COM['x']:
                    X = ex
                for ye in COM['y']:
                    Y = ye
                for zet in COM['z']:
                    Z = zet
                
                angle_Imu = imu.IMU(dt, 11, Komp[0], Komp[1])
                timer = time.time()
                detail_step = str(step + round(elapsed/times,2))
                data = [times,round(elapsed,2),detail_step,str(angle_Imu['Pitch']),str(X), str(Y),str(Z),str(dxl[15].speed*0.111), \
                        str(dxl[14].speed*0.111), str(dxl[15].posNow), str(dxl[14].posNow), title]
                data_list.append(data)
            
                
        print('elapsed times: %.2f' %elapsed)

        # start_coba = time.time()
        # isMoving = robot.readAll(ADDR_AX_MOVING,1)
        # print('isMoving: ', isMoving)
        # print('waktu read: ', time.time() - start_coba)
        # robot.readAllPresentPos()
        # indexMoving = 0
        # angle_error_flag = 0
        # for obj in dxl:
            # if isMoving[obj.id-1] == 1:
                # angle_error = abs(obj.presentpos - obj.prevGoal)
                # print(obj.id, ' angle error: ', angle_error)
                # if angle_error<6 and angle_error_flag == 0 :
                    # indexMoving = 0
                # else:
                    # indexMoving = 3
                    # angle_error_flag = 1
        # while indexMoving > 2 :
            # print('cek2')
            # isMoving = robot.readAll(ADDR_AX_MOVING,1)
            # print('isMoving: ', isMoving)
            # indexMoving = 0
            # for i in isMoving:
                # indexMoving = indexMoving + i
        step = step + 1
        # print('index: ', indexMoving)
    input_confirm = input("Press y to input data: ")
    if input_confirm =='y':
        new = input("File Baru (y/n): ")
        if new == 'y':
            number = input("Enter data name: ")
            print("saving to file ", number, "...")
            name = 'Data' + number + '.csv'
            Title_wrapper = ['Pola: ' +  title] 
            with open(name, mode='a') as file:
                Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                # Data.writerow(["    "])
                # Data.writerow(Title_wrapper)
                Data.writerow(['Periode(s)','Elapsed', 'Step', 'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                'Left Ankle Speed(rpm)','Right Ankle Speed(rpm)', 'Left Ankle Angle', 'Right Ankle Angle', 'Pola'])
                Data.writerows(data_list)
        else:
            print("saving to file ", number, "...")
            name = 'Data' + number + '.csv'
            Title_wrapper = ['Pola: ' +  title] 
            with open(name, mode='a') as file:
                Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                # Data.writerow(["    "])
                # Data.writerow(Title_wrapper)
                # Data.writerow(['Periode(s)','Elapsed', 'Step', 'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                # 'Left Ankle Speed(rpm)','Right Ankle Speed(rpm)', 'Left Ankle Angle', 'Right Ankle Angle', 'Pola'])
                Data.writerows(data_list)
        
    againn = input("Masukkan y untuk merecord ulang: ")
    if againn != 'y':
        break
        
    if againn == 'y':
        step = 0
        data_list= []
        X = 0
        Y = 0
        Z = 0
    
if quitt!='q':
    robot.readAllPresentPos()
    print("Press E to enable or ESC to disable torque or 2 to disable hip")
    inp = getch()
    if inp == 'e':
        robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
    if inp == chr(0x1b):
        robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
    if inp == '2':
        robot.cekServoHip(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
        while(1):
            print("Press E to enable or ESC to out")
            inpu = getch()
            if inpu == 'e':
                robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
                robot.readAllPresentPos()
            else:
                break
robot.portHandler.closePort()
