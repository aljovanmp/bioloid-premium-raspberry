"""
Created on Fri Mar 13 16:32:45 2020

@author: Aljovan Magyarsa P
"""

#!/usr/bin/env python
# -*- coding: utf-8 -*-

################################################################################
# Copyright 2017 ROBOTIS CO., LTD.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

# Author: Ryu Woon Jung (Leon)

#
# *********     Sync Write Example      *********
#
#
# Available Dynamixel model on this example : All models using Protocol 1.0
# This example is tested with two Dynamixel MX-28, and an USB2DYNAMIXEL
# Be sure that Dynamixel MX properties are already set as %% ID : 1 / Baudnum : 34 (Baudrate : 57600)
#

import os
import get_CSV
import time
import numpy as np
import imu_inMain as imu
import torch.nn as nn
from torch.utils.data import TensorDataset, DataLoader
import torch.nn.functional as F
import torch
from Kalman import KalmanAngle

if os.name == 'nt':
    import msvcrt
    def getch():
        return msvcrt.getch().decode()
else:
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    def getch():
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

from dynamixel_sdk import *                    # Uses Dynamixel SDK library
import servo
import robot_handler as rh
dxl = []
fwd = []
DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller
                                                # ex) Windows: "COM1"   Linux: "/dev/ttyUSB0" Mac: "/dev/tty.usbserial-*"
# Protocol version
PROTOCOL_VERSION            = 1.0               # See which protocol version is used in the Dynamixel

# Initialize PortHandler instance
# Set the port path
# Get methods and members of PortHandlerLinux or PortHandlerWindows
  
portHandler = PortHandler(DEVICENAME)


# Initialize PacketHandler instance
# Set the protocol version
# Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
packetHandler = PacketHandler(PROTOCOL_VERSION)

dxl.append(servo.Servo(235,1,portHandler,packetHandler))
dxl.append(servo.Servo(788,2,portHandler,packetHandler))
dxl.append(servo.Servo(279,3,portHandler,packetHandler))
dxl.append(servo.Servo(744,4,portHandler,packetHandler))
dxl.append(servo.Servo(462,5,portHandler,packetHandler))
dxl.append(servo.Servo(561,6,portHandler,packetHandler))
dxl.append(servo.Servo(358,7,portHandler,packetHandler))
dxl.append(servo.Servo(666,8,portHandler,packetHandler))
dxl.append(servo.Servo(507,9,portHandler,packetHandler))
dxl.append(servo.Servo(516,10,portHandler,packetHandler))
dxl.append(servo.Servo(341,11,portHandler,packetHandler))
dxl.append(servo.Servo(682,12,portHandler,packetHandler))
dxl.append(servo.Servo(240,13,portHandler,packetHandler))
dxl.append(servo.Servo(783,14,portHandler,packetHandler))
dxl.append(servo.Servo(647,15,portHandler,packetHandler))
dxl.append(servo.Servo(376,16,portHandler,packetHandler))
dxl.append(servo.Servo(507,17,portHandler,packetHandler))
dxl.append(servo.Servo(516,18,portHandler,packetHandler))

for a in range(1,41):
    fwd.append(rh.Forward(a))


# Control table address
ADDR_AX_TORQUE_ENABLE      = 24            # Control table address is different in Dynamixel model
ADDR_AX_GOAL_POSITION      = 30
ADDR_AX_PRESENT_POSITION   = 36
ADDR_AX_MOVING             = 46

# Data Byte Length
LEN_MX_GOAL_POSITION       = 4
LEN_MX_PRESENT_POSITION    = 2

# Default setting
DXL1_ID                     = 1                 # Dynamixel#1 ID : 1
DXL2_ID                     = 2                 # Dynamixel#1 ID : 2
BAUDRATE                    = 1000000          # Dynamixel default baudrate : 57600

TORQUE_ENABLE               = 1                 # Value for enabling the torque
TORQUE_DISABLE              = 0                 # Value for disabling the torque

# Initialize GroupSyncWrite instance
groupSyncWrite = GroupSyncWrite(portHandler, packetHandler, ADDR_AX_GOAL_POSITION, LEN_MX_GOAL_POSITION)
CoM = []
robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite,fwd, CoM)
CoM.append(rh.CoM_Class('L0_Kanan' ,2  ,[[0.005] ,[0]      ,[0.007] ,[1]],robot,0.036))
CoM.append(rh.CoM_Class('L1_Kanan' ,5  ,[[0.013] ,[-0.016] ,[0]     ,[1]],robot,0.129))
CoM.append(rh.CoM_Class('L2_Kanan' ,6  ,[[-0.013],[0]      ,[-0.001],[1]],robot,0.082))
CoM.append(rh.CoM_Class('L3_Kanan' ,10  ,[[0]     ,[-0.043] ,[0]     ,[1]],robot,0.030))
CoM.append(rh.CoM_Class('L4_Kanan' ,11 ,[[-0.013],[0]      ,[0.016] ,[1]],robot,0.124))
CoM.append(rh.CoM_Class('L5_Kanan' ,14 ,[[0]     ,[-0.0165],[-0.016],[1]],robot,0.015))
CoM.append(rh.CoM_Class('L0_Kiri'  ,16 ,[[0.005] ,[0]      ,[0.007] ,[1]],robot,0.036))
CoM.append(rh.CoM_Class('L1_Kiri'  ,19 ,[[0.013] ,[-0.016] ,[0]     ,[1]],robot,0.129))
CoM.append(rh.CoM_Class('L2_Kiri'  ,20 ,[[-0.013],[0]      ,[-0.001],[1]],robot,0.082))
CoM.append(rh.CoM_Class('L3_Kiri'  ,24 ,[[0]     ,[-0.043] ,[0]     ,[1]],robot,0.030))
CoM.append(rh.CoM_Class('L4_Kiri'  ,25 ,[[-0.013],[0]      ,[0.016] ,[1]],robot,0.124))
CoM.append(rh.CoM_Class('L5_Kiri'  ,28 ,[[0]     ,[-0.017] ,[-0.016],[1]],robot,0.015))
CoM.append(rh.CoM_Class('L6_KIri'  ,33 ,[[-0.004],[-0.012] ,[0]     ,[1]],robot,0.011))
CoM.append(rh.CoM_Class('L7_Kiri'  ,34 ,[[0.044] ,[0]      ,[0.001] ,[1]],robot,0.075))
CoM.append(rh.CoM_Class('L8_Kiri'  ,35 ,[[0.080] ,[0.001]  ,[0]     ,[1]],robot,0.079))
CoM.append(rh.CoM_Class('Badan'    ,37 ,[[-0.015],[-0.036] ,[-0.046],[1]],robot,0.472))
CoM.append(rh.CoM_Class('L6_Kanan' ,38 ,[[-0.004],[-0.012] ,[0]     ,[1]],robot,0.011))
CoM.append(rh.CoM_Class('L7_Kanan' ,39 ,[[0.044] ,[0]      ,[0.001] ,[1]],robot,0.075))
CoM.append(rh.CoM_Class('L8_Kanan' ,40 ,[[0.080] ,[0.001]  ,[0]     ,[1]],robot,0.079))

robot = rh.Robot(dxl,portHandler,packetHandler,groupSyncWrite,fwd, CoM)
import csv
data_list = []
data_list_2 = []

KalmanX = KalmanAngle()
KalmanY = KalmanAngle()

# Open port
if robot.portHandler.openPort():
    print("Succeeded to open the port")
else:
    print("Failed to open the port")
    print("Press any key to terminate...")
    getch()
    quit()

# Set port baudrate
if robot.portHandler.setBaudRate(BAUDRATE):
    print("Succeeded to change the baudrate")
else:
    print("Failed to change the baudrate")
    print("Press any key to terminate...")
    getch()
    quit()
    
# Enable Dynamixel#1 Torque
robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
quitt = ''
while 1:
    print("Press any key to continue! (or press ESC to quit!) or q to terminate or f to forward")
    quitt = getch()
    if quitt == chr(0x1b) or quitt=='q' or quitt=='f':
        break
    for obj in dxl :
        obj.moveSync(obj.default, 2,0, move_flag = 0)
        print('prev: ', obj.prevGoal)
    start = time.time()
    
    # dxl[0].move(100,200)
    # Add Dynamixel goal position value to the Syncwrite parameter storage
    robot.syncWrite()
    indexMoving = 3
    while indexMoving > 2 :
        # print('cek2')
        isMoving = robot.readAll(ADDR_AX_MOVING,1)
        print('isMoving: ', isMoving)
        indexMoving = 0
        for i in isMoving:
            indexMoving = indexMoving + i
    end = time.time()
    print("waktu: ", end - start)
robot.readAllPresentPos()
cek_arah = input("Press anything to continue or y to masuk ke Cek arah: ")
if cek_arah == 'y':
    while(1):
        servo_id = input("Masukkan ID Servo: ")
        sudut    = input("Masukkan Sudut   : ")
        dxl[int(servo_id)-1].move(int(sudut),1,type='degree') 
        break_arah = input("press y to break cek arah")
        if break_arah == 'y':
            break

cek_TM = input("press anything to continue or y to masuk ke Cek TM: ")
if cek_TM == 'y': 
    while(1):
        robot.readAllPresentPos()
        Last_a = input("Masukkan Last axis: ")    
        TM = robot.TransMatGlob(int(Last_a))
        print('====================')
        print('Trans mat to: ', TM['to'])
        Trans_m = np.array(TM['TM'])
        print(Trans_m)
        x = input("Masukkan x : ")
        y = input("Masukkan y : ")
        z = input("Masukkan z : ")
        local = np.array([[int(x)],[int(y)],[int(z)],[1]])
        glob = np.matmul(Trans_m,local)
        print("global: ")
        print(glob)
COM = robot.com()
print("CoM : ",COM)
X,Y,Z = 0,0,0
for ex in COM['x']:
    X = ex
for ye in COM['y']:
    Y = ye
for zet in COM['z']:
    Z = zet
# for i in range(0,2):
    # com = CoM[i].cek()
    # print("i :",i," ",com)

cek_arah_2 = input("Press anything to continue or y to masuk ke cek IMU: ")
if cek_arah_2 == 'y':
    servo_id = []
    sudut= []
    while(1):
        againnn = input("Press y to input another joint: ")
        if againnn == 'y':
            servo_id.append(input("Masukkan ID Servo: "))
            sudut.append(input("Masukkan Sudut   : "))
        else:
            ind = 0
            for i in servo_id:
                dxl[int(i)-1].move(int(sudut[ind]),1,type='degree')
                ind+=1
            timer = time.time()
            indec = 0
            while(indec <= 200):
                dt = time.time() - timer
                angle_Imu = imu.IMU(dt, 11, Komp[0], Komp[1], KalmanX, KalmanY, zero_gyro = Komp[2])
                timer = time.time()
                print("Pitch: ", angle_Imu['Pitch'])
                indec+=1
                time.sleep(0.1)
            
            break_arah = input("press y to break cek IMU or n to cek again")
            if break_arah == 'y':
                break
        
    
# robot.readAllPresentPos()
# dxl[0].move(dxl[0].presentpos - 50, 10)
step = 0
inp  = ''
tittle =''
times = 0
periode = ''

number = input("Masukkan file: ")
PATH = 'mode' + number +'.pth'
hid = int(input("Masukkan Jumlah Hidden Node: "))

model = torch.nn.Sequential(
        torch.nn.Linear(7, hid),
        torch.nn.LeakyReLU(),
        # torch.nn.Linear(hid, int(4)),
        
        torch.nn.Linear(hid, int(100)),
        torch.nn.LeakyReLU(),
        # torch.nn.Linear(100, int(100)),
        # torch.nn.LeakyReLU(),
        torch.nn.Linear(int(100), 2),
        
    )
    
model.load_state_dict(torch.load(PATH))
data_input = []
data_target = []
state_fall = 0
inp = ''
while(1):
    if quitt!='q':
        print("Press 1 to get telentang pattern or 2 to get tengkurap")
        inp_pattern = getch()
        if inp_pattern == '1':
            csv_name='tes_telentang.csv'
            title = 'telentang'
        if inp_pattern == '2':
            csv_name='tes_tengkurap.csv'
            title = 'tengkurap'
        print("Press any key to continue to Pattern! (or press ESC to quit!)")
        inp = getch()
        times = input("Enter moving interval: ")
        times = float(times)
        print("interval: ", times)
        periode = str(times)

# if quitt=='q':
    # Trans_All = robot.updateTransMat()
    # for i in range(0,2):
        # print(Trans_All[i])
    # Trans_1 = robot.TransMatGlob(Trans_All,2)
    # print(Trans_1)
    
    
    
    num_step = input("Masukkan Jumlah step: ")
    using_model = input("Using model?: ")
    empirical = []
    
    while(1):
        emp_again = input("Masukkan y untuk menambah sudut: ")
        if emp_again == 'y':
            step_e  = int(input("Masukkan step yg ingin diubah: "))
            angle_e = int(input("Masukkan sudut yg ingin diubah: "))
            speed_e = int(input("Masukkan speed yg ingin diubah: "))
            empirical.append({'step':step_e, 'angle':angle_e, 'speed':speed_e})
        else:
            break
            
    calibrate = input("Press y to calibrate IMU: ")
    Komp = 0
    if calibrate== 'y':
        timer = time.time()
        while(1):
            dt = time.time() - timer
            timer = time.time()
            Komp = imu.IMU(dt,0,0,0, KalmanX, KalmanY)
            print("Komp: ", Komp)
            again = input("Press y to calibrate IMU: ")
            if again == 'n':
                break
    data2 = []
    data2_save= []
    for obj in dxl :
        obj.moveSync(obj.default, 2,0, move_flag = 0)
        # print('prev: ', obj.prevGoal)
    robot.syncWrite()
    time.sleep(3)
    times_M,detail_step_M,pitch,CMx,CMy,CMz,Ankle_angle = 0,0,0,0,0,0,0
    angle_Imu = 0
    while step < int(num_step):
        if inp == chr(0x1b) or quitt=='q' or quitt=='f':
            break
        param,rows = get_CSV.getCSV(csv_name)
        for row in rows:
            # print('id: ',row[0], '\t',"row: ", row[2], '\t', row[4])
            if row[1] == str(step):
                id_joint = int(row[2])
                goal = int(row[4])
                # if id_joint!=16 and id_joint!=15:
                dxl[id_joint-1].moveSync(goal,times,dxl[id_joint-1].prevGoal, read=1)
        print()
        print('moving step ', step)
        timer = time.time()
        timerIMU = time.time() - 0.005
        while time.time() - timer <= 0.1:
            dt_IMU = time.time()-timerIMU
            if dt_IMU >= 0.005:
                dt = timerIMU-time.time()
                angle_Imu = imu.IMU(dt, 11, Komp[0], Komp[1], KalmanX, KalmanY, zero_gyro = Komp[2])
                timerIMU = time.time()
        
        print('Pitch : ', angle_Imu['Pitch'])
        if using_model != 'y':
            robot.syncWrite()
        
        if step != 0 :
            end = time.time()
            print("waktu: %.2f" %(end - start))
        start = time.time()
        timer = time.time()
        elapsed = 0
        timerCoM = 0
        
        COM = robot.com(read_flag=1, Time_read=elapsed)
        for ex in COM['x']:
            X = ex
        for ye in COM['y']:
            Y = ye
        for zet in COM['z']:
            Z = zet
        
        # angle_Imu = imu.IMU(dt, 11, Komp[0], Komp[1], KalmanX, KalmanY, zero_gyro = Komp[2])
        detail_step = step + round(elapsed/times,2)
        # print('before model presentpos: ', dxl[15].presentpos)
        # print('posNow: ', dxl[15].posNow)
        
        if inp_pattern=='2':    
            times_M = ((times - 0) / 5) * 100
            detail_step_M = ((detail_step - 0) / 5) * 100
            pitch = ((angle_Imu['Pitch']-35.993)/(108.866-35.993)) * 100
            CMx = ((X+0.039)/(0.049+0.039)) * 100
            CMy = ((Y+0.018)/(0.018-0.009)) * 100
            CMz = ((Z+0.114)/(0.172+0.114)) * 100
            Ankle_angle = ((dxl[15].presentpos-279)/(551-279)) * 100
        if inp_pattern=='1':
            print("Masuk telentang")
            times_M = ((times - 0) / 5) * 100
            detail_step_M = ((detail_step - 0) / 5) * 100
            pitch = ((angle_Imu['Pitch']+118.69)/(118.69-5.36)) * 100
            CMx = ((X+0.082)/(0.025+0.082)) * 100
            CMy = ((Y+0.011)/(0.011-0.001)) * 100
            CMz = ((Z-0.082)/(0.175+0.082)) * 100
            Ankle_angle = ((dxl[15].presentpos-239)/(506-239)) * 100
        data_input = np.array([times_M,detail_step_M,pitch, CMx, CMy, CMz, \
        Ankle_angle], dtype='float32')
        #ankle speed xmin 0.555 xmax 17.316
        #goal xmin 288 xmax 550
        # data = [times,str(detail_step),str(angle_Imu['Pitch']),str(X), str(Y),str(Z), \
        # str(dxl[15].presentpos)]
        
        
        inputs = torch.from_numpy(data_input)
        # print('before: ', data)
        # print('input: ',inputs)
        before = time.time()
        pred = model(inputs)
        pred_n = pred.detach().numpy()
        # print('Model time: ', time.time() - before)
        predict = []
        for i in pred_n: 
            predict.append(i)
            # print('pred: ', i)
        
        if inp_pattern=='2':
            predict_speed = round((((predict[0]/100)*(17.316-0.555)) + 0.555),3)
            predict_goal = round((((predict[1]/100)*(550-288)) + 288),3)
        if inp_pattern=='1':
            predict_speed = round((((predict[0]/100)*(30.081-0.666)) + 0.666),3)
            predict_goal = round((((predict[1]/100)*(509-237)) + 237),3)
            dif = 0
            if step == 2:
                if predict_goal>239:
                    dif = predict_goal - 239
                    predict_goal = 239
            # if empirical_step != 0:
            for i in empirical:
                if (step == i['step']):
                    print('speed_ori: ', predict_speed)
                    print('goal_ori : ', predict_goal)
                    predict_goal += i['angle']
                    predict_speed += i['speed']
                
                
            
        if using_model == 'y':
            data2 = ([times_M,str(detail_step_M),str(pitch),str(CMx), str(CMy),str(CMz), \
            str(Ankle_angle), str(predict[0]), str(predict[1])])
            # print('posNow: ', dxl[15].posNow)
            data = [times,detail_step,str(angle_Imu['Pitch']),str(X), str(Y),str(Z),str(dxl[15].presentpos),str(predict_speed), \
                            predict_goal, title]
            data_list.append(data)
            data2_save.append(data2)
            data2 = []
            
            print('pattern speed: ', dxl[15].speed*0.111)
            print('pattern goal : ', dxl[15].prevGoal)
            print('speed: ', predict_speed)
            print('goal : ', predict_goal)
            
            dif_predict = predict_goal - dxl[15].default
            dxl[15].moveSync(int(predict_goal),predict_speed,dxl[15].presentpos, time_type='rpm')
            dxl[14].moveSync(int(dxl[14].default-dif_predict),predict_speed,dxl[14].presentpos, time_type='rpm')
            robot.syncWrite()
            
        else:
            # print("saving data to not using model....")
            goal,speedd = 0,0
            # print('speed: ', dxl[15].speed)
            
            if inp_pattern=='2':
                goal = round((((dxl[15].prevGoal-288)/(550-288)) * 100),3)
                speedd = round(((((dxl[15].speed*0.111)-0.555)/(17.316-0.555)) * 100),3)
            if inp_pattern=='1':
                goal = round((((dxl[15].prevGoal-237)/(509-237)) * 100),3)
                speedd = round(((((dxl[15].speed*0.111)-0.666)/(30.081-0.666)) * 100),3)
            # print('speed in data: ', speedd)
            # goal = round((((dxl[15].prevGoal-288)/(550-228)) * 100),3)
            # speedd = round(((((dxl[15].speed*0.111)-0.555)/(17.316-0.555)) * 100),3)
            data2 = ([times_M,str(detail_step_M),str(pitch),str(CMx), str(CMy),str(CMz), \
            str(Ankle_angle), str(speedd),str(goal)])
            # print('posNow: ', dxl[15].posNow)
            data = [times,detail_step,str(angle_Imu['Pitch']),str(X), str(Y),str(Z),str(dxl[15].presentpos), \
                            str(dxl[15].speed*0.111), str(dxl[15].prevGoal), title]
            data_list.append(data)
            data2_save.append(data2)
            data2 = []
        start = time.time()
        timer = time.time()
        elapsed = 0
        timerCoM = time.time()
        timerIMU = time.time()
        
        while (elapsed < times):
            # print('cek')
            elapsed = time.time() - start
            dt = time.time() - timer
            dtCoM = time.time() - timerCoM
            dt_IMU = time.time() - timerIMU - 0.005
            # print('elapsed times: %.2f' %elapsed, ' elapsed dt   : %.2f' %dt, end = '\r')
            # if dtCoM>= 0.3 and times-elapsed >= 0.3 and times >= 1:
                # timerCoM = time.time()
                # COM = robot.com()
                # print()
                # print('com: ')
                # print(COM)
                # print()
                # for ex in COM['x']:
                    # X = ex
                # for ye in COM['y']:
                    # Y = ye
                # for zet in COM['z']:
                    # Z = zet
            # if dt >= 0.1 and times-elapsed >= 0.1 and times <= 0.3:
                # COM = robot.com()
                # print()
                ## print('com: ')
                ## print(COM)
                ## print()
                # for ex in COM['x']:
                    # X = ex
                # for ye in COM['y']:
                    # Y = ye
                # for zet in COM['z']:
                    # Z = zet
             # and times-elapsed >= 0.1
            if dt_IMU >= 0.005:
                angle_Imu = imu.IMU(dt_IMU, 11, Komp[0], Komp[1], KalmanX, KalmanY, zero_gyro = Komp[2])
                timerIMU = time.time()
            if dt >= 0.1:
                COM = robot.com(read_flag=0, Time_read=elapsed)
                for ex in COM['x']:
                    X = ex
                for ye in COM['y']:
                    Y = ye
                for zet in COM['z']:
                    Z = zet
                
                # angle_Imu = imu.IMU(0.005, 11, Komp[0], Komp[1], KalmanX, KalmanY, zero_gyro = Komp[2])
                timer = time.time()
                detail_step = str(step + round(elapsed/times,2))
                data = [times,detail_step,str(angle_Imu['Pitch']),str(X), str(Y),str(Z),str(dxl[15].posNow),str(dxl[15].speed*0.111), \
                        str(dxl[15].prevGoal), title]
                data_list.append(data)
            
                
        print('elapsed times: %.2f' %elapsed)

        # start_coba = time.time()
        # isMoving = robot.readAll(ADDR_AX_MOVING,1)
        # print('isMoving: ', isMoving)
        # print('waktu read: ', time.time() - start_coba)
        # robot.readAllPresentPos()
        # indexMoving = 0
        # angle_error_flag = 0
        # for obj in dxl:
            # if isMoving[obj.id-1] == 1:
                # angle_error = abs(obj.presentpos - obj.prevGoal)
                # print(obj.id, ' angle error: ', angle_error)
                # if angle_error<6 and angle_error_flag == 0 :
                    # indexMoving = 0
                # else:
                    # indexMoving = 3
                    # angle_error_flag = 1
        # while indexMoving > 2 :
            # print('cek2')
            # isMoving = robot.readAll(ADDR_AX_MOVING,1)
            # print('isMoving: ', isMoving)
            # indexMoving = 0
            # for i in isMoving:
                # indexMoving = indexMoving + i
        step = step + 1
        # print('index: ', indexMoving)
    input_confirm = input("Press y to input data: ")
    if input_confirm =='y':
        new = input("File Baru (y/n): ")
        fall = input("Robot Fall?: ")
        if new == 'y':
            if fall == 'y':
                number = input("Enter data name: ")
                name = 'Data' + number + '_Fall'+ '.csv'
                print("saving to file: ", name)
                Title_wrapper = ['Pola: ' +  title] 
                state_fall = 1
                with open(name, mode='a') as file:
                    Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    # Data.writerow(["    "])
                    # Data.writerow(Title_wrapper)
                    Data.writerow(['Periode(s)', 'Step', 'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                    'Left Ankle Angle Now','Left Ankle Speed(rpm)', 'Goal', 'Pola'])
                    Data.writerows(data_list)
                    Data.writerow(["    "])
                    Data.writerows(data2_save)
                    Data.writerow(["    "])
            else:
                number = input("Enter data name: ")
                name = 'Data' + number + '.csv'
                print("saving to file: ", name)
                Title_wrapper = ['Pola: ' +  title] 
                with open(name, mode='a') as file:
                    Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    # Data.writerow(["    "])
                    # Data.writerow(Title_wrapper)
                    Data.writerow(['Periode(s)', 'Step', 'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                    'Left Ankle Angle Now','Left Ankle Speed(rpm)', 'Goal', 'Pola'])
                    Data.writerows(data_list)
                    Data.writerow(["    "])
                    Data.writerows(data2_save)
                    Data.writerow(["    "])
                
            
        else:
            if fall == 'y':
                name = 'Data' + number + '_Fall' + '.csv'
                print("saving to file: ", name)
                Title_wrapper = ['Pola: ' +  title] 
                if state_fall == 1:
                    with open(name, mode='a') as file:
                        Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        # Data.writerow(["    "])
                        # Data.writerow(Title_wrapper)
                        Data.writerows(data_list)
                        Data.writerow(["    "])
                        Data.writerows(data2_save)
                        Data.writerow(["    "])
                else:
                    with open(name, mode='a') as file:
                        Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        # Data.writerow(["    "])
                        # Data.writerow(Title_wrapper)
                        Data.writerow(['Periode(s)', 'Step', 'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                    'Left Ankle Angle Now','Left Ankle Speed(rpm)', 'Goal', 'Pola'])
                        Data.writerows(data_list)
                        Data.writerow(["    "])
                        Data.writerows(data2_save)
                        Data.writerow(["    "])
                    state_fall = 1
            else:
                name = 'Data' + number + '.csv'
                print("saving to file: ", name)
                Title_wrapper = ['Pola: ' +  title] 
                with open(name, mode='a') as file:
                    Data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    # Data.writerow(["    "])
                    # Data.writerow(Title_wrapper)
                    # Data.writerow(['Periode(s)','Elapsed', 'Step', 'Pitch Angle', 'CoMx(m)', 'CoMy(m)', 'CoMz(m)', \
                    # 'Left Ankle Speed(rpm)','Right Ankle Speed(rpm)', 'Left Ankle Angle', 'Right Ankle Angle', 'Pola'])
                    Data.writerows(data_list)
                    Data.writerow(["    "])
                    Data.writerows(data2_save)
                    Data.writerow(["    "])
      
    againn = input("Masukkan y untuk merecord ulang: ")
    if againn != 'y':
        break
        
    if againn == 'y':
        step = 0
        data_list= []
        X = 0
        Y = 0
        Z = 0
    
if quitt!='q':
    robot.readAllPresentPos()
    print("Press E to enable or ESC to disable torque or 2 to disable hip")
    inp = getch()
    if inp == 'e':
        robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
    if inp == chr(0x1b):
        robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
    if inp == '2':
        robot.cekServoHip(ADDR_AX_TORQUE_ENABLE,TORQUE_DISABLE)
        while(1):
            print("Press E to enable or ESC to out")
            inpu = getch()
            if inpu == 'e':
                robot.cekServo(ADDR_AX_TORQUE_ENABLE,TORQUE_ENABLE)
                robot.readAllPresentPos()
            else:
                break
robot.portHandler.closePort()
