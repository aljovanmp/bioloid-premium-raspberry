
from dynamixel_sdk import *                    # Uses Dynamixel SDK library
import numpy as np
import math

class Robot:
    theta = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    tDefault = [0,8,-8,-20,20,13,-15,0,0,0,0,40,40,-75,-75,40,40,0,0]
    # tDefault = []
    # for i in range(0,19):
        # tDefault.append(0)
    servo_rot_dir = [0,1,1,-1,-1,-1,-1,0,0,-1,-1,-1,1,1,-1,1,-1,-1,-1]
    cek_joint = 0
    
    
    def __init__(self,object_dxl,port_handler,packet_handler,sync_write,object_forward,object_CoM):
        self.dxl = object_dxl
        self.portHandler = port_handler
        self.packetHandler = packet_handler
        self.groupSyncWrite= sync_write
        self.fwd = object_forward
        self.CoM = object_CoM
        
        
    def cekServo(self,addr_AX_torque, data):
        for obj in self.dxl:
            dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, obj.id, addr_AX_torque, data)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            else:
                print("Dynamixel#%d has been successfully connected" % obj.id)
    def cekServoHip(self,addr_AX_torque, data):
        for i in range(11,13):
            dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, i, addr_AX_torque, data)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
            else:
                print("Dynamixel#%d has been successfully connected" % i)    
    def readAllPresentPos(self,real_data_flag = 1, time_read = 0):
        addr_AX_Pres_pos = 36
        # print()
        start = time.time()
        if real_data_flag == 0:
            for obj in self.dxl:
                goal = obj.prevGoal
                obj.posNow = (obj.presentpos*0.29) + (obj.speed_dir*(obj.speed*0.666)*time_read)
                obj.posNow = round((obj.posNow/0.29),2)
                # if obj.id == 16:
                    # print('goal in read all: ', obj.prevGoal)
                if obj.posNow >= goal and obj.speed_dir == 1: 
                    obj.posNow = goal
                    # print("Masuk max goal id: ", obj.id)
                if obj.posNow <=goal and obj.speed_dir ==-1:
                    obj.posNow = goal
                    # print("Masuk min goal id: ", obj.id)
                # if obj.id == 16:
                    # print('=====================')
                    # print('prespos: ', obj.presentpos)
                    # print('posNow : ', obj.posNow)
                    # print('=====================')
        else :
            for obj in self.dxl:
                obj.presentpos, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, addr_AX_Pres_pos)
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
                elif dxl_error != 0:
                    print("%s" % self.packetHandler.getRxPacketError(dxl_error))
                # print("present position id %s is %s in degrees %f" %(obj.id,obj.presentpos,(obj.presentpos*0.29)))
        # print("Reading servo... time: ", round((time.time()-start),4))
    def readAll(self,address,length):
        out = []
        for obj in self.dxl:
            if length == 2 :
                feedback, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, obj.id, address)
            if length == 1 :
                feedback, dxl_comm_result, dxl_error = self.packetHandler.read1ByteTxRx(self.portHandler, obj.id, address)
            out.append(feedback)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % self.packetHandler.getRxPacketError(dxl_error))
        return out
    def syncWrite(self):
        # print("Masuk syncWrite...")
        for obj in self.dxl:
            dxl_addparam_result = self.groupSyncWrite.addParam(obj.id, obj.param)
            # print(obj.id, ": ", obj.param)
            if dxl_addparam_result != True:
                print("[ID:%03d] groupSyncWrite addparam failed" % obj.id)
                print("return: ", dxl_addparam_result, )
                
        # Syncwrite goal position
        dxl_comm_result = self.groupSyncWrite.txPacket()
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
    
        # Clear syncwrite parameter storage
        self.groupSyncWrite.clearParam()
   
    def updateTransMat(self):
        index = 0
        for obj in self.fwd:
            obj.trans_mat(self.DH_param[index])
            index=index+1
   
        # index = 0
        # for obj in self.fwd:
            # obj.trans_mat(self.DH_param[index])
            # if index <= 1:
                # print(obj.TM)
                # print('++++++++++++++++++')
            # index = index+1
    
    def updateDHParam(self,t):
        l = [0.057, 0.057, 0.015 , 0.057, 0.050, 0.048, 0.023]
        h = [0.032, 0.075, 0.014 , 0.061, 0.033, 0.087, 0.081, 0.107]
        d0 = 0.038
        self.t = t
        self.DH_param = [[0    ,90   ,0  ,0      ], #0
                [0       ,-90 ,d0    ,0     ], # 1 / 0,5 kanan
                [0       ,0   ,0     ,-l[0] ], #2 / 1 kanan V4
                [90       ,-90 ,h[0] ,0     ], #3
                [-90+t[17],-90 ,-l[1],0     ], #4
                [t[15]    ,-90 ,0    ,h[1]  ], #5
                [0        ,90  ,-l[2],0     ], #6
                [t[13]    ,-90 ,0    ,h[2]  ], #7
                [-90      ,-90 ,l[2] ,0     ], #8
                [90       ,90  ,h[3] ,0     ], #9
                [t[11]    ,0   ,0    ,l[1]  ], # 10 / 8,5
                [90       ,-90 ,0    ,0     ], # 11
                [t[9]     ,0   ,0    ,h[4]  ], #12
                [90       ,90  ,l[3] ,0     ], #13
                [0        ,0   ,0    ,-l[4] ], #14 / 12
                [0        ,-90 ,-d0  ,0     ], #15 / 0,5 Kiri
                [0       ,0   ,0     ,-l[0] ], #16 / 1 kiri V4
                [90       ,-90 ,h[0] ,0     ], #17 
                [-90+t[17],-90 ,-l[1],0     ], #18
                [t[15]    ,-90 ,0    ,h[1]  ], #19
                [0        ,90  ,-l[2],0     ], #20 
                [t[13]    ,-90 ,0    ,h[2]  ], #21
                [-90      ,-90 ,l[2] ,0     ], #22
                [90       ,90  ,h[3] ,0     ], #23
                [t[11]    ,0   ,0    ,l[1]  ], #24
                [90       ,-90 ,0    ,0     ], #25
                [t[9]     ,0   ,0    ,h[4]  ], #26
                [90       ,90  ,l[3] ,0     ], #27
                [0        ,0   ,0    ,l[4]  ], # 28 / 24 -> 12
                [0        ,0   ,h[5] ,0     ], # 29 / 12->26
                [0        ,0   ,0    ,-l[5] ], #30
                [90       ,-90 ,0    ,0     ], #31
                [t[2]-90  ,-90 ,l[6] ,0     ], #32
                [t[4]     ,0   ,0    ,-h[6] ], #33
                [t[6]     ,0   ,0    ,-h[7] ], #34
                [0        ,0   ,0    ,l[5]  ], # 35 / 26->32
                [90       ,90  ,0    ,0     ], #36
                [90+t[1]  ,90  ,l[6] ,0     ], #37
                [t[3]     ,0   ,0    ,-h[6] ], #38
                [t[5]     ,0   ,0    ,-h[7] ]] #39
        
        self.updateTransMat()
    
        
    def updateThetaDHParam(self, Real_data_flag):
        # print("Real Data: ", Real_data_flag)
        if Real_data_flag == 1:
            # print("Masuk real data")
            for obj in self.dxl:  
                self.theta[obj.id] = ((obj.presentpos-obj.default)*0.29*self.servo_rot_dir[obj.id])+self.tDefault[obj.id]
        else:
            for obj in self.dxl:        
                # if obj.id == 16:
                    # print('id 16 update: ',obj.posNow)
                self.theta[obj.id] = ((obj.posNow-obj.default)*0.29*self.servo_rot_dir[obj.id])+self.tDefault[obj.id]
        #self.theta[0] = ((self.dxl[4].presentpos-self.dxl[4].default)*0.29*self.servo_rot_dir[0])+self.tDefault[0]
        #self.theta[1] = ((self.dxl[3].presentpos-self.dxl[3].default)*0.29*self.servo_rot_dir[1])+self.tDefault[1]
    
    def TransMatGlob(self,last_axis, Read_Flag):
        self.updateThetaDHParam(Read_Flag)
        self.updateDHParam(self.theta)
        # if self.cek_joint == 0:
            # print()
            # print("Sudut joint: ")
            # for i in range(1,19):
                # print(i,end='   ')
            # print(" ")
            # for i in range(1,19):
                # print(round(self.t[i],1),end='   ')
            # print(" ")
            # self.cek_joint = 1
        # print(" ")
        # print('====================')
        
        Trans_mat_glob=np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
        Trans_matrices = []
        if last_axis > 15 and last_axis < 35:
            for obj in self.fwd:
                if obj.n==1:
                    Trans_matrices.append(obj.TM)
                if obj.n>=16 and obj.n<=last_axis:
                    Trans_matrices.append(obj.TM)
                if obj.n>last_axis:
                    break
        elif last_axis >35:
            for obj in self.fwd:
                if obj.n==1:
                    Trans_matrices.append(obj.TM)
                if obj.n>=16 and obj.n<=30:
                    Trans_matrices.append(obj.TM)
                if obj.n > 35 and obj.n<=last_axis:
                    Trans_matrices.append(obj.TM)
                if obj.n > last_axis:
                    break
        else:
            for obj in self.fwd:
                if (obj.n<=last_axis):
                    Trans_matrices.append(obj.TM)
                else:
                    break
        # print('Trans_matrices: ')
        # print(Trans_matrices)
        for i in Trans_matrices:
            next_mat = np.array(i)
            # print('next_mat: ')
            # print(next_mat)
            recent_glob = np.matmul(Trans_mat_glob,next_mat)
            Trans_mat_glob = recent_glob.copy()
        return {'to':last_axis, 'TM':Trans_mat_glob}
    
    def com(self,read_flag = 1, Time_read = 0):
        if read_flag == 1:
            self.readAllPresentPos()
        if read_flag == 0:
            self.readAllPresentPos(real_data_flag = 0, time_read=Time_read)
        x = 0
        y = 0
        z = 0
        m = 0
        self.cek_joint = 0
        for obj in self.CoM:
            # print('===============================================')
            part = obj.cek(read_flag)
            # print()
            # print("Part: ", obj.Link, " CoM:")
            # print(part)
            x_next = obj.Coord_from_glob[0].copy()
            y_next = obj.Coord_from_glob[1].copy()
            z_next = obj.Coord_from_glob[2].copy()
            m_next = obj.mass
            # print("Coordinat Before: ")
            # print('x: ', x, 'y: ', y, 'z: ', z, 'm: ', m)
            # print()
            # print("Next : ")
            # print('x: ', x_next, 'y: ', y_next, 'z: ', z_next, 'm: ', m_next)
            x = np.around(((x*m) + (x_next*m_next)) / (m+m_next),3)
            y = np.around(((y*m) + (y_next*m_next)) / (m+m_next),3)
            z = np.around(((z*m) + (z_next*m_next)) / (m+m_next),3)
            m = round((m + m_next),3)
            # print()
            # print('CoM: ')
            # print("Last_axis: ", obj.Last_a)
            # print('x: ', x, 'y: ', y, 'z: ', z, 'm: ', m)
            # print()
            
        return{'x':x, 'y':y,'z':z}       
        
class Forward:
    def __init__(self,n):
        self.n = n
        
    def trans_mat(self,DH):
        thetaN = DH[0]
        alphaN = DH[1]
        dN     = DH[2]
        rN     = DH[3]
        # print("DH: ", DH)
        a1b1 = round((math.cos(math.radians(thetaN))),3)
        a1b2 = round(((math.sin(math.radians(thetaN)))*\
               (math.cos(math.radians(alphaN)))),3)
        a1b3 = round(((math.sin(math.radians(thetaN)))*\
               (math.sin(math.radians(alphaN)))),3)
        a1b4 = round((rN*(math.cos(math.radians(thetaN)))),3)    
        
        a2b1 = round((math.sin(math.radians(thetaN))),3)
        a2b2 = round(((math.cos(math.radians(thetaN)))*\
               (math.cos(math.radians(alphaN)))),3)
        a2b3 = round(((math.cos(math.radians(thetaN)))*\
               (math.sin(math.radians(alphaN)))),3)
        a2b4 = round((rN*(math.sin(math.radians(thetaN)))),3)
        
        a3b2 = round((math.sin(math.radians(alphaN))),3)
        a3b3 = round((math.cos(math.radians(alphaN))),3)
        a3b4 = dN
        
        self.TM = [[a1b1,-a1b2,a1b3 ,a1b4],   #TM for Transformation Matrices
                   [a2b1,a2b2 ,-a2b3,a2b4],
                   [0   ,a3b2 ,a3b3 ,a3b4],
                   [0   ,0    ,0    ,1   ]]
                   
        return{'from':(self.n-1), 'to':(self.n), 'TM':self.TM}
        
class CoM_Class:
    def __init__(self,Link_Name,Last_axis,CoM_Coordinate,Robot_obj,Link_mass):
        self.Link = Link_Name
        self.Last_a = Last_axis
        self.Robot = Robot_obj
        self.CoM_Coord = CoM_Coordinate
        self.mass = Link_mass
    def cek(self,Read_flag):
        TM_from_glob = self.Robot.TransMatGlob(self.Last_a,Read_flag)
        TM = np.array(TM_from_glob['TM'])
        Coord = np.array(self.CoM_Coord)
        # print()
        # print("Trans to:  ", TM_from_glob['to'])
        # print(TM)
        # print("coo: ")
        # print(Coord)
        self.Coord_from_glob = np.matmul(TM,Coord)
        return(self.Coord_from_glob)    