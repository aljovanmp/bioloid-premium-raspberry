from mpu6050 import mpu6050
from Kalman import KalmanAngle
import time
import math

data2Write= [[]]
kalmanX = KalmanAngle()
kalmanY = KalmanAngle()
kompX = 0
kompY = 0

radToDeg = 57.2957786
kalAngleX = 0
kalAngleY = 0

AcGy = [0. for k in range (6)] #inisialisasi nilai akselerometer dan gyroscope

def ambilData():
    mpu = mpu6050(0x68)
    print("Temp : ", str(mpu.get_temp()))
    print()
    imu = [0. for k in range (6)] #inisialisasi imu
    accel_data = mpu.get_accel_data()
    imu[0] = accel_data['x']
    imu[1] = accel_data['y']
    imu[2] = accel_data['z']

    gyro_data = mpu.get_gyro_data()
    imu[3] = gyro_data['x']
    imu[4] = gyro_data['y']
    imu[5] = gyro_data['z']
    #print("Imu : " + str(imu))
    #print()
    return imu
    
time.sleep(1)
AcGy = ambilData()

accX = AcGy[0]
accY = AcGy[1]
accZ = AcGy[2]

roll = math.atan2(accY,accZ) * radToDeg
pitch = math.atan2(-accX,accZ) * radToDeg

print("Roll awal = ",roll)
print("Pitch awal = ",pitch)

kalmanX.setAngle(roll)
kalmanY.setAngle(pitch)
gyroXAngle = roll;
gyroYAngle = pitch;
compAngleX = roll;
compAngleY = pitch;

timer = time.time()
flag = 0
loopke = 0

import rospy
from std_msgs.msg import String

msg = ''
def callback(data):
	rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    msg = data.data
    
     
def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
     # name are launched, the previous one is kicked off. The
     # anonymous=True flag means that rospy will choose a unique
     # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
	rospy.init_node('listener', anonymous=True)

	rospy.Subscriber("chatter", String, callback)

	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()

import csv
data_list = [["SN", "Name", "Contribution"],
             [1, "Linus Torvalds", "Linux Kernel"],
             [2, "Tim Berners-Lee", "World Wide Web"],
             [3, "Guido van Rossum", "Python Programming"]]
             
with open('innovators.csv', 'w', newline='') as file:
    writer = csv.writer(file, delimiter='|')
    writer.writerows(['step', 'pitch angle']
    writer.writerows(data_list)
    
while True:
    listener()
    
    loopke += 1
    if(flag >100): #Problem with the connection
        print("There is a problem with the connection")
        flag=0
        continue
    try:
        AcGy = ambilData()
        accX = AcGy[0]
        accY = AcGy[1]
        accZ = AcGy[2]
        
        gyroX = AcGy[3]
        gyroY = AcGy[4]
        gyroZ = AcGy[5]

        dt = time.time() - timer
        timer = time.time()

        roll = math.atan2(accY,accZ) * radToDeg
        pitch = math.atan2(-accX,accZ) * radToDeg

        gyroXRate = gyroX/131
        gyroYRate = gyroY/131


        if((roll < -90 and kalAngleX >90) or (roll > 90 and kalAngleX < -90)):
            kalmanX.setAngle(roll)
            complAngleX = roll
            kalAngleX   = roll
            gyroXAngle  = roll
        else:
            kalAngleX = kalmanX.getAngle(roll,gyroXRate,dt)

        if(abs(kalAngleX)>90):
            gyroYRate  = -gyroYRate
            kalAngleY  = kalmanY.getAngle(pitch,gyroYRate,dt)

        if((pitch < -90 and kalAngleY >90) or (pitch > 90 and kalAngleY < -90)):
            kalmanY.setAngle(pitch)
            complAngleY = pitch
            kalAngleY   = pitch
            gyroYAngle  = pitch
        else:
            kalAngleY = kalmanY.getAngle(pitch,gyroYRate,dt)

        if(abs(kalAngleY)>90):
            gyroXRate  = -gyroXRate
            kalAngleX = kalmanX.getAngle(roll,gyroXRate,dt)

        #angle = (rate of change of angle) * change in time
        gyroXAngle = gyroXRate * dt
        gyroYAngle = gyroYAngle * dt

        #compAngle = constant * (old_compAngle + angle_obtained_from_gyro) + constant * angle_obtained from accelerometer
        compAngleX = 0.93 * (compAngleX + gyroXRate * dt) + 0.07 * roll
        compAngleY = 0.93 * (compAngleY + gyroYRate * dt) + 0.07 * pitch

        if ((gyroXAngle < -180) or (gyroXAngle > 180)):
            gyroXAngle = kalAngleX
        if ((gyroYAngle < -180) or (gyroYAngle > 180)):
            gyroYAngle = kalAngleY
        
        kalAngleX = kalAngleX + kompX
        kalAngleY = kalAngleY + kompY

        if (kalAngleX < -180):
            kalAngleXfix = round(360 + kalAngleX,3)
            if (kalAngleY < -180):
                kalAngleYfix = round(360 + kalAngleY,3)
            else:
                if (kalAngleY > 180):
                    kalAngleYfix = round(kalAngleY - 360,3)
                else:
                    kalAngleYfix = round(kalAngleY,3)

        if (kalAngleX > 180):
            kalAngleXfix = round(kalAngleX - 360,3)
            if (kalAngleY < -180):
                kalAngleYfix = round(360 + kalAngleY,3)
            else:
                if (kalAngleY > 180):
                    kalAngleYfix = round(kalAngleY - 360,3)
                else:
                    kalAngleYfix = round(kalAngleY,3)
        else:
            kalAngleXfix = round(kalAngleX,3)
            if (kalAngleY < -180):
                kalAngleYfix = round(360 + kalAngleY,3)
            else:
                if (kalAngleY > 180):
                    kalAngleYfix = round(kalAngleY - 360,3)
                else:
                    kalAngleYfix = round(kalAngleY,3)


        print("Angle X: " + str(kalAngleXfix)+"   " +"Angle Y: " + str(kalAngleYfix))
        data2Write.append([msg,str(kakalAngleYfix)])
        print('msg: ', msg)
        #print(str(roll)+"  "+str(gyroXAngle)+"  "+str(compAngleX)+"  "+str(kalAngleX)+"  "+str(pitch)+"  "+str(gyroYAngle)+"  "+str(co$
        print("---------------------------------------------------------------------------------")
        time.sleep(0.1) #kalau terlalu lambat, maka semakin lambat pula pasnya

        if (loopke == 10): #pasang kompensator
            kompX = 0 - kalAngleX
            kompY = 0 - kalAngleY
        
    except Exception as exc:
        flag += 1
