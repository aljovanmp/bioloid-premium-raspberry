from mpu6050 import mpu6050
from Kalman import KalmanAngle
import time
import math

# bias = float(input("Masukkan q bias: "))
# angle = float(input("Masukkan q angle: "))
# kalmanX = KalmanAngle(bias, angle)
# kalmanY = KalmanAngle(bias, angle)

kalmanX = KalmanAngle()
kalmanY = KalmanAngle()
kompX = 0
kompY = 0

radToDeg = 57.2957786
kalAngleX = 0
kalAngleY = 0

AcGy = [0. for k in range (6)] #inisialisasi nilai akselerometer dan gyroscope

def ambilData():
    mpu = mpu6050(0x68)
    # print("Temp : ", str(mpu.get_temp()))
    # print()
    imu = [0. for k in range (6)] #inisialisasi imu
    accel_data = mpu.get_accel_data()
    imu[0] = accel_data['x']
    imu[1] = accel_data['y']
    imu[2] = accel_data['z']

    gyro_data = mpu.get_gyro_data()
    imu[3] = gyro_data['x']
    imu[4] = gyro_data['y']
    imu[5] = gyro_data['z']
    #print("Imu : " + str(imu))
    #print()
    return imu
    
time.sleep(1)
AcGy = ambilData()

accX = AcGy[0]
accY = AcGy[1]
accZ = AcGy[2]

roll = math.atan(accY/math.sqrt((accX**2)+(accZ**2))) * radToDeg
pitch = math.atan2(-accX,accZ) * radToDeg

print("Roll awal = ",roll)
print("Pitch awal = ",pitch)

kalmanX.setAngle(roll)
kalmanY.setAngle(pitch)
gyroXAngle = roll;
gyroYAngle = pitch;
compAngleX = roll;
compAngleY = pitch;

timer = time.time()
flag = 0
loopke = 0
zero = 0
calibrate = 0 
kalAngleYfix = 0
while True:
    if calibrate == 0:
        print('calibrating...')
        for i in range(100):
            AcGy = ambilData()
            zero += 0 - AcGy[4]
            time.sleep(0.05)
        calibrate = 1
        zero /= 100
        print('zero: ', zero)
    
    loopke += 1
    if(flag >100): #Problem with the connection
        print("There is a problem with the connection")
        flag=0
        continue
    # try:
    AcGy = ambilData()
    accX = AcGy[0]
    accY = AcGy[1]
    accZ = AcGy[2]
    
    
    gyroX = AcGy[3]
    gyroY = AcGy[4] + zero
    gyroZ = AcGy[5]

    dt = time.time() - timer
    timer = time.time()

    roll = math.atan2(accY,accZ) * radToDeg
    pitch = math.atan2(-accX,accZ) * radToDeg
    
    
    # print('gyro bef:', gyroX)
    gyroXRate = gyroX
    gyroYRate = gyroY
    
    # gyroXRate = gyroX/131
    # gyroYRate = gyroY/131
    # print('gyro : ', gyroYRate)
    # if((roll < -90 and kalAngleX >90) or (roll > 90 and kalAngleX < -90)):
        # kalmanX.setAngle(roll)
        # complAngleX = roll
        # kalAngleX   = roll
        # gyroXAngle  = roll
    # else:
        # kalAngleX = kalmanX.getAngle(roll,gyroXRate,dt)
    # if(abs(kalAngleX)>90):
        # gyroYRate  = -gyroYRate
        # kalAngleY  = kalmanY.getAngle(pitch,gyroYRate,dt)

    if((pitch < -90 and kalAngleY >90) or (pitch > 90 and kalAngleY < -90)):
        kalmanY.setAngle(pitch)
        complAngleY = pitch
        kalAngleY   = pitch
        gyroYAngle  = pitch
    else:
        kalAngleY = kalmanY.getAngle(pitch,gyroYRate,dt)

    if(abs(kalAngleY)>90):
        gyroXRate  = -gyroXRate
        kalAngleX = kalmanX.getAngle(roll,gyroXRate,dt)
    #angle = (rate of change of angle) * change in time
    gyroXAngle = gyroXRate * dt
    gyroYAngle = gyroYAngle * dt

    #compAngle = constant * (old_compAngle + angle_obtained_from_gyro) + constant * angle_obtained from accelerometer
    compAngleX = 0.93 * (compAngleX + kalmanX.rate * dt) + 0.07 * roll
    compAngleY = 0.93 * (compAngleY + kalmanY.rate * dt) + 0.07 * pitch

    if ((gyroXAngle < -180) or (gyroXAngle > 180)):
        gyroXAngle = kalAngleX
    if ((gyroYAngle < -180) or (gyroYAngle > 180)):
        gyroYAngle = kalAngleY
   

    if (kalAngleY > -180 and kalAngleY<0):
        kalAngleYfix = round(kalAngleY + 360,3) + kompY
    else:
        kalAngleYfix = round(kalAngleY,3) + kompY
    # if (kalAngleX < -180):
        # kalAngleXfix = round(360 + kalAngleX,3)
        # if (kalAngleY < -180):
            # kalAngleYfix = round(360 + kalAngleY,3)
        # else:
            # if (kalAngleY > 180):
                # kalAngleYfix = round(kalAngleY - 360,3)
            # else:
                # kalAngleYfix = round(kalAngleY,3)

    # if (kalAngleX > 180):
        # kalAngleXfix = round(kalAngleX - 360,3)
        # if (kalAngleY < -180):
            # kalAngleYfix = round(360 + kalAngleY,3)
        # else:
            # if (kalAngleY > 180):
                # kalAngleYfix = round(kalAngleY - 360,3)
            # else:
                # kalAngleYfix = round(kalAngleY,3)
    # else:
        # kalAngleXfix = round(kalAngleX,3)
        # if (kalAngleY < -180):
            # kalAngleYfix = round(360 + kalAngleY,3)
        # else:
            # if (kalAngleY > 180):
                # kalAngleYfix = round(kalAngleY - 360,3)
            # else:
                # kalAngleYfix = round(kalAngleY,3)


    print('Pitch  : %1f' %(kalAngleY),'rate    : %1f'%kalmanY.rate,'bias: %1f' %(kalmanY.bias),sep='\t')
    # print()
    print('pitch_a: ', round(kalAngleYfix,3), 'raw_gyro: %1f raw_acc: %1f'%(gyroYRate,pitch))
    print()
    
    #print(str(roll)+"  "+str(gyroXAngle)+"  "+str(compAngleX)+"  "+str(kalAngleX)+"  "+str(pitch)+"  "+str(gyroYAngle)+"  "+str(co$
    # print("---------------------------------------------------------------------------------")
    time.sleep(0.005) #kalau terlalu lambat, maka semakin lambat pula pasnya

    if (loopke == 15): #pasang kompensator
        # kompX = 0 - kalAngleXfix
        kompY = 0 - kalAngleYfix
        
    # except Exception as exc:
        # flag += 1
