from mpu6050 import mpu6050
import math
import time

imu = mpu6050(0x68)
imu.set_accel_range(imu.ACCEL_RANGE_4G)
imu.set_gyro_range(imu.GYRO_RANGE_500DEG)

def processed_accel(x,y,z):
    roll = math.atan2(y,math.sqrt((x*x)+(z*z)))
    pitch = math.atan2(-x,z)
    
    return {'pitch':round(math.degrees(pitch),3),'roll':round(math.degrees(roll),3)}

def processed_gyro(x,y,z,prevroll,prevpitch,dt):
    roll     = x*dt
    sum_roll = prevroll + roll
    
    pitch     = y*dt
    sum_pitch = prevpitch + pitch
    
    return {'pitch':round(sum_pitch,3),'roll':round(sum_roll,3)} 
    
prev_roll_gyro=0
prev_pitch_gyro = 0 
prevTime = 0
gyro_offset = {'x':9.400, 'y':5.929, 'z':0.474}
accel_offset = {'pitch':178.027, 'roll':0}
mean_accel= [0,0,0]
mean_gyro= [0,0,0]
comp_fltr = {'pitch': 0, 'roll':0}
mean_accel_angle = {'pitch': 0, 'roll':0}
firstcalc = True
alpha = 0.98
x = 0
while(1):
    dt = 0.1
    if (time.time() - prevTime > dt):
        data = imu.get_all_data()
        accel_data = data[0]
        gyro_data = data[1]
        x = x + 1
        a = 0
        for i in accel_data :
            accel_data[i] = round(accel_data[i],3)
            gyro_data[i] = round(gyro_data[i],3) + gyro_offset[i]
            # print(a)
            # mean_accel[a] = (accel_data[i] + (mean_accel[a]*(x-1))) / x
            mean_gyro[a] = (gyro_data[i] + (mean_gyro[a]*(x-1))) / x
            a = a+1
        print("gyro data  : ",gyro_data)
        # print("accel_data : ",accel_data)
        # print("mean gyro  : ",mean_gyro)
        # print("mean accel : ",mean_accel)
        accel_angle= processed_accel(accel_data['x'],accel_data['y'], accel_data['z'])
        if accel_angle['pitch'] < 0:
            accel_angle['pitch'] = accel_angle['pitch'] + 360
        accel_angle['pitch'] = round(accel_angle['pitch'] - accel_offset['pitch'],3) 
        # for i in accel_angle:
            # mean_accel_angle[i] = (accel_angle[i] + (mean_accel_angle[i]*(x-1))) / x
        # print("mean accel : ",mean_accel_angle)
        if (firstcalc):
            prev_roll_gyro = accel_angle['roll']
            prev_pitch_gyro = accel_angle['pitch']
        gyro_angle = processed_gyro(gyro_data['x'],gyro_data['y'],gyro_data['z'],prev_roll_gyro,prev_pitch_gyro,dt)
        prev_roll_gyro  = gyro_angle['roll']
        prev_pitch_gyro = gyro_angle['pitch']
        prevTime = time.time()
        print('accel angle: ', accel_angle)
        print('gyro_angle : ', gyro_angle)
        
        if (firstcalc):
            comp_fltr['pitch'] = accel_angle['pitch']
            comp_fltr['roll']  = accel_angle['roll']
            firstcalc = False
            print("firstcalc")
        else :
            # comp_fltr['pitch'] = round(((1-alpha) * (comp_fltr['pitch'] + (gyro_angle['pitch']* dt))) + ((alpha) * accel_angle['pitch']),3)
            # comp_fltr['roll']  = round(((1-alpha) * (comp_fltr['roll']  + (gyro_angle['roll']* dt)))  + ((alpha) * accel_angle['roll']),3)
            print("cekk")
            # comp_fltr['pitch'] = round((((1-alpha)*accel_angle['pitch']) + (alpha*gyro_angle['pitch']))/(1-(alpha*comp_fltr['pitch'])),3)
            # comp_fltr['roll']  = round((((1-alpha)*accel_angle['roll']) + (alpha*gyro_angle['roll']))/(1-(alpha*comp_fltr['roll'])),3)
            
            comp_fltr['pitch'] = round(alpha*(comp_fltr['pitch'] + (gyro_data['y'] * dt)) + ((1-alpha)*accel_angle['pitch']),3)
            comp_fltr['roll'] = round(alpha*(comp_fltr['roll'] + (gyro_data['x'] * dt)) + ((1-alpha)*accel_angle['roll']),3)
        print('comp_fltr  : ', comp_fltr)
        print('==================')
        